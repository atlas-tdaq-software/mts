# MTS

## tdaq-11-04-00

#### Subscription syntax
Extended the subscription syntax allowing
- ':' symbol in tokens
- wildcard in the beginning of a token
- spaces as part of context values, e.g. in function or file names

Current subscription syntax (quasy EBNF notation):

```ebnf
key = 'app' | 'msg' | 'qual'
sev = 'sev'
par = '(param|par)(+[a-zA-Z_-])'
context_item = 'app' | 'host' | 'user' | 'package' | 'file' | 'function' | 'line'
context = 'context(context_item)'
sevlev = 'fatal' | 'error' | 'warning' | 'info' | 'information'
token_wildcard = -'*'+[a-zA-Z_.:-]-'*'
token = +[a-zA-Z_.:-]
char = // any character exclusing quotes
quoted_string = '"'+char'"' | "'"+char"'"
item = (key ('=' | '!=') token_wildcard) | (sev ('=' | '!=') sevlev) | (par ('=' | '!=') token)
    | (context ('=' | '!=') quoted_string)
factor = item | ( expression ) | ('not' factor)
expression = '*' | factor *(('and' expression) | ('or' expression))
```

An example of subscription is

```ebnf
sev=ERROR or (msg=mtssender::* and (msg=*Died or msg=*::longmessageid) and param(pid)!=666 and context(line)!=1 and context(function) != 'const int daq::function')
```

#### Use new way of declaring jERS stream implementations
Follow the changes in jERS, using ErsStreamName annotation to declare a class implementing certain ERS stream:

```java
@ErsStreamName(name="mts")
public class MTSInStream extends ers.AbstractInputStream { ...
```

## tdaq-11-02-00

Use new ers2idl package. Removed dependency on OWLSemaphore.

Added ers2splunk.sh utility to redirect the output of mts2splunk_receiver to stdout and further to TCP host:port, feeding splunk indexer on a host. This is to be used on tbed splunk instance. An application (ers2splunk) is added to the setup segment infrastrcuture on tbed.

## tdaq-10-00-00

### MTS library: synchronous subscription

Added synchronous subscription functionality (<https://its.cern.ch/jira/browse/ATDAQCCMRS-41>): call to `add_receiver` publishes subscripton information in MTS IS server and then waits untill all MTS workers (registered at that moment) explicitly notify the subscribed that subscription is registered, or untill the specified timeout expires (milliseconds).

Functionality is implemented in both Java and C++ libraries. The changes are backward-compatible: the synchronization is only enabled when `add_receiver` is called with additional third parameter, the timeout value. Most of the clients create subscription well in advance before receiving first message, so this functionality is mostly addressing those who creates a subscription and may expect messages to arrive very shortly after (CES recovery case which is based on MTS messages exchange).

A reasonable timeout would be 50ms, though normally workers are notified within few milliseconds.

### mts2splunk utility

Added two new command line parameters, allowing to filter out messages with particular `msgId` or `appId`.

## tdaq-09-00-00

Since tdaq-07-01-00 release, only few bug fixes and 2 little additions:

- allow specifying ERS context line number in subscription syntax, e.g.

```
(app=HLTMPPU* and context(line)!=666) 
```

- a Java ulility (available in erssender.jar) to send arbitrary ERS messages like

```bash
TDAQ_PARTITION=mypart TDAQ_APPLICATION_NAME=myapp java -cp erssender.jar:ers.jar utils.ERSSender ERROR mymsg-id "My message text"
```
