/**	\file IPCPartitionManager.h
	\brief      Declaration of class IPCPartitionManager.
  	\author     A. Kazarov
    \copyright 	2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
	\date     	2013Sep 2013
 */

#ifndef IPCPARTITIONMANAGER_H_
#define IPCPARTITIONMANAGER_H_

#include <ipc/partition.h>

namespace daq {
namespace mts {
/**
 * \class IPCPartitionManager
 * \brief This base class contains common code for other MTS classes (WorkersManager, SubscriptionManager)
 * which need to create and use IPCPartition object.
 */
class IPCPartitionManager {

const char* part_env = "TDAQ_PARTITION" ; // where to read partition name if not explicitly provided

protected:
	IPCPartition				m_ipc_partition ;

	/*!
	 * Read partition name from environment
	 * @return literal, partition name, empty "" if part_env environment is not set
	 */
	const char* get_partition_name() ;

public:
	IPCPartitionManager(const std::string& partition) ;

	IPCPartitionManager() ;

	virtual ~IPCPartitionManager() ;
};

}}

#endif /* IPCPARTITIONMANAGER_H_ */
