/*
 * SubscriptionManager.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: andrei
 */

#include <sstream>
#include <unistd.h>

#include <system/Host.h>

#include <is/infodictionary.h>
#include <is/info.h>

#include "mts/exceptions.h"
#include "mts/IPCPartitionManager.h"
#include "mts/SubscriptionManager.h"

using namespace daq::mts ;
using namespace ::is ;

std::atomic<size_t>		SubscriptionManager::s_index ;

SubscriptionManager::SubscriptionManager( const std::string& partition )
:IPCPartitionManager(partition), m_is_receiver(partition)
{
ERS_DEBUG(1, "Constructed SubscriptionManager") ;
}

SubscriptionManager::~SubscriptionManager()
{
ERS_DEBUG(1, "Destroyed SubscriptionManager") ;
}

/**
 *  Called by MTS Receiver class.
 *
 *  Converts CORBA ref to string and publishes to IS, sharing it to all workers.
 *  Constructs an unique name for the subscriber, which is the name is IS info object.
 *
 * @param ref CORBA reference
 * @param expression subscription criteria
 * @param sync if synchronous subscription is requested
 * @exception SubscriptionRegistrationFailure in case of any IPC/IS exception
 */
void SubscriptionManager::subscribe(const std::string& corba_ref, const std::string& expression, bool sync)
{
// create MTSSubscriptionInfo for this application
// std::string ref_string = IPCCore::objectToString(ref) ;
::mts::MTSSubscriptionInfo myinfo ;
myinfo.receiver_ref = corba_ref ;
myinfo.filter_expression = expression ;
myinfo.sync_required = sync ;

// need to invent some unique name for this corba application

std::ostringstream local_id ;
// local_id << IS_server_name + "." ;

if ( std::getenv("TDAQ_APPLICATION_NAME") != NULL )
	{
	local_id << std::getenv("TDAQ_APPLICATION_NAME") << ":" ;
	}

local_id << ::System::LocalHost::full_local_name() << ':' << ::getpid() << ":" << s_index++ ;
m_my_is_name = local_id.str() ;

ERS_DEBUG(0, "Registering subscription info for " << m_my_is_name << " on " << IS_server_name << " IS server") ;
try {
	ISInfoDictionary dict(m_ipc_partition) ;
	dict.checkin(IS_server_name + "." + m_my_is_name, myinfo) ; // ( daq::is::InvalidName, daq::is::RepositoryNotFound, daq::is::InfoNotCompatible )
	}
catch ( const daq::is::Exception& ex )
	{
	throw SubscriptionRegistrationFailure(ERS_HERE, ex) ;
	}

ERS_LOG("Subscription info for " << m_my_is_name << " is registered on " << IS_server_name << " IS server") ;
}

void SubscriptionManager::unsubscribe_receiver(const std::string& is_name)
{
	ISInfoDictionary dict(m_ipc_partition) ;
	try {
		dict.remove(IS_server_name + "." + is_name) ;
	}
	catch ( const daq::is::Exception& ex )
	{
		ERS_LOG("Failed to remove " << is_name << " from " << IS_server_name << " IS server: " << ex) ;
		return ;
	// TODO Failed to unsubscribe, so what?
	}

	ERS_LOG("Receiver " << is_name << " unpublished from " << IS_server_name << " IS server") ;
}

void SubscriptionManager::unsubscribe_worker()
{
	try	{
	m_is_receiver.unsubscribe(IS_server_name, subscription_criteria) ;
	}
	catch ( const daq::is::Exception& ex )
	{
		ERS_LOG("Failed to cleanly unsubscribed from IS MTS information changes: " << ex) ;
		return ;
	}

	ERS_LOG("Worker unsubscribed from IS MTS information changes") ;
}

// to be called by workers
// register IS callback on any change in MTSSubscriptionInfo IS objects
// to be called on existing objects as well - new IS feature
// throws IS exceptions - to be caught and reported by the binary
void SubscriptionManager::get_subscriptions(ISCallbackF callback, Worker* caller)
{
	m_is_receiver.subscribe( IS_server_name, subscription_criteria, callback, caller,
				{ reason::Created, reason::Updated, reason::Deleted, reason::Subscribed } ) ; // throw daq::is::Exception
	ERS_LOG("Subscribed to MTS info on " << IS_server_name << " IS server") ;
}

