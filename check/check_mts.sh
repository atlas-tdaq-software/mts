#!/bin/bash

partition=mtscheck

tmpdir=`mktemp -d`
trap "rm -rf ${tmpdir}" EXIT KILL TERM
cd ${tmpdir}

export TDAQ_IPC_INIT_REF="file:/${tmpdir}/ipc.ref"
ipc_server &
pidipcg=$!

count=0;
while test $count -lt 5
do
        sleep 2
        test_ipc_server > /dev/null 2>&1 && { echo " OK!" ; break ; } 
        count=`expr $count + 1`
done

ipc_server -p $partition &
pidipcp=$!
sleep 1

is_server -p $partition -n MTS &> mts-is-server.out &
pidis=$!
sleep 1

#export TDAQ_ERS_STREAM_LIBS="mtsStreams"

TDAQ_APPLICATION_NAME=worker1 mts_worker -p $partition &> mts-worker1.out &
pidw1=$!

mts_worker -p $partition -n worker2  &> mts-worker2.out &
pidw2=$!
sleep 1

TDAQ_PARTITION=$partition TDAQ_ERS_DEBUG='null' TDAQ_ERS_LOG='lstderr' \
mts_ers_receiver -t 0 -n 1 -s "sev=ERROR or msg=mtssender::ProcDied" -v > mts_ers_receiver.out 2> mts_ers_receiver.err &
pidr1=$!

TDAQ_ERS_DEBUG='null' TDAQ_ERS_LOG='lstderr' \
mts_ers_receiver -p $partition -t 0 -n 1 -s "sev=ERROR or (msg=mtssender::* and (msg=*Died or msg=*::longmessageid) and param(pid)!=666 and context(line)!=1 and context(function) != 'const int daq::function')" -v > mts_ers_receiver2.out 2> mts_ers_receiver2.err &
pidr2=$!

sleep 1

TDAQ_APPLICATION_NAME="test-sender" TDAQ_PARTITION=$partition TDAQ_ERS_ERROR='mts,lstdout,file(mts_ers_sender.out)' \
mts_ers_sender -n 1 >& mts_ers_sender-local.out &
pids=$!

sleep 1

echo "Comparing output..."
#cat mts_ers_sender.out
#cat mts_ers_receiver.out
#cat mts_ers_receiver2.out
#cat mts_ers_sender-local.out

result=1 ;
diff mts_ers_sender.out mts_ers_receiver.out && \
diff mts_ers_receiver2.out mts_ers_receiver.out && \
diff mts_ers_sender.out mts_ers_sender-local.out && { echo "Identical! Test passed."; result=0 ;}

kill $pidr1 $pidr2 $pids

ipc_rm -p $partition -f -n .\* -i .\*
sleep 1

kill $pidw2 $pidw1
sleep 1
kill $pidis
sleep 1
kill $pidipcp
sleep 1
kill $pidipcg &> /dev/null
sleep 1
kill -9 $pidw2 $pidw1 $pidipcp $pidis $pidipcg &> /dev/null

exit $result

