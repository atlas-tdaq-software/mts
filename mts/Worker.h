/*!
 *  \file Worker.h
 *  Defines main class for implementation of the MTS worker application.
 *
 *
 *  \author A. Kazarov
 *  \date Nov. 2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef MTS_WORKER_H
#define MTS_WORKER_H

#include <utility>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <atomic>
#include <vector>
#include <set>
#include <random>

#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/policy.h>

#include "tbb/queuing_rw_mutex.h"

#include "mts/mts.hh"
#include "mts/exceptions.h"
#include "mts/ThreadPoolNQ.h"
#include "mts/Subscription.h"
#include "mts/SubscriptionManager.h"

namespace daq {
namespace mts {

// possible distributions for calculating priorities for slow subscribers
// std::piecewise_linear_distribution<>
// std::uniform_int_distribution<int>
using dist_type = std::exponential_distribution<> ;

using SubscriptionSP = std::shared_ptr< Subscription > ;		/*!< shared pointer to Subscription structure */

using SubscriptionsList = std::vector< SubscriptionSP >	;		/*!< List of active subscriptions */

using SubscriptionsListCIt = SubscriptionsList::const_iterator ;
using SubscriptionsListIt = SubscriptionsList::iterator ;


/*! \brief Implements the MTS worker functionality.
    \ingroup private

    Implements named IPC object (to be able to register in IPC) and TaskProvider, to schedule delivery jobs to the thread pool.
    Implements notify() IDL method which accepts incoming messages from Senders,
    filter them and (if matched) places to the Subscription delivery queues.

    Contains list of Subscriptions (buffering the messages for delivery) and thread pool of delivery worker threads.
    The scheduler guarantees fairness of mapping of Subscriptions to the worker threads, isolating slow or overloaded subscribers and
    giving equal amount of thread pool to each subscriber.
	\see TaskProvider 
	\see SubscriptionManager
 */
class Worker: public IPCNamedObject< POA_mts::worker, ::ipc::multi_thread >, public TaskProvider, public SubscriptionManager
	{
	constexpr static size_t	n_threads = 24 ; 						/*!<  N of threads in thread pool */
	constexpr static size_t	max_threads_fraction_per_subscr = 3 ; 	/*!<  max 1/N of available threads allocated for one subscriber */
	constexpr static size_t	stat_period = 5 ; 						/*!<  Interval for publishing statistics, secs */

public:

	constexpr static size_t call_timeout = 1000 ;		/*!<  CORBA client side call timeout, millisecs */

	/*!
	 * Constructor for a specific partition.
	 * \param partition Name of IPC partition
	 * \param app_name Application name, used for publishing. Either given in command line for mts_worker binary, or
	 * read from TDAQ_APPLICATION_NAME environment (i.e. defined in the database)
	 * \param interval_stat flag to trigger publishing of interval statistics
	 * \throw IPC Exception if IPC partition can not be accessed
	 */
	Worker( const std::string& partition, const std::string& app_name, size_t = n_threads, bool interval_stat = false ) ;

	~Worker() ;

	/*! Implementation of TaskProvider abstract class. Called from threads in a pool.
		\ingroup public
		\return next Task (runnable) to be executed by a thread pool
		\throw NoTasks from ThreadPool if nothing to be done
	 */
	Task 		get_next_task() ; // throw(NoTasks)

	/*! Remove a misbehaving or dead receiver. Called from notification thread if Transient exception is received.
		\ingroup public
		\param rec Receiver to be removed
	 */
	void		remove_receiver ( Receiver rec ) ;

	/**
	 * Called by sending threads, just to increase counter.
	 */
	void 		message_sent() ;

	/**
	 * Called by Subscriber to indicate that there is still job to do.
	 */
	void 		awake_thread() {
					m_thrpool.awake_one() ;
				}

private:

	/**
	 * remote IDL method called by Senders
	 * \param message
	 *
	 * Cycles through all Subscribers and if Message matched the subscription criteria, places it to the Subscription delivery queue.
	 */
	void 		report(const Message& message) throw() ;

	/**
	 * Callback method registered in IS and called when a subscription is added or removed
	 * @param callback changed IS object
	 */
	void 		subscriptions_changed(ISCallbackInfo * callback) ;

	/**
	 * Called from notify() for each Subscription to check if the message is to be delivered to it.
	 * @param msg Incoming message
	 * @param subs Subscriber to check
	 * @return true if message matches and will be delivered to the subscriber
	 */
	bool		message_match(const Message& msg, Subscription& subs) throw() ;

	/**
	 * Called periodically from statistics thread
	 */
	void 		count_rate() ;

private:
	tbb::queuing_rw_mutex	m_subscr_mutex ;
	SubscriptionsList		m_subscriptions ; // list of subscribed clients
	std::set<std::string>	m_early_deleted ; // keep info in rare cases when InfoDeleted callback comes before InfoCreated
	size_t					m_subsc_counter ;
	size_t					m_total_threads ;
	std::atomic<size_t>		m_last_served_subscriber ;
	std::atomic<size_t>		m_messages_sent ; 
	std::atomic<size_t>		m_messages_received ; 
	std::atomic<size_t>		m_messages_queued ; 

	size_t					m_period ;
	bool 					m_interval_stat ;

	std::condition_variable m_thread_cond ;
	std::mutex 				m_thread_cond_mx;
	std::thread*			m_stat_thread ; // thread to periodically publish statistics in IS

	ThreadPoolNQ			m_thrpool ;	// thread pool of threads handling the subscriptions

	std::random_device 		m_rnddev ;
	std::mt19937 			m_rndgen ;
	dist_type 				m_dist ;

	std::string				m_app_name ;

} ;

}} // daq::mts

#endif
