package mts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author Lykourgos Papaevgeniou
 * 
 */


public class Sender {
	
	
	private final WorkersManager m_workers_manager;
	private final String m_partition;
	
	Sender(String partition) {
		m_partition = partition;
		m_workers_manager = new WorkersManager(new ipc.Partition(m_partition));
	}
	
	
					// build chained mts.Issue and call remote method directly
	public void report_block(final ers.Issue ers_issue) {

		final ers2idl.Issue mts_issue = ers2mtsIssue(ers_issue);

		int loopN = 0;
		
		while (true) // try until we find a working node or no working nodes...
		{
			try {
				m_workers_manager.getWorker().reference.report(mts_issue);
//				System.out.println("normal break..");
				break;
			} catch (NoWorkers ex) {				
//				System.err.println("NoWorkers exception: " + ex);
				return;
			} catch ( RuntimeException ex) {	// CORBA exceptions are Runtime
//				System.out.println("CORBA: " + ex.getMessage());
				if( loopN < 3 ){
					loopN++;
					continue;
				}
				else {		
					System.err.println("Failed to find any good MTS worker 3 times in a row, give up sending the message");
					break;
				}
			}
		}
	}
	

	/**
	 * Recursively converts ers.Issue to mts.Issue
	 * 
	 * @param ers_issue
	 *            ERS stream message
	 * @return mts.Issue new MTS Issue
	 */
	private ers2idl.Issue ers2mtsIssue(final ers.Issue ers_issue) {
		ers2idl.Issue mts_issue = null, cause = null;

		final ers2idl.RemoteContext remContext = new ers2idl.RemoteContext(ers_issue.context().cwd(),
				ers_issue.context().file_name(), ers_issue.context().function_name(),
				ers_issue.context().host_name(), ers_issue.context().user_name(),
				ers_issue.context().package_name(), ers_issue.context().application_name(),
				(int) ers_issue.context().user_id(), ers_issue.context().process_id(),
				(int) ers_issue.context().thread_id(), ers_issue.context().line_number());
		
		final String[] quals = ers_issue.qualifiers().toArray(new String[ers_issue.qualifiers().size()]);

		final List<ers2idl.ERSParameter> params = new ArrayList<ers2idl.ERSParameter>();
		
		for (Map.Entry<String, String> entry : ers_issue.parameters().entrySet()) {
		    params.add( new ers2idl.ERSParameter(entry.getKey(), entry.getValue()) );
		}
		
		String message_str = ers_issue.message() == null ? "" : ers_issue.message() ;

		if ( ers_issue.cause() != null ) {
			ers.Issue ers_cause ;
			if ( ers_issue.cause() instanceof ers.Issue )
				{
				ers_cause = ers_issue.cause() ;
				}
			else	{ // Throwable
				ers_cause = new ers.Issue(ers_issue.cause()) ;
				}

			ers_cause.setSeverity(ers_issue.severity()) ;
			cause = ers2mtsIssue(ers_cause) ; // recursion

			mts_issue = new ers2idl.Issue(ers_issue.getId(), message_str, ers_issue.ltime()*1000, // java time is millisecs, MTS/IDL and c++ is micro
					ers2mtsSeverity(ers_issue.severity()), remContext,
					params.toArray( new ers2idl.ERSParameter[params.size()] ), quals,
					new ers2idl.Issue[] {cause});
		}
		else {
			mts_issue = new ers2idl.Issue(ers_issue.getId(), message_str, ers_issue.ltime()*1000,
					ers2mtsSeverity(ers_issue.severity()), remContext,
					params.toArray( new ers2idl.ERSParameter[params.size()] ), quals,
					new ers2idl.Issue[]{});
		}


		return mts_issue;
	}

	/**
	 * Maps ers.Severity enum to mts.Severity generated from mts.idl
	 * 
	 * @param ers.Severity.level
	 * @return mts_sev
	 */
	private static ers2idl.Severity ers2mtsSeverity(final ers.Severity ers_sev) {
		// ers&mts severities in order: "DEBUG", "LOG", "INFORMATION", "WARNING", "ERROR", "FATAL"
		
		switch (ers_sev.level()) {
			case Debug:
				return ers2idl.Severity.debug;
			case Log:
				return ers2idl.Severity.log;
			case Information:
				return ers2idl.Severity.information;
			case Warning:
				return ers2idl.Severity.warning;
			case Error:
				return ers2idl.Severity.error;
			case Fatal:
				return ers2idl.Severity.fatal;
			default:
				System.err.println("Unexpected integral ers.Severity received from ERS srteam: "
								+ ers_sev.toString() + " falling back to mts.Log");
				return ers2idl.Severity.log;
		}
	}
	
}
