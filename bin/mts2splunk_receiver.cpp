/*! \file mts2splunk_receiver
 * Command-line C++ ERS receiver using MTS transport which dumps received ERS issues into files for asyncronous SPLUNK indexing.
 * Incoming ERS issues are dumped to temporary /tmp/ers.PID.out file which is being closed and moved to /data/4splunk/$partition.$timestamp.out file for splunk.
 * This is done by async thread eveny N seconds (parameter of the app).
 * Files in are /data/4splunk/ asyncrounously monitored, indexed and removed by splunk.
 * \author Andrei Kazarov
 * \date 2016
 *
 */

/* 
SPLUNK expects ERS issues to be formatted like e.g.

t=1463153399, rn=299119, part=IBL-DAQSlice, uname=crpixel, msgID=rc::ApplicationSignaled, host=pc-pix-fit-02.cern.ch, app=Ctrl_PixelIBL, sev=WARNING,
text="Application "Appl_PixelRCD-IBL" on host "sbc-pix-rcc-i-1.cern.ch" died while exiting on signal 9.",
context="PACKAGE_NAME: RunControl. FILE_NAME: ../src/Controller/ApplicationController.cxx. FUNCTION_NAME: daq::rc::ApplicationController::pmgCallback(const std::weak_ptr<daq::rc::ApplicationController>&, daq::pmg::Process*, void*)::<lambda()>. LINE_NUMBER: 182. DATE_TIME: 1463153399.",
params="application: Appl_PixelRCD-IBL. errStream: /detwork/pix/logs/tdaq-06-01-01/IBL-DAQSlice/Appl_PixelRCD-IBL_sbc-pix-rcc-i-1.cern.ch_1463151401.err. hostName: sbc-pix-rcc-i-1.cern.ch. onExit: 1. outStream: /detwork/pix/logs/tdaq-06-01-01/IBL-DAQSlice/Appl_PixelRCD-IBL_sbc-pix-rcc-i-1.cern.ch_1463151401.out. signal: 9. ",
quals="RunControl PIXEL ", chained="0", gh=123244, T0=data_test, LB=45, Fill=0, R4P=0, SB=0

NB:
the order is important! New fields must be added to the end (after gh).
Fileds are extracted and indexed by splunk ers application, configuration is in 
/opt/splunk/etc/apps/ers/default/props.conf and transforms.conf

d.quotes inside d.quotes is OK for splunk, no need to escape.

chained = 0 means no nested
= 1 this is a top-level message in a chain
= 2 this is a nested message
*/


#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <functional>
#include <condition_variable>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/prctl.h>

#include <boost/program_options.hpp>
#include <boost/crc.hpp>
#include <boost/scope_exit.hpp>

#include <ers/ers.h>
#include <ers/Issue.h>
#include <ipc/core.h>
#include <is/infodictionary.h>
#include <is/infodynany.h>
#include <is/inforeceiver.h>
#include <is/callbackinfo.h>
#include <is/criteria.h>
#include <is/exceptions.h>

#include <ddc/DdcIntInfo.h>
#include <rc/Ready4PhysicsInfo.h>

using std::chrono::system_clock;

void sig_func(int) {}

pthread_t main_tid ;

namespace po = boost::program_options ;

ERS_DECLARE_ISSUE(      mts,
                        WrongProgramOptions,
                        "Wrong program options passed: " << text,
                        ((std::string)text )
                )


// where to find things in IS
// const std::pair<std::string, std::string> lumiblock_is_source ("RunParams.LumiBlock", "LumiBlockNumber") ;
const std::pair<std::string, std::string> tier0tag_is_source  ("RunParams.RunParams", "T0_project_tag") ; // 
const std::pair<std::string, std::string> runnumber_is_source ("RunParams.RunParams", "run_number") ; // RunParams.run_number u32
const std::pair<std::string, std::string> lumiblock_is_source ("RunParams.LumiBlock", "LumiBlockNumber") ;
const std::pair<std::string, std::string> SB_is_source ("LHC.StableBeamsFlag", "value") ; // initial partition, DdcIntInfo.value
const std::pair<std::string, std::string> fillnumber_is_source ("LHC.FillNumber", "value") ; // initial partition, DdcIntInfo.value
const std::pair<std::string, std::string> R4P_is_source ("RunParams.Ready4Physics", "ready4physics") ; // Ready4PhysicsInfo.ready4physics, bool

const ISCriteria sb_subscription_criteria = ddc::DdcIntInfo::type() && "StableBeamsFlag" ;
const ISCriteria r4p_subscription_criteria = Ready4PhysicsInfo::Ready4PhysicsInfo::type() && "RunParams" ;

using namespace is ;

struct MyReceiver : public ers::IssueReceiver
{
    std::mutex			m_file_mutex ;
    std::ofstream		m_temp_file ;					/** file in tmp for buffering messages */
    std::atomic<int> 	m_global_counter ;
    size_t				m_dump_period ; 				/*!<  Interval for moving files to splunk place */
    std::condition_variable 	m_thread_cond ;
    std::mutex 			m_thread_cond_mx ;
    std::mutex 			m_t0tag_mx ;
    std::string			m_splunk_area ;
    std::string			m_part_name ;
    std::string			m_drop_msgid ;
    std::string			m_drop_appid ;
    std::string			m_tmp_filename ;
    std::thread			m_out_thread ;
    std::atomic<unsigned int> 	m_run_number ;		// RN: either from IS or from ERS message from RootController
    std::atomic<unsigned int> 	m_fill_number ;		// RN: either from IS or from ERS message from RootController
    std::string			m_tier0tag ;				// Tier0 project tag from IS, get it once at Start of run
    std::atomic<unsigned int>	m_R4P_flag ;				// ready for physics flag from IS
    std::atomic<unsigned int>	m_SB_flag ;					// Stable Beams flag from IS
    std::atomic<unsigned int>	m_lumiblock ;		// LB number from particular ERS L1CT message

	ISInfoReceiver 		m_r4p_receiver ;
	ISInfoReceiver 		m_sb_receiver ;
	ISInfoReceiver 		m_fillnumber_receiver ;

	void r4p_changed(ISCallbackInfo * callback) {
		Ready4PhysicsInfo newinfo ;
		try {
			callback->value( newinfo ) ;
			}
		catch ( const daq::is::Exception & ex  ) {
			ers::error(ex) ;
			return ;
		}

		switch ( callback->reason() ) {
			case  is::reason::Created:
			case  is::reason::Subscribed:
			case  is::reason::Updated:
				m_R4P_flag = newinfo.ready4physics ;
			default: ;
		}
	}
	
	void sb_changed(ISCallbackInfo * callback) {
		ddc::DdcIntInfo newinfo ;
		try {
			callback->value( newinfo ) ;
			}
		catch ( const daq::is::Exception & ex  ) {
			ers::error(ex) ;
			return ;
		}

		switch ( callback->reason() ) {
			case  is::reason::Created:
			case  is::reason::Subscribed:
			case  is::reason::Updated:
				m_SB_flag = newinfo.value ;
			default: ;
		}
	}

	void fillnumber_changed(ISCallbackInfo * callback) {
		ddc::DdcIntInfo newinfo ;
		try {
			callback->value( newinfo ) ;
			}
		catch ( const daq::is::Exception & ex  ) {
			ers::error(ex) ;
			return ;
		}

		switch ( callback->reason() ) {
			case  is::reason::Created:
			case  is::reason::Subscribed:
			case  is::reason::Updated:
				m_fill_number = newinfo.value ;
			default: ;
		}
	}

    // constructor, also startm_fill_numberzs async dump thread
    MyReceiver( const std::string& part, size_t dump_period, const std::string& splunk_area, const std::string& dmi, const std::string& dai):
	m_global_counter(0), m_dump_period(dump_period), m_splunk_area(splunk_area), m_part_name(part),
	m_drop_msgid(dmi), m_drop_appid(dai),
	m_tmp_filename(splunk_area + "/tmp/ers.out." + part + "." + std::to_string(::getpid())), 
	m_out_thread(std::bind(&MyReceiver::move_to_splunk, this)),
	m_run_number(0), m_fill_number(0), m_R4P_flag(0), m_SB_flag(0), m_lumiblock(0),
	m_r4p_receiver(part), m_sb_receiver("initial"), m_fillnumber_receiver("initial")
	{
	try {
		m_r4p_receiver.subscribe( R4P_is_source.first, &MyReceiver::r4p_changed, this,
			{ISInfo::Created, ISInfo::Updated, ISInfo::Deleted, ISInfo::Subscribed}  ) ; // throw daq::is::Exception
	} catch ( const ers::Issue& ex ) {
		ers::error(ex) ;
	} ;

	try {
		m_sb_receiver.subscribe( SB_is_source.first, &MyReceiver::sb_changed, this,
			{ISInfo::Created, ISInfo::Updated, ISInfo::Deleted, ISInfo::Subscribed}  ) ; // throw daq::is::Exception
	} catch ( const ers::Issue& ex ) {
		ers::error(ex) ;
	} ;
	try {
		m_fillnumber_receiver.subscribe( fillnumber_is_source.first, &MyReceiver::fillnumber_changed, this,
			{ISInfo::Created, ISInfo::Updated, ISInfo::Deleted, ISInfo::Subscribed}  ) ; // throw daq::is::Exception
	} catch ( const ers::Issue& ex ) {
		ers::error(ex) ;
	} ;

	ERS_DEBUG(0, "Constructed MyReceiver") ;
	}

    ~MyReceiver()
	{
	ERS_DEBUG(0, "Destructing MyReceiver, waiting for m_out_thread to join") ;
	std::unique_lock<std::mutex> tlock(m_thread_cond_mx) ;
	m_thread_cond.notify_all() ;
	tlock.unlock() ;

	m_out_thread.join() ;
	ERS_LOG("Exiting Splunk ERS receiver, total messages received: " << m_global_counter);

	    	// cleanup if any
    if (  m_splunk_area.compare("stdout") && ::unlink(m_tmp_filename.c_str()) )
		{
		ERS_LOG("Failed to remove " << m_tmp_filename) ;
		::perror("Failed to remove temporary file") ;
		}
	else 	{
		ERS_LOG("Removed temporary file " << m_tmp_filename) ;
		}

	try {
		m_fillnumber_receiver.unsubscribe(fillnumber_is_source.first) ;
		m_sb_receiver.unsubscribe(SB_is_source.first) ;
		m_r4p_receiver.unsubscribe(R4P_is_source.first) ;
	} catch ( const ers::Issue& ex ) {
		ers::error(ex) ;
	}

	}

	// ers::IssueReceiver::receive()
    void receive( const ers::Issue & issue )
    {
	if ( m_drop_appid == issue.context().application_name() || m_drop_msgid == issue.get_class_name() ) {
		ERS_DEBUG(1, "Dropping message " << m_drop_msgid << " from application " << m_drop_appid) ;
		return ;
	}
	get_run_number(issue) ;
	if ( m_run_number == 0 )  { m_run_number = get_run_number_is() ; } ; 	// keep trying to get RN from IS
	if ( m_tier0tag.empty() ) { std::unique_lock<std::mutex> flock(m_t0tag_mx) ; m_tier0tag = get_tier0_is() ; } ; 		
	++m_global_counter ;
	// size_t group_hash = 0 ;
	std::ostringstream tmpstream(issue.message()) ;
	tmpstream << issue.context().process_id() << issue.time_t() << issue.context().application_name() << issue.context().host_name() << rand() ;
	std::string tmp = tmpstream.str() ;
	// build a unique hash for a group of nested issues
	// std::hash may be too long
	// std::hash<std::string> hash ;
	// group_hash = hash(tmp.str()) ;
	// let's use boost_crc32
	boost::crc_32_type crc32 ;
	crc32.process_bytes(tmp.c_str(), tmp.length());

	{ std::unique_lock<std::mutex> flock(m_file_mutex) ;

	if ( !m_temp_file.good() ) // protection for may be too early messages if periodic thread has not yet created a tmp file
		{
		ERS_LOG("Tmp file " << m_tmp_filename << " is not ready, skipping this message");
		return ; // 
		}

	if ( issue.cause() != nullptr )
		{ print_issue(issue, crc32.checksum(), 1, ers::to_string(issue.severity()), m_tier0tag, m_lumiblock) ; }
	else
		{ print_issue(issue, crc32.checksum(), 0, ers::to_string(issue.severity()), m_tier0tag, m_lumiblock) ; }
	} // output file lock
    }

	// recursive function to print out group of nested issues.
	// hash, chained and sev are common attributes for a group
    void print_issue(const ers::Issue& issue, size_t group_hash, size_t chained, const std::string& sev, const std::string& t0tag, unsigned int lbn)
	{
	// first print chained issues, for proper ordering in splunk: it seems to read from the end of the file...
	if ( issue.cause() != nullptr )
		{ print_issue(*issue.cause(), group_hash, 2, sev, t0tag, lbn) ; } // chained issue inherits severity from parent

	std::ostream& out = m_splunk_area == "stdout" ? std::cout : m_temp_file ;
	
	out << "t=" << issue.time_t()
	<< ", rn=" << m_run_number << ", part=" << m_part_name << ", uname=" << issue.context().user_name() << ", msgID=" << issue.get_class_name()
	<< ", host=" << issue.context().host_name() << ", app=" <<  issue.context().application_name() << ", sev=" << sev 
	<< ", text=\"" << issue.message() << "\", context=\"" << "PACKAGE: " << issue.context().package_name()
	<< " FILE: " << strip_filename(issue.context().file_name(), issue.context().package_name()) 
	<< " FUNCTION: " << strip_function(issue.context().function_name()) << " LINE: " << issue.context().line_number() 
	<< "\", params=\"" ; 
	for ( auto aparam: issue.parameters() ) 
		{ out << aparam.first << ": " << aparam.second << " " ; } ;
	out << "\", quals=\"" ;
	for ( auto aqual: issue.qualifiers() ) 
		{ out << aqual << " " ; } ;
	out << "\", chained=" << chained << ", gh=" << group_hash << ", T0=" << t0tag << ", LB=" << lbn 
		<< ", Fill=" << m_fill_number << ", R4P=" << m_R4P_flag << ", SB=" << m_SB_flag << std::endl ;
	}

    const std::string strip_filename(const std::string& origin, const std::string& package_name)
	{
		std::size_t pos = origin.find("/"+ package_name + "/") ;
		if ( pos != std::string::npos ) 
			return origin.substr(pos + package_name.length() + 2) ;
		else
			return origin ;
	}

/*
FUNCTION_NAME: daq::rc::ApplicationController::pmgCallback(const std::weak_ptr<daq::rc::ApplicationController>&, daq::pmg::Process*, void*)::<lambda()>.
LINE_NUMBER: 182.
DATE_TIME: 1520850723.
*/
    const std::string strip_function(const std::string& origin)
	{
		std::size_t pos = origin.find("(") ;
		if ( pos != std::string::npos ) 
			return origin.substr(0,pos) ;
		else
			return origin ;
	}
 
    // thread to close and move file from /tmp to m_splunk_area
    void move_to_splunk()
    {
	if ( m_splunk_area == "stdout" ) { ERS_LOG("Exiting from periodic thread, no file to write to") ; return  ;} 

	BOOST_SCOPE_EXIT(&main_tid) // called when thread is exited e.g. by an error
   	{
		ERS_DEBUG(0, "Exiting from periodic thread") ;
		pthread_kill(main_tid, SIGTERM) ;
   	}BOOST_SCOPE_EXIT_END

	ERS_DEBUG(0, "Periodic thread started") ;
	bool to_exit = false ;

    while (true) { 	
	    // nothing else to do
	    if ( to_exit )  {
			ERS_DEBUG(0, "Moved the last file, exiting") ;
			return ;
		}

		{ 		// open new temp file
		std::unique_lock<std::mutex> flock(m_file_mutex) ;
		if ( !m_temp_file.is_open() ) {
			m_temp_file.open(m_tmp_filename) ;
			ERS_DEBUG(0, "Created tmp file " << m_tmp_filename) ;
		}
		if ( !m_temp_file.good() ) {
			std::cerr << "Fatal: failed to open file " << m_tmp_filename << std::endl ;
			std::perror("Failed to open file");
			raise(SIGTERM) ; // exit application
			return ; // exit thread
		} }

	  // safely sleep and wait for a signal to stop the thread	
		std::unique_lock<std::mutex> lock(m_thread_cond_mx) ;
		if ( m_thread_cond.wait_for(lock, std::chrono::seconds(m_dump_period )) == std::cv_status::no_timeout ) { 
			ERS_DEBUG(0, "Got signal, cleaning up and exiting periodic thread") ;
			to_exit = true ; // delay the exit until the last file is moved
		}

	  	{ std::unique_lock<std::mutex> flock(m_file_mutex) ; 
	    
	    // if nothing has been written to the biffer yet, do nothing
 	    if ( m_temp_file.tellp() == 0 ) {
			ERS_DEBUG(4, "Nothing to write, continue loop") ;
			continue ;
		}

	    ERS_DEBUG(2, "buffer size: " << m_temp_file.tellp()) ;

	    m_temp_file.close() ;
	    if ( m_temp_file.fail() ) {
			std::cerr << "Failed to close file " << m_tmp_filename << std::endl ;
			std::perror("Failed to close file");
			// return ;
			// try to continue, if file can not be moved or open, then exit
		}
	    std::ostringstream outfilename ;
            outfilename << m_splunk_area << "/" << m_part_name << "." << std::time(0) ;
            ERS_DEBUG(1,"Moving " << m_tmp_filename.c_str() << " to " << outfilename.str().c_str()) ;

	    if ( ::rename(m_tmp_filename.c_str(), outfilename.str().c_str()) ) {
			char err[256] ;
			std::cerr << "Failed to move file " << m_tmp_filename << " to " << outfilename.str() << ": " << strerror_r(errno,err,sizeof(err)) << std::endl ;
			raise(SIGTERM) ; // exit application
			return ;
		}

	  } // out file lock 

    } // while

    ERS_DEBUG(0, "Exiting from periodic thread") ;
    } // move_to_splunk

    void get_run_number(const ers::Issue& issue)
    	{
	if (0 == std::string(issue.get_class_name()).compare("rc::StartOfRun"))
		{
		std::stringstream tmp;
		tmp << issue.what();
		unsigned int runNumber;
		tmp >> runNumber;
		m_run_number = runNumber ;
    		ERS_LOG("Got new RunNumber from Root Controller: " << m_run_number) ;

	    	// should be actual and available
		{ std::unique_lock<std::mutex> flock(m_t0tag_mx) ; m_tier0tag = get_tier0_is() ; }
    		}
	else 
/*
Message ID: L1CT::LumiBlockChange
Message: Performed luminosity block update, lumi block is now 21
*/
	if (0 == std::string(issue.get_class_name()).compare("L1CT::LumiBlockChange"))
		{
		std::size_t pos = std::string(issue.what()).find("is now") ;
		if ( pos != std::string::npos )
			{
			std::stringstream tmp;
			tmp << std::string(issue.what()).substr(pos+6) ;
			unsigned int lbn;
			tmp >> lbn;
			m_lumiblock = lbn ;
    			ERS_DEBUG(0, "Got new LB from CtpController: " << m_lumiblock) ;
			}
		else
			{ // must be an error?
			ERS_LOG("Failed to parse L1CT::LumiBlockChange message while looking for new LB number") ;
			}
    		}
    	}

    unsigned int get_fill_number_is() const
	{
	unsigned int isRunNumber = 0 ;
	IPCPartition p("initial") ;
 	ISInfoDictionary dict(p) ;
	ISInfoDynAny info ;

	try {	dict.getValue(fillnumber_is_source.first, info) ;
			isRunNumber = info.getAttributeValue<double>(fillnumber_is_source.second) ;
			ERS_LOG("Got Fill Number from IS: " << isRunNumber) ;
   		}
   	catch (daq::is::RepositoryNotFound& issue) {
      		ERS_LOG("IS Repository " << fillnumber_is_source.first << " not yet available, can not get latest Fill number.") ;
   		}
   	catch (daq::is::Exception& ex) {
      		ERS_LOG("Something unexpected happended when getting Fill number fom IS: " << ex << ", will try again later!") ;
   		}

	return isRunNumber;
	}

    unsigned int get_run_number_is() const
	{
	unsigned int isRunNumber = 0 ;
	IPCPartition p(m_part_name) ;
 	ISInfoDictionary dict(p) ;
	ISInfoDynAny info ;

	try {	dict.getValue(runnumber_is_source.first, info) ;
		isRunNumber = info.getAttributeValue<unsigned int>(runnumber_is_source.second) ;
		ERS_LOG("Got RunNumber from IS: " << isRunNumber) ;
   		}
   	catch (daq::is::RepositoryNotFound& issue) {
      		ERS_LOG("RunParams not yet available, can not get latest RunNumber.") ;
   		}
   	catch (daq::is::Exception& ex) {
      		ERS_LOG("Something unexpected happended when getting RN fom IS: " << ex << ", will try again later!") ;
   		}

	return isRunNumber;
	}

		// at start of the run also get T0project tag from IS

    	std::string get_tier0_is() const
		{
		IPCPartition p(m_part_name) ;
	 	ISInfoDictionary dict(p) ;
		ISInfoDynAny info ;

		try 	{
			dict.getValue(tier0tag_is_source.first, info) ;
			std::string t0tag = info.getAttributeValue<std::string>(tier0tag_is_source.second) ;
			ERS_LOG("Got T0tag from IS: " << t0tag) ;
			return t0tag ;
	   		}
   		catch (daq::is::RepositoryNotFound& issue) {
      			ERS_LOG("RunParams IS not yet available, can not get latest T0Tag.") ;
   			}
   		catch (daq::is::Exception& ex) {
      			ERS_LOG("Something unexpected happended when getting T0tag fom IS: " << ex << ", will try again later!") ;
   			}

      		ERS_LOG("Unexpected: get value from IS") ;
		return "" ;
		}

};

int main(int argc, char** argv)
{
    main_tid = pthread_self() ;

    std::list< std::pair< std::string, std::string > > opt;
    try {
        opt = IPCCore::extractOptions( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::error( ex );
        return 2;
    }

//    opt.push_front( std::make_pair( std::string("maxListeningThreadsPerEndpoint"), std::string("2") ) );	// # of threads for connection polling
    opt.push_front( std::make_pair( std::string("threadPerConnectionPolicy"), std::string("0") ) );
    opt.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), std::string("8") ) );
//    opt.push_front( std::make_pair( std::string("threadPoolWatchConnection"), std::string("0") ) );
//    opt.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("5000") ) );
//    opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("30") ) );

    try {
		IPCCore::init(opt);    
	}
    catch( daq::ipc::Exception & ex ) {
        ers::error( ex );
        return 1;
    }

    // command line options and default values
    std::string subscription = "*", partition, splunk_area, drop_msgid, drop_appid ;
    size_t dump_interval = 5 ;

    po::variables_map vm ;
    po::options_description desc("ERS (MTS) to SPLUNK receiver application. Used to receive ERS issues and to dump them in SPLUNK format to a files for SPLUNK indexing.") ;

	try {
		desc.add_options()
			("subscription,s",	po::value<std::string>(&subscription), 		"subscription, e.g. * or 'sev=ERROR and not qual=Tile'")
			("partition,p",		po::value<std::string>(&partition), 		"IPC partition")
			("splunk_path,i",	po::value<std::string>(&splunk_area), 		"path to SPLUNK indexation area, in case of 'stdout', no files created and messages dumped to stdout")
			("interval,t",		po::value<size_t>(&dump_interval)->default_value(dump_interval),	"interval for sending files to splunk, seconds")
			("drop_msgid,m",	po::value<std::string>(&drop_msgid), 		"drop certain messgages by msgId field")
			("drop_appid,a",	po::value<std::string>(&drop_appid), 		"drop certain messgages by application name field")
			("die,d", 		"Get signalled (SIGTERM) when my parent (e.g. a calling shell script) process terminates") ;
			("help,h", 		"Print help message") ;

		po::store(po::parse_command_line(argc, argv, desc), vm) ;
		po::notify(vm) ;
		}
	catch( std::exception& ex )
		{
		ers::error( mts::WrongProgramOptions(ERS_HERE, argv[1], ex) );
		return 0 ;
		}

 	if ( vm.count("help") )
 		{
 		std::cout << desc << std::endl ;
 		return 0 ;
 		}

	if ( vm.count("die") )
 		{
 		int res = prctl(PR_SET_PDEATHSIG, SIGTERM) ;
		if ( res ) { 
			std::cout << "Failed to subscribe to parent exit signal" << std::endl ;
			return 1 ;
		}
		}

	if ( partition.empty() && !std::getenv("TDAQ_PARTITION") )
		{
		std::cerr << "partition name neither specified in program options nor defined in TDAQ_PARTITION environment." << std::endl ;
 		std::cout << desc << std::endl ;
		return 1 ;
		}

	if ( splunk_area.empty() )
		{
		std::cerr << "splunk path must be defined." << std::endl ;
 		std::cout << desc << std::endl ;
		return 1 ;
		}
 
    try {
	std::unique_ptr<MyReceiver> rec(new MyReceiver(partition, dump_interval, splunk_area, drop_msgid, drop_appid)) ;
	try	{
	    	ers::StreamManager::instance().add_receiver( "mts", { partition, subscription} , rec.get() );
		}
	catch ( const ers::InvalidFormat& ex )
		{
    		std::cerr << "Failed to add ERS receiver for 'mts' stream:\n" << ex << std::endl ;
		return 1 ;
		}
    	ERS_DEBUG(0, "Receiver registered in MTS, waiting for messages") ;

	sigset_t set ;
	sigemptyset(&set) ;
	sigaddset(&set, SIGINT) ;
	sigaddset(&set, SIGQUIT) ;
	sigaddset(&set, SIGTERM) ;
        signal(SIGINT, sig_func) ;
        signal(SIGQUIT, sig_func) ;
        signal(SIGTERM, sig_func) ;

   	// just block the application until a signal is received
    	int sig ;
	sigwait(&set, &sig) ;
	ERS_DEBUG(0, "Main thread stopped, cleaning up and exiting ERS recevier, signal: " << sig) ;

	// we've done, clean up
    	ers::StreamManager::instance().remove_receiver(rec.get()) ; // will wait for periodc thread in destructor of MyReceiver
	}
    catch ( ers::Issue& issue )
	{
    	std::cerr << "Failed to add ERS receiver for 'mts' stream:\n" << issue << std::endl ;
	return 1 ;
	}

    return 0; 
}
