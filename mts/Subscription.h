/*! \file Subscription.h
 *  Defines main class for implementation of the Subscription, list of which is stored in Worker.
 *
*  \author A. Kazarov 
*  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
*  \date   Nov. 2012
*/

#ifndef MTS_SUBSCRIPTION_H
#define MTS_SUBSCRIPTION_H

#include <mutex>
#include <chrono>
#include <string>
#include <atomic>
#include <deque>

#include "mts/mts.hh"
#include "mts/defs.h"
#include <mts/SubscriptionParser.h>

namespace daq {
namespace mts {

class Worker ; // forward declaration

/**
 * \class Subscription
 * \brief Represents a remote client subscribed to the messages with some criteria.
 * \ingroup mts
 *
 * Subscription is the end point of MTS notification chain on a worker. Each MTS receiver client (log manager, controller) subscribes to MTS
 * using a subscription criteria with pre-defined syntax, defined in high-level design document.
 *
 * Each MTS worker holds a list of active Subscriptions (i.e. subscriber clients).
 * Each %Subscription contains a buffer (queue) to store all messages selected for delivery to this subscriber.
 *
 * Selection criteria syntax allows to subscribe to certain messages specifying selection expressions using following ERS Issue attributes:
 * severity (sev), applicationID (app), messageID (msg), qualifiers (qual), parameters (par) and context (context),
 * grouping of expressions wiith parentheses and arbitrary logical combination (and, or, not) of them.
 * The syntax is defined using <a href="http://www.cs.cmu.edu/~pattis/misc/ebnf.pdf">EBNF notation</a>:
 *
\code
    key = 'app' | 'msg' | 'qual'
    sev = 'sev'
    par = '(param|par)(+[a-zA-Z_-])'
    context_item = 'app' | 'host' | 'user' | 'package' | 'file' | 'function'
    context = 'context(context_item)'
    sevlev = 'fatal' | 'error' | 'warning' | 'info' | 'information'
    token_wildcard = +[a-zA-Z_:\-]-'*'
    token = +[a-zA-Z_:\-]
    item = (key ('=' | '!=') token_wildcard) | (sev ('=' | '!=') sevlev) | (par ('=' | '!=') token) | (context ('=' | '!=') token)
    factor = item | ( expression ) | ('not' factor)
    expression = '*' | factor *(('and' expression) | ('or' expression))
\endcode

 *
 * Keys and logical operators are case-insensitive.
 * Simple wildcard string comparision supported for app, msg and qual tokens (like app=HLT*). File names are short (no full path).
 * Example:
 * \verbatim(app=HLT* and (not (param(algorithm)=muons and context(file)=test.cpp)) or sev=fatal or msg=rc::*)\endverbatim

 * The syntax parsing is done by boost::spirit framework, see SubscriptionParser.h.
 *
 */
class Subscription {
friend class Worker ;

enum Priority {  Dead, Slow, Busy, Normal } ;	/*!< Priority of this subscriber, used by scheduler to make fair allocation*/

public:

	Subscription (const std::string& expr, const std::string& is_name, ReceiverP rec, Worker* wrk) ;
	~Subscription () ;

	/*!
	 * Function to define if the Messages is matched the subscription criteria. If yes, added to the delivery queue. Called by the reporter (inbound) threads.
	 * \param message to match
	 * \return true if Message matched, false otherwise
	 */
	bool			is_matched( const Message& message ) ;

	bool			operator==( const Subscription& another) const
					{ return m_is_object == another.m_is_object ; } ;

	bool			operator==( const std::string& another) const
					{ return m_is_object == another ; } ;

private:

	/*! Send a single message to the receiver
	 *
	 *  A real remote CORBA call to receiver. To be called from thread pool.
	 *  \warning May block, depending on receiver state.
	 */
	void		notify_receiver ( ) ;

	/*!
	 * Adds a pointer to Message to the end of delivery queue for this Subscription.
	 *
	 * Called by Worker input thread.
	 * The message will be picked up by TaskProvider scheduler at a good moment. Non-blocking call.
	 * \param msg shared pointer to Message
	 */
	void		enqueue_message ( MessageSP msg ) ;

	/*!
	 * Evaluate the status (priority) of the Subscriber. Called after each invocation of notify_receiver()
	 * or from Timeout exception.
	 *
	 * \param delay microseconds taken to talk to this subscriber, -1 in case of CORBA timeout
	 */
	void		evaluate_priority ( int delay ) ;

	/*!
	 * Called by worker when it selects next Subscriber to serve.
	 * The more delay, the less priority (= less threads) is given to the Subscriber.
	 *
	 * \return average delay of this worker in milliseconds
	 */
	int			get_average_delay () {
		std::lock_guard<std::mutex> lock(m_eval_mutex) ;
		return (int)m_average_delay ;
	}

	//Priority	get_priority()	{ }

	std::atomic<Priority>		m_priority ;		// priority of the receiver
	Receiver					m_receiver ;		// CORBA destination for this Subscription
	Worker*						m_worker ;		// hold a ref to Worker which is common for all Subscriptions
	std::string					m_expression ;		// logical subscription expression, passed from client
	std::string					m_is_object ;		// key in IS repository, where information about subscriber is shared
	MessagesQueue				m_messages_queue ; 	// queue of smart pointers to avoid extra copying
	std::atomic<bool>			m_is_active ; 		// flag to remove subscriber
	std::atomic<bool>			m_is_slow ; 		// flag of slow subscriber
	std::atomic<unsigned int>	m_queue_counter ; 		// size of the queue
	std::atomic<size_t>			m_thread_counter ; 	// number of threads serving this subscription, to help handling slow subscribers
	std::atomic<unsigned int>	m_messages_sent ;	// total number of sent messages
	std::atomic<unsigned int>	m_messages_enqueued ;	// total number of queued messages

	double						m_average_delay ;	// average delay in communication, milliseconds, just used for statistics
	double						m_total_delay ;		// total delay for last N messages, used for calculation of average
	std::mutex 								m_eval_mutex ;
	std::mutex 								m_spirit_mutex ;		// mutex to protect shared parser when it is called for new msg
	std::chrono::system_clock::time_point	m_last_timeout ;		// when last timeout was observed
	const int 					s_timeout_reset = 60 ; 	// time to reset Slow state, if no TIMEOUT received within this period

	MessageFilter 			m_filter ; 			// callable object for passing the concrete message to parser
	SubscriptionParser<std::string::const_iterator> m_parser ; // parser object, created once and used in matching function

} ;

}} // daq::mts 
#endif
