#include <string>
#include <regex>
#include <memory>
#include <map>

#include <ers/ers.h>
#include <ipc/core.h>
#include <is/infodictionary.h>

#include "mts/MTSSubscriptionInfo.h"
#include "mts/MTSReceiverStat.h"
#include "mts/MTSWorkerStat.h"
#include "mts/Worker.h"

/* CORBA Timeouts in omniORB
namespace omniORB {
void setClientCallTimeout(CORBA::ULong millisecs);
*/

using namespace daq::mts ;

Worker::Worker( const std::string& partition, const std::string& app_name, size_t threads, bool interval_stat )
:
IPCNamedObject<POA_mts::worker> (partition, app_name),
SubscriptionManager(partition),
m_subsc_counter(0),
m_total_threads(threads), m_last_served_subscriber(0), m_messages_sent(0),
m_messages_received(0), m_messages_queued(0), m_period(stat_period), m_interval_stat(interval_stat),
m_thrpool(threads, *this, true), m_rndgen(m_rnddev()),
m_dist(10),m_app_name(app_name)
//m_dist({0, call_timeout/4}, [](double x) { return 1/(x+1); })
{
ERS_DEBUG(3, "Constructing Worker") ;

// set timeout to 2 seconds
omniORB::setClientCallTimeout(call_timeout) ;

m_stat_thread = new std::thread(std::bind(&Worker::count_rate, this)) ;

// subscribe in IS for mts::SubscriptionInfo
get_subscriptions( &Worker::subscriptions_changed, this ) ; // throw is::Exception

ERS_DEBUG(3, "Constructed Worker") ;
}

Worker::~Worker() 
{
ERS_DEBUG(3, "Destructing Worker") ;

std::unique_lock<std::mutex> tlock(m_thread_cond_mx) ;
m_thread_cond.notify_all() ;
tlock.unlock() ;
m_stat_thread->join() ;
delete m_stat_thread ;

try	{
	withdraw() ;
	}
catch 	( const std::exception& ex ) // ORB does not like exceptions in servants destructors
	{
	ERS_LOG("Exception ignored in Worker destructor: " << ex.what() ) ;
	}

tbb::queuing_rw_mutex::scoped_lock slock(m_subscr_mutex, true) ;

// SubscriptionManager::
unsubscribe_worker() ;

// TODO ?to remove subscription stats for IS or better leave it for experts?
m_subscriptions.clear() ;
m_early_deleted.clear() ;
ERS_DEBUG(1, "Messages left queued:\t" << m_messages_queued);
ERS_DEBUG(1, "Total messages sent:\t" << m_messages_sent);
ERS_DEBUG(1, "Total messages received:\t" << m_messages_received);
ERS_DEBUG(3, "Destructed Worker") ;
}

// IDL method called from Senders
// must not block on a network operations
// it creates sh.pointer to a copy of incoming message, 
// calls matching method and places it in a local queue of subscriber
void 	Worker::report(const Message& message) throw() 
{
ERS_DEBUG(3, "Got message " << message.ID ) ;
m_messages_received++ ;

// copy the message and create shared pointer to it which is to be stored in subscriptions as needed
MessageSP msgp = std::make_shared<Message>(message) ;

// prevent concurrent report and subscribe/unsubscribe, read lock
tbb::queuing_rw_mutex::scoped_lock lock(m_subscr_mutex, false) ;

ERS_DEBUG(3, "Subscribers to be served: " << m_subscriptions.size() ) ;

// TODO can be parallelized
for ( auto subs = m_subscriptions.begin(); subs != m_subscriptions.end(); subs++ ) 
	{
	ERS_DEBUG(3, "Serving subscriber " << (*subs)->m_is_object) ;
 	if ( message_match(message, *(*subs)) )		// message to be delivered to this subscriber
	//if ( message_match(message, (*subs).second->m_rexpression) )
		{
		ERS_DEBUG(3, "Message matched subscription, adding to the subscriber delivery queue") ;
		(*subs)->enqueue_message(msgp) ;
		m_messages_queued++ ;
		ERS_DEBUG(5, "Waking up a thread") ;
		m_thrpool.awake_one() ;
		}
	}
}
//		ERS_DEBUG(0, "Subscriber still has " << (*subs).second->m_thread_counter << " active threads") ;

// Worker::get_next_task(): called from thread pool as soon as there is an empty slot
// most of intelligence of handling slow subscriber is here

// selects a next Subscriber to be handled, prepares runnable Task and returns it to the thread
// a simple round-robin guarantees that each Subscriber gets at least equal amount of thread pool resources
// Subscriber may be given more then one thread (if there is a free thread), however there is a check
// that any subscriber is not given too many threads
// throw NoTasks makes pool to sleep
Task Worker::get_next_task() 
{
bool loop_completed = false ;
while ( true ) // loop through subscribers to find something to send to
try {
	SubscriptionSP subs ;

	{	tbb::queuing_rw_mutex::scoped_lock lock(m_subscr_mutex, false) ;
		size_t size = m_subscriptions.size() ;
		if ( size == 0 ) // no subscribers
			{ throw NoTasks() ; }
		size_t p = m_last_served_subscriber++ % size ;
		if ( p == 0 && loop_completed )	// loop over all subscribers completed
			{
ERS_DEBUG(4,"All subscribers served!") ;
			throw NoTasks() ;
			}
		if ( p == 0 )
			{
ERS_DEBUG(6,"Loop completed!") ;
			loop_completed = true ;
			}
		subs = m_subscriptions.at(p) ;
ERS_DEBUG(5,"Selected subscriber " << subs->m_is_object ) ;
	} // end critical section

	// find next subscriber with something ready to deliver
	if ( subs->m_messages_queue.unsafe_size() == 0 )
		{ continue ; }	// get to another subscriber

	// give less priority (exponentially) to slower subscribers, see m_dist definition
	if ( subs->get_average_delay() > m_dist(m_rndgen) * call_timeout )
		{
		ERS_DEBUG(2, "Subscriber " << subs->m_receiver << " is averagely slow ("<< subs->get_average_delay() <<"ms), skip it, threads now: " << subs->m_thread_counter) ;
		continue ;
		}

	// really safe policy for good subscribers, but not so fair for Slow ones:
	// do not give more threads to Slow guys if 1/2 of all threads is already allocated
	// WARNING: in case of many Slow subscribers, this may cause starving of some of them
	// TODO: Add more threads to pool in case many slow subscribers? They are waiting most of the time anyway.
/*	if ( subs->m_priority == Subscription::Priority::Slow && m_thrpool.get_busy_threads() > n_threads >> 1 ) 
		{
		ERS_DEBUG(2, "Subscriber " << subs->m_receiver << " is really slow, and we have too many busy threads, skipping this subscriber having threads now: " << subs->m_thread_counter) ;
		continue ;
		}
*/

	if ( subs->m_thread_counter <  (n_threads - m_thrpool.get_busy_threads()) / max_threads_fraction_per_subscr )  	// do not give all threads to one subscriber
		{
			// slow subscriber which is already being served
			// at first loop, serve only non-slow receivers
//		if ( subs->m_priority == Subscription::Priority::Slow && subs->m_thread_counter > 0 ) 
// 		     || (subs->m_priority == Subscription::Priority::Slow && !loop_completed ))
//			{
//ERS_DEBUG(4, "Subscriber " << subs->m_receiver << " is slow, not giving it more threads, threads now: " << subs->m_thread_counter) ;
//			continue ;
//			}
//		else // normal subscriber or slow without threads allocated
//			{
ERS_DEBUG(2, "Assigned subscriber " << subs->m_receiver << " to thread ID " << std::this_thread::get_id()) ;

			// subs->m_priority = Subscription::Priority::Normal ;
			return std::bind(&Subscription::notify_receiver, subs) ; // real notification code
//			}
		}
	else	// this subscriber has too many threads allocated
		{
ERS_DEBUG(2, "Subscriber " << subs->m_receiver << " is being served by " << subs->m_thread_counter << " threads, not giving it more") ;
		if ( subs->m_priority == Subscription::Priority::Normal )
			{ subs->m_priority = Subscription::Priority::Busy ; }
		continue ; // try next subscriber
		}
	} // while
catch 	( std::out_of_range &ex ) // either some subscribers were removed or list is completed
	{ 
ERS_LOG("Not really expected: out of bound when iterating over subscriptions: " << ex.what()) ;
	continue ;
	}
}

// IS callback with either added or removed subscription object
void
Worker::subscriptions_changed(ISCallbackInfo * callback)
{
	std::string subscr_name = callback->objectName() ;
	::mts::MTSSubscriptionInfo newinfo ;
	try {
		callback->value( newinfo ) ;
		}
	catch ( const is::Exception & ex  )
	{ 	ers::error(ex) ;
		return ; }

	ERS_LOG("Subscription " << newinfo.filter_expression << " of receiver " << callback->objectName() << " was " << callback->reason()) ;
	
	Receiver rec_ref ;

	switch ( callback->reason() )
	{
	case  ::is::reason::Created:
	case  ::is::reason::Subscribed:
	{
		{
		tbb::queuing_rw_mutex::scoped_lock lock(m_subscr_mutex, true) ;

		// check if Created callback came after Deleted
		auto iter = m_early_deleted.find(subscr_name) ;
		if ( iter != m_early_deleted.end() )
			{
			ERS_LOG("Subscription " << subscr_name << " was already deleted, not adding it!") ;
			m_early_deleted.erase(iter) ;
			return ;
			}

		for ( auto it = m_subscriptions.begin(); it!=m_subscriptions.end(); it++ )
			if ( (*it)->m_is_object == subscr_name && (*it)->m_expression == newinfo.filter_expression )
			{
				ERS_LOG("Subscription " << newinfo.filter_expression << " of receiver " << subscr_name << " already existed!") ;
				return ;
			}
		rec_ref = ::mts::receiver::_narrow(IPCCore::stringToObject(newinfo.receiver_ref)) ;
		m_subscriptions.push_back( std::make_shared<Subscription>(
										std::string(newinfo.filter_expression), subscr_name, ::mts::receiver::_duplicate(rec_ref), this) ) ;

		} // scoped lock
		ERS_DEBUG(0, "Added subscription to " << subscr_name << " : " << newinfo.filter_expression) ;
		
		if ( newinfo.sync_required ) {
			ERS_DEBUG(2, "Subscriber requested syncronization on subscription " + newinfo.filter_expression) ;
			try {
				rec_ref->subscribed(newinfo.filter_expression.c_str()) ;
				ERS_DEBUG(0, "Subscriber notified that the syncronous subscription is done: " << newinfo.filter_expression) ;
			} catch ( const CORBA::SystemException& ex ) {
				ers::error(mts::FailedNotifySubscriber(ERS_HERE, subscr_name, std::runtime_error(ex._name()))) ;
			} catch ( const std::exception& ex ) { // not sure if this inherits from CORBA::SystemException 
				ers::error(mts::FailedNotifySubscriber(ERS_HERE, subscr_name, ex)) ;	
			} catch ( ... ) {
				ers::error(mts::FailedNotifySubscriber(ERS_HERE, subscr_name)) ;
			}
		}

		break ;
	}
	case  ::is::reason::Deleted:
	{
		tbb::queuing_rw_mutex::scoped_lock lock(m_subscr_mutex, true) ;

		bool deleted = false ;
		for ( auto it = m_subscriptions.begin(); it!=m_subscriptions.end(); )
			if ( (*it)->m_is_object == subscr_name )
			{
				it = m_subscriptions.erase(it) ;
				deleted = true ;
				ERS_LOG("Removed subscription " << newinfo.filter_expression << " of " << subscr_name) ;
			}
			else
			{
				it++ ;
			}
		if ( deleted == false )
		{
			ERS_LOG("Subscription " << newinfo.filter_expression << " of receiver " << subscr_name << " not found: perhaps callbacks order broken") ;
			m_early_deleted.insert(subscr_name) ;
		}
		break ;
	}
	case  ::is::reason::Updated:
		// TODO proper ers issue or it will never happen?
		ERS_LOG("Not really expected: MTS subscriber info changed (normally it can be only removed or added)!") ;
		break ;

	default:
		ERS_LOG("Unexpected IS callback reason: " << callback->reason()) ;
		break ;
	}
ERS_DEBUG(1, "Subscribers left: " <<  m_subscriptions.size()) ;
}


// remove a (disconnected) receiver
// TODO make m_messages_queue.clear() async (copy pointer and call clear() out of critical region)
// otherwise, in case of removal of many slow receivers with many buffered messages
// lock(m_subscr_mutex, true) may lock everything for some seconds
void
Worker::remove_receiver ( Receiver r )
{
ERS_DEBUG(1, "Active subscribers: " <<  m_subscriptions.size()) ;
ERS_DEBUG(0, "Removing (dead) receiver " << r ) ;

std::string is_name ;

tbb::queuing_rw_mutex::scoped_lock lock(m_subscr_mutex, true) ;

SubscriptionsListIt subs = m_subscriptions.begin() ;
while ( subs != m_subscriptions.end() )
	{
	if ( (*subs)->m_receiver->_is_equivalent(r) )
		{
ERS_DEBUG(4, "Matched receiver " << (*subs)->m_is_object << " :" << (*subs)->m_receiver) ;
		(*subs)->m_messages_queue.clear() ; // NB: takes time if queue is long
		std::string removed = (*subs)->m_is_object ;
		subs = m_subscriptions.erase(subs) ;
ERS_LOG("Dead receiver " << removed << "(" << r << ") removed from the local subscriptions list" ) ;
		// remove from IS repository by name TODO: make this  without locking the mutex
		unsubscribe_receiver(removed) ;
ERS_DEBUG(2, "Receiver " << removed <<  " unpublished from IS") ;
		}
	else{
		subs++ ;
	}
	}

ERS_DEBUG(1, "Active subscribers: " <<  m_subscriptions.size()) ;
}

bool
Worker::message_match(const Message& msg, Subscription& subs) throw()
{
return subs.is_matched(msg) ;
}

void
Worker::message_sent()
{ 
m_messages_sent++ ;
m_messages_queued-- ;

ERS_DEBUG(5, "Messages still queued:\t" << m_messages_queued);
ERS_DEBUG(5, "Total messages sent:  \t" << m_messages_sent);
} 

void
Worker::count_rate()
{
::mts::MTSWorkerStat wstat ;
::mts::MTSReceiverStat aninfo ;
ISInfoDictionary dict(m_ipc_partition) ;
std::string pubname_w = IS_server_name + "." + m_app_name ;

try {
while (true)
{
size_t s_prev = m_messages_sent, r_prev = m_messages_received, q_prev = m_messages_queued ;
std::unique_lock<std::mutex> lock(m_thread_cond_mx) ;
if ( m_thread_cond.wait_for(lock, std::chrono::seconds(m_period) ) == std::cv_status::no_timeout )
	{ 
ERS_DEBUG(1, "Got signal, exiting stat thread") ;
	return ;
	}

size_t s_now = m_messages_sent, r_now = m_messages_received, q_now = m_messages_queued  ;
size_t rate_r = (r_now - r_prev)/m_period, rate_s = (s_now - s_prev)/m_period;

wstat.msgs_received = m_messages_received ;
wstat.msgs_delivered = m_messages_sent ;
wstat.msgs_queued = m_messages_queued ;
wstat.delivery_rate = rate_s ;
wstat.received_rate = rate_r ;
wstat.active_threads = m_thrpool.get_busy_threads() ;
wstat.total_threads = m_thrpool.get_total_threads() ;

if ( m_interval_stat )
	{
	ERS_LOG("Rates (msgps): R: " << rate_r << " S: " << rate_s << ", Q: " << (q_now - q_prev)/m_period <<
		", T: " << m_messages_sent << ", R: " << m_messages_received << ", BT: " << m_thrpool.get_busy_threads()) ;
	}

try {
	dict.checkin(pubname_w, wstat) ;
	}
catch ( const daq::is::Exception& ex )
	{
	ers::warning(ex) ;
	}

tbb::queuing_rw_mutex::scoped_lock slock(m_subscr_mutex, false) ;
for ( auto subs: m_subscriptions )
	{
	if ( m_interval_stat )
		{
		ERS_LOG("Subs: " << (*subs).m_is_object << " AD: " << (*subs).m_average_delay << " TC: " << (*subs).m_thread_counter
				<< " MS: " << (*subs).m_messages_sent
				<< " Slow: " << ((*subs).m_priority == Subscription::Priority::Slow ? "1" : "0") ) ;
		}

	std::string pubname = IS_server_name + "." + (*subs).m_is_object + ":" + m_app_name ;
	ERS_DEBUG(4, "Publishing MTSReceiverStat for " << pubname << " on " << IS_server_name << " IS server") ;
	try {
		// TODO make MTSReceiverStat an attribute of Subscription
		aninfo.msgs_delivered = (*subs).m_messages_sent ;
		aninfo.msgs_received = (*subs).m_messages_enqueued ;
		aninfo.av_delay = (*subs).m_average_delay ;
		aninfo.queue_length = (*subs).m_queue_counter ;
		aninfo.threads_count = (*subs).m_thread_counter ;
		dict.checkin(pubname, aninfo) ; // ( daq::is::InvalidName, daq::is::RepositoryNotFound, daq::is::InfoNotCompatible )
		}
	catch ( const daq::is::Exception& ex )
		{
		ers::warning(ex) ;
		}
	} // for m_subscriptions
}
}
catch ( std::exception& ex )
	{
	ERS_LOG("Stat thread got unexpected exception: " << ex.what()) ;
	}
}
