
/*!
 *  \file SubscriptionManager.h
 *  
 *  Defines class encapsulates managing of Subscriptions information, shared b/w MTS Receivers and Workers by means os IS.
 * 
 *  \author Andrei Kazarov
 *  \date Nov 2012.
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef SUBSCRIPTIONMANAGER_H_
#define SUBSCRIPTIONMANAGER_H_

#include <atomic>

#include <is/inforeceiver.h>
#include <is/callbackinfo.h>
#include <is/criteria.h>

#include <mts/IPCPartitionManager.h>
#include "mts/MTSSubscriptionInfo.h"

namespace daq {
namespace mts {

class Worker ;
class SubscriptionManager ;

// type of IS callback to be called as Worker member call
// typedef void (Worker::*IS_CALLBACK)(ISCallbackInfo *) ;
using ISCallbackF = void(Worker::*)(ISCallbackInfo *) ;
using ISSubsInfo = std::pair<std::string, ::mts::MTSSubscriptionInfo> ; // pair of IS object name and published MTSSubscriptionInfo


/**
 * \class SubscriptionManager
 * \ingroup mts
 * \brief Encapsulates managing of Subscriptions information, shared b/w MTS Receivers and Workers by means of IS.
 * \see IPCPartitionManager
 * 
 * The IS schema is in mts/mts.schema.xml:
 * MTSSubscription info class: contains receiver CORBA ref and subscription criteria.
 * Data is published in dedicated 'MTS' IS server in partition.
 * 
 * @warning Proper dependencies on MTS IS need to be added to setup segment.
 *
 * Use cases:
 * Receivers: publish (check-in) MTSSubscription object in IS, remove data at exit.
 * Workers: subscribe to all MTS.* objects of MTSSubscription type, receive callbacks on any change.
 * remove data in case of a slow subscriber.
 *
 */
class SubscriptionManager: public IPCPartitionManager {

private:
	const ISCriteria  	subscription_criteria = ::mts::MTSSubscriptionInfo::type() && ".*" ;
	ISInfoReceiver 		m_is_receiver ;

	static std::atomic<size_t>		s_index ;	// unique index of subscription, needed when the same process subscribes with the same criteria twice

protected:
	std::string 		m_my_is_name ; 				// IS info name, built in subscribe() - NB makes sense for Receivers only, worker has it empty
	const std::string 	IS_server_name = "MTS" ; 	// where to publish SubscriptionoInfo ;

public:

	/**
	 * Constructor, just passes partition name to the base class
	 *
	 * @param partition
	 * @throw
	 */
	SubscriptionManager( const std::string& partition ) ;

	/**
	 *  Called by MTS Receiver class.
	 *
	 *  Converts CORBA ref to string and publishes to IS, sharing it to all workers.
	 *
	 * @param corba_ref CORBA reference of the subscriber
	 * @param expression subscription criteria (MTS filter)
	 * @param sync if syncronous subscription is requested
	 * @exception SubscriptionRegistrationFailure
	 */
	void subscribe(const std::string& corba_ref, const std::string& expression, bool sync = false) ;

	/**
	 * Removes receiver data from IS.
	 *
	 * Normally called by Receivers at exit, and also by workers willing to
	 * remove slow subscriber.
	 *
	 * @param is_name Name of IS object in repository, i.e. m_my_is_name for Receivers
	 */
	void unsubscribe_receiver(const std::string& is_name) ;

	/**
	 * Unsubscribes worker from IS MTS information.
	 *
	 * @throw nothing, exceptions are reported only
	 */
	void unsubscribe_worker() ;

	/**
	 * Subscribes a worker to IS information changes
	 *
	 * @param cb IS callback function (worker class member)
	 * @param caller pointer to Worker instance
	 * @throw daq::is::Exception whatever IS problem occured
	 */
	void get_subscriptions(ISCallbackF cb, Worker* caller) ;
	
	virtual ~SubscriptionManager();
};

}}

#endif /* SUBSCRIPTIONMANAGER_H_ */
