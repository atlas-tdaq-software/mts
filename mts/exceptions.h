/**	\file exceptions.h
 	  \brief      ERS exceptions definition for MTS package.
  	\author     A. Kazarov
    \copyright  2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
	  \date       March 2013
 */

#ifndef MTS_EXCEPTIONS_H
#define MTS_EXCEPTIONS_H
                                                                                
#include <ers/ers.h>

namespace daq {

    /*! \class mts::Exception
     *  \brief This is a base class for all MTS exceptions.
     */
    ERS_DECLARE_ISSUE( mts, 
                       Exception,
		       ERS_EMPTY, ERS_EMPTY
                     )

    /*! \class mts::NoWorkers 
     *  \brief This exception is thrown if no mts:workers are registered in 
     *	IPC partition
     *	\param partition IPC partition
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            NoWorkers,
                            mts::Exception,
                            "No MTS workers found in partition " << partition,
			    ERS_EMPTY,
			    ((std::string)partition)
                          )

   /*! \class mts::InvalidExpression 
     * \brief This exception is thrown if the expression provided is
     * not a valid MTS logical expression.
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            InvalidExpression,
                            mts::Exception,
                            "Invalid subscription expression " << expression,
                            ERS_EMPTY,
                            ((std::string)expression)
                         )

   /*! \class mts::InvalidStreamConfiguration
     * \brief This exception is thrown when stream constructor fails because of wrong parameters (e.g. non-numeric timeout value)
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            InvalidStreamConfiguration,
                            mts::Exception,
                            "Invalid stream configuration: " << reason,
                            ERS_EMPTY,
                            ((std::string)reason)
                         )

   /*! \class mts::SyncSubscriptionFailed
     * \brief This exception is thrown when stream constructor fails because of wrong parameters (e.g. non-numeric timeout value)
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            SyncSubscriptionFailed,
                            mts::Exception,
                            "Timeout expired in waiting for subscription confirmation from workers",
                            ERS_EMPTY,
                            ERS_EMPTY
                         )

    /*! \class mts::SubscriptionNotFound
     *   \brief  This exception is thrown if the subscription for 
     *           the MRS server does not exist.
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            SubscriptionNotFound,
                            mts::Exception,
                            "The subscription does not exist :" << reason,
                            ERS_EMPTY,
                            ((std::string)reason)
                          )

    /*! \class mts::PartitionEnvNotSet
     *  \brief  This exception is thrown if the TDAQ_PARTITION environment is not set
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            PartitionEnvNotSet,
                            mts::Exception,
                            "TDAQ_PARTITION environment needed for MTS receiver initialization is not set",
                            ERS_EMPTY,
                            ERS_EMPTY
                          )

	 /*! \class mts::SubscriptionRegistrationFailure
	  *  \brief Failure to register an MTS Subscription information on the IS server
	  */
	  ERS_DECLARE_ISSUE_BASE( mts,
				  SubscriptionRegistrationFailure,
				  mts::Exception,
				  "Failed to register MTS Subscription information on IS server",
				  ERS_EMPTY,
				  ERS_EMPTY
				)

    /*! \class mts::SubscriberRemoved
     *  \brief This exception is thrown when Subscriber is removed after a communication error.
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            SubscriberRemoved,
                            mts::Exception,
                            "The subscriber :" << subscriber << " seems to be down or diconnected. It was removed from MTS after a communication error.",
                            ERS_EMPTY,
                            ((std::string)subscriber)
                          )

    /*! \class mtsFailedNotifySubscriber
     *  \brief This exception is thrown when worker can not notify subscriber (communication issue).
     */
    ERS_DECLARE_ISSUE_BASE( mts,
                            FailedNotifySubscriber,
                            mts::Exception,
                            "Failed to notify subscriber " << subscriber << " about syncronous subsciption. This may cause issues (timeout) on subscriber",
                            ERS_EMPTY,
                            ((std::string)subscriber)
                          )
}

#endif
