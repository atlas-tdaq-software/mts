package utils;

import ers.Issue;
import ers.Logger;

class MyIssue extends ers.Issue {
	private final String my_id ;

	public MyIssue(final String id, final String message) {
		super(message) ;
		my_id = id ;
	}

	public String getId() { return my_id ; } 
}


public class ERSSender
{

public static void main(String[] args)
{
if ( args.length !=3 )
	{
	System.err.println("3 command line parameters required: severity msgID 'a message'") ; System.exit(1) ;
	}

final String severity 	= args[0] ;
final String msg_id 	= args[1] ;
final String message 	= args[2] ;

Issue an_issue = new MyIssue(msg_id, message);

if ( severity.equalsIgnoreCase(ers.Severity.level.Error.name()) )
	{ ers.Logger.error(an_issue) ; }
else
if ( severity.equalsIgnoreCase(ers.Severity.level.Fatal.name()) )
	{ ers.Logger.fatal(an_issue) ; }
else
if ( severity.equalsIgnoreCase(ers.Severity.level.Warning.name()) )
	{ ers.Logger.warning(an_issue) ; }
else
if ( severity.equalsIgnoreCase(ers.Severity.level.Information.name()) ||
     severity.equalsIgnoreCase("info") )
	{ ers.Logger.info(an_issue) ; }
else
if ( severity.equalsIgnoreCase(ers.Severity.level.Debug.name()) )
	{ ers.Logger.debug(0, an_issue) ; }
else
	{ System.err.println("Severity " + severity + " not recognized") ; System.exit(1) ; }

}

}
