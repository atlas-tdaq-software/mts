#include <cstring>

#include <iostream>
#include <stack>
#include <functional>
#include <string>
#include <sstream>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace qi = boost::spirit::qi ;
namespace ascii = boost::spirit::ascii ;

struct Message {
	const char* messageID ;
	const char* severity ;
	std::vector<std::string> qualifiers ;
	} ;

template <typename Iterator>
struct MessageSubscription : qi::grammar<Iterator, bool(), ascii::space_type>
{
    MessageSubscription (const Message& msg):
	MessageSubscription::base_type(expr)

		{
        	using qi::_val;
         	using qi::_1;
         	using qi::_2;

    		NOT         = qi::no_case["not"] ;
    		AND         = qi::no_case["and"] ;
    		OR          = qi::no_case["or"] ;

    		key = 	  (qi::no_case["severity"|qi::no_case["sev"]]) [ _val=std::string(msg.severity) ] 
			| (qi::no_case["messageid"]|qi::no_case["message"]|qi::no_case["msg"])[ _val=std::string(msg.messageID) ] ;

		qual = (qi::no_case["qualifier"]|qi::no_case["qual"]) ;

    		value_str %=
    			qi::lexeme
            		[
              		'\'' >>  (+( qi::alnum - '\'' )) >> '\''
            		] ;

    		value_token %=
    			qi::lexeme
            		[
              		+qi::char_("a-zA-Z0-9_\-:")
            		] ;

    		bool_item = 
	 		  ( key >> '=' >> (value_str | value_token))[ _val = (_1 == _2) ] 
			| ( key >> "!=" >> (value_str | value_token))[ _val = (_1 != _2) ]
			| ( qual >> '=' >> (value_str | value_token))[ _val = boost::phoenix::bind(&MessageSubscription::compare_vector, this, msg.qualifiers, _2) ] ;

    		factor = 
			bool_item[_val=_1]
			| '(' >> expr[_val=_1] >> ')'
			| (NOT >> factor)[_val=!_1]
			;
 
    		expr =	factor[_val=_1] >>  *( (AND >> expr)[_val = _val && _1] |
					       (OR >> expr)[_val = _val || _1] )
			;
		} 

	qi::rule<Iterator, bool(), ascii::space_type> 		expr, bool_item, factor ;
	qi::rule<Iterator> 					NOT, AND, OR ;
	qi::rule<Iterator, std::string()> 			key, value_str, value_token, qual ;

	bool	compare_vector(const std::vector<std::string>& quals, const std::string& value)
		{
		for ( const std::string& qual: quals ) 
			{ if ( qual == value) return true ; }
		return false;
		}	
}; // gramma structure

int main()
{
    std::string input ;
    getline(std::cin,input) ;

    const Message msg = {"TileError::Proc", "ERROR", {"TILE","DEBUG"}} ;

    std::string::iterator it = input.begin() ;
    MessageSubscription<std::string::iterator> r(msg) ;
    bool match ;
    bool res = phrase_parse(it, input.end(), r, ascii::space, match) ;

    if (it != input.end())
    	std::cout << "stopped at: " << std::string(it, input.end()) << "\n";
    
    if ( res ) 
    {
	std::cout << "result: " << match << std::endl ;
    }

}
