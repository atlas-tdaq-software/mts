package mts;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List ;
import java.util.Arrays;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ManagementFactory;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ers.ErsStreamName ;

import is.Repository ;
import ipc.Core ;

/**
 * @author Andrei Kazarov
 */
@ErsStreamName(name="mts")
public class MTSInStream extends ers.AbstractInputStream {

	/**
	 * Internal class implementing the transport layer. 
	 * Receives MTS message, reconstructs ERS Issue (mts.Issue -> ers.Isse) and calls ers.InputStream::receive(Issue).
	 * Inherits from receiverPOA, this is needed to pass CORBA reference to receiver to the workers
	 */
	class CORBAReceiver extends receiverPOA {

	/**
	 * IDL method implementation of one-way notify(Issue)
	 */
	public final void _notify(ers2idl.Issue an_issue) {
	    final ers.Issue issue = mts2ersIssue(an_issue);
	    ers.Logger.debug(3, "Issue constructed, calling user receiver callback");
	    receive(issue); // call method of ers.InputStream
	    }

	/**
	 * IDL method implementation of subscribed
	 * used for syncronous implementation of constructor MTSInStream()
	 * TODO: counter per partition?
	 */
	public final void subscribed(java.lang.String subscription) {
		ers.Logger.debug(2, "Got subscription notification for " + subscription + ", workers expected: " + m_workers.get());
		if ( m_is_sync.get() == false ) { return ; } // ignore
		if ( m_subscription.equals(subscription) ) {
			if ( m_workers.decrementAndGet() == 0 ) {
				ers.Logger.debug(2, "All workers confirmed receiving of subscription " + subscription);
				m_lock.lock() ;
				try {
					m_subscribed.signalAll() ;
				} catch ( final Exception ex ) {
					ex.printStackTrace() ;
					return ;
				} finally {
					m_lock.unlock() ;
				}
			} 
		}
		}
	} // CORBAReceiver

	// TODO share this name with C++ code via mts.idl
    private final String 	IS_Server_name = "MTS" ; // IS server to publish MTS subs info
	
    private is.Repository	m_is_repo ;  		// where to publish subscriptions
    private String	 	m_subscription ;	// subscription string - needed in unsubscribe
    private String	 	m_partition ;		// subscription string - needed in unsubscribe
    private String	 	m_is_id ;		// IS info name
	private mts.receiver 	m_corba_receiver ;	// real transport layer
	private Lock 		m_lock ; 
	private Condition 	m_subscribed ; 
	private AtomicBoolean m_is_sync = new AtomicBoolean(false) ;
	private AtomicInteger m_workers = new AtomicInteger(0); // nuumber of workers in partition

    private static		int s_counter = 0 ;	// to make a unique ID of the subscriber
 
    /*
     * internal function to get current pid from VM
     * needed to construct an unique ID for the receiver publication in IS 
     */
    int getpid()
	{	
	final RuntimeMXBean rmxb = ManagementFactory.getRuntimeMXBean();
	final String token = rmxb.getName() ; // returns pid@hostname
	String pid = token.substring(0, token.indexOf('@') - 1) ;
	int ipid = 0 ;
	try { ipid = Integer.parseInt(pid) ; }
	catch ( NumberFormatException ex ) { java.lang.System.err.println("Failed to get PID from string " + pid); } 
	return ipid;
	}
    
    // loads JNI library implementing native boolean assertValidity
    static	{
    	System.loadLibrary("mtsj") ;
    }
    
    /**
     * Native method, realized in a small C++ library libmtsj.so - to share the validation code with C++ 
     */
    private native boolean assertValidity(String expr, String result) ;
    
    /**
     * Called by ERS framework in shutdown phase
     * Removes current subscription from MTS IS and deactivates CORBA object
     */
    @Override
    public void cleanUp()
	{
	    try	{
		ers.Logger.debug(2, "Removing MTS info " + m_is_id + " from IS server " + IS_Server_name) ;
		m_is_repo.remove(IS_Server_name + "." + m_is_id) ;
		}
	    catch ( final Exception ex ) // CORBA exceptions, not really expected since we've just checked the worker
		{
		ers.Logger.info("Failed to remove subscription in IS: " + ex) ;
		}

	    try	{
		ipc.Core.getRootPOA().deactivate_object( ipc.Core.getRootPOA().reference_to_id( m_corba_receiver ) ) ; 
		}
	    catch ( final Exception ex ) // CORBA exceptions, not really expected since we've just checked the worker
		{
		ers.Logger.info("Failed to deactivate receiver " + ex) ;
		}
	}

    /**
     * Constructor with one parameter, TDAQ_PARTITION is taken from environment. To be used like
     * ers.StreamManager.instance().add_receiver(null_receiver, "mts", "*") ;
     * 
     * @see ers.StreamManager#add_receiver(ers.IssueReceiver, String, Object...)
     * 
     * @param subscription
     * 	message subscription criteria, see class description.
     * @throws PublicationFailure
     * 	in case failed to publish subscription info on IS server in partition
     * @throws mts.BadExpression
     * 	in case of malformed subscription expression
     */
    public MTSInStream(final String subscription)
	    throws mts.BadExpression, PublicationFailure
	    {
	    this(System.getenv("TDAQ_PARTITION"), subscription) ;
	    }

    /**
     * Constructor with two parameter, TDAQ_PARTITION is taken from environment. To be used like
     * ers.StreamManager.instance().add_receiver(null_receiver, "mts", "*", 20) ;
     * 
     * @see ers.StreamManager#add_receiver(ers.IssueReceiver, String, Object...)
     * 
     * @param subscription
     * 	message subscription criteria, see class description.
     * @throws PublicationFailure
     * 	in case failed to publish subscription info on IS server in partition
     * @throws mts.BadExpression
     * 	in case of malformed subscription expression
     * @throws java.util.concurrent.TimeoutException
     * 	in case did not get notified by all workers that subsciption was registered within timeout
     */
    public MTSInStream(final String subscription, final Long timeout)
	    throws mts.BadExpression, PublicationFailure, NoWorkers, java.util.concurrent.TimeoutException
	    {
	    this(System.getenv("TDAQ_PARTITION"), subscription, timeout) ;
	    }
	
	public MTSInStream(final String subscription, final Integer timeout)
	    throws mts.BadExpression, PublicationFailure, NoWorkers, java.util.concurrent.TimeoutException
	    {
	    this(System.getenv("TDAQ_PARTITION"), subscription, timeout) ;
	    }    
    /**
     * Constructor with 2 parameters, to be used like
     * ers.StreamManager.instance().add_receiver(null_receiver, "mts", "initial", "sev!=INFO") ;
     * 
     * @see ers.StreamManager#add_receiver(ers.IssueReceiver, String, Object...)
     * 
     * @param subscription filter string, see class description.
     * @param partition IPC partition
     * @param sync require subscription syncrnonization from workers
     * @throws PublicationFailure in case failed to publish subscription info on IS server in partition
     * @throws mts.BadExpression in case of malformed subscription expression
     */
	public MTSInStream(final String partition, final String subscription)
	throws mts.BadExpression, PublicationFailure {
		super() ;
		_MTSInStream(partition, subscription, false);
	}

	// private version with boolean flag
    private void _MTSInStream(final String partition, final String subscription, final boolean sync)
    	throws mts.BadExpression, PublicationFailure
	{
	String result = "";		//TODO nothing written by C++ code
	if( assertValidity(subscription, result) == false ) {
		System.err.println("assertValidity failed") ;
		throw new mts.BadExpression("Subscription criteria string \"" + subscription + "\" is not valid!");
	}
	
	m_subscription = subscription ;
	m_partition = partition ;
	
	ers.Logger.debug(2, "Initializing MTSInStream with subscription " + subscription) ;
	
	try {
	    m_is_repo = new is.Repository(new ipc.Partition(partition)) ;
	    m_corba_receiver = new CORBAReceiver()._this(ipc.Core.getORB()) ;
	    }
	catch (final Exception ex)
	    {
	    // ers.Logger.info("Failed to init MTSInStream: " + ex) ;
	    throw new PublicationFailure(partition, ex) ;
	    }
	
	int idx ; 
	synchronized(MTSInStream.class) { idx = s_counter++ ; } ; 
	try	{
	String app_name = System.getenv("TDAQ_APPLICATION_NAME") ;
	if ( app_name == null ) { app_name = "" ; }
	
	m_is_id =  app_name + ":" + InetAddress.getLocalHost().getCanonicalHostName()
				+ ":" + getpid() + ":" + idx ;
	}
	catch (final java.net.UnknownHostException ex) 
	    {
	    throw new PublicationFailure(m_partition, ex) ; // TODO proper exception?
	    }
	
	try	{
		MTSSubscriptionInfo info = new MTSSubscriptionInfo() ;
		info.filter_expression = subscription ;
		info.receiver_ref = ipc.Core.objectToString(m_corba_receiver, ipc.Core.IOR) ;
		info.sync_required = sync ;

		ers.Logger.debug(2, "Publishing subscription info " + m_is_id + " in IS server " + IS_Server_name) ;
		
		m_is_repo.checkin(IS_Server_name + "." + m_is_id, info) ;
		}
	    catch ( final Exception ex ) // IS problems
		{
		ers.Logger.error(ex) ;
		ex.printStackTrace() ;
		throw new PublicationFailure(m_partition, ex) ;
		}

	ers.Logger.debug(2, "Constructed MTSInStream") ;
	}

    /**
     * Constructor with 3 parameters, to be used like
     * ers.StreamManager.instance().add_receiver(null_receiver, "mts", "initial", "sev!=INFO", 10) ;
	 * This is to be used mainly in CHIP when it creates subscription and expects new messages right after 
     * 
     * @see ers.StreamManager#add_receiver(ers.IssueReceiver, String, Object...)
     * 
     * @param subscription filter string, see class description.
     * @param partition IPC partition
     * @param timeout_ms timout (milliseconds) to wait for the subscription to be confirmed by all workers
	 * 
     * @throws PublicationFailure in case failed to publish subscription info on IS server in partition
     * @throws mts.BadExpression in case of malformed subscription expression
     * @throws java.util.concurrent.TimeoutException in case no confirmation received from all workers within requested timeout
     */
	public MTSInStream(final String partition, final String subscription, final Long timeout_ms) 
		throws mts.BadExpression, NoWorkers, PublicationFailure, java.util.concurrent.TimeoutException {
		this(partition, subscription, timeout_ms.longValue()) ;
	}

	// version with Integer
    public MTSInStream(final String partition, final String subscription, final Integer timeout_ms) 
    	throws mts.BadExpression, NoWorkers, PublicationFailure, java.util.concurrent.TimeoutException {
		this(partition, subscription, timeout_ms.intValue()) ;
	}
    
	// real code
    private MTSInStream(final String partition, final String subscription, final long timeout_ms)
    	throws mts.BadExpression, NoWorkers, PublicationFailure, java.util.concurrent.TimeoutException {
	super() ;

	WorkersManager wm = new WorkersManager(new ipc.Partition(partition)) ;
	ipc.ObjectEnumeration<mts.worker> workers = wm.getAllWorkers() ;
	m_is_sync.set(true) ;
	m_workers.set(workers.size()) ;
	m_lock = new ReentrantLock() ;
	m_subscribed = m_lock.newCondition() ; // prepare for syncronization in subscribed() callback
	m_lock.lock() ;
	try {
		_MTSInStream(partition, subscription, true) ; // just publishes subscription to MTS IS, now need to wait for all workers to confirm via subscribe() 
		try {
			if ( m_subscribed.await(timeout_ms, TimeUnit.MILLISECONDS) == false ) {
				ers.Logger.log("Timeout of " + timeout_ms + "ms elapsed") ;
				throw new java.util.concurrent.TimeoutException("Did not get subscription confirmation from all MTS workers within timeout") ;
			} ;
		} catch ( final InterruptedException ex ) {
			ex.printStackTrace() ;
		}
	} finally {
		m_lock.unlock() ;
	}
	}

	/**
	 * Recursively converts mts.Issue to ers.Issue
	 * 
	 * @param mts_issue
	 *            MTS stream message
	 * @return ers.Issue new ERS Issue to be passed to client issue receiver in
	 *         callback
	 */
	private ers.Issue mts2ersIssue(final ers2idl.Issue mts_issue) {
		ers.Issue issue = null, cause = null;

		ers.ProcessContext pcontext = new ers.ProcessContext(mts_issue.cont.host_name, mts_issue.cont.process_id,
				mts_issue.cont.thread_id, mts_issue.cont.cwd, mts_issue.cont.user_id, mts_issue.cont.user_name);
		ers.RemoteContext rcontext = new ers.RemoteContext(mts_issue.cont.package_name, mts_issue.cont.file_name,
				mts_issue.cont.line_number, mts_issue.cont.function_name, mts_issue.cont.application_name, pcontext,
				(long)(mts_issue.time/1000) );	// mts is ll in microsecs, but jers time in millis

		ArrayList<String> qualifiers = new ArrayList<String>(
				Arrays.asList(mts_issue.quals));

		Map<String, String> parameters = new HashMap<String, String>();

		for (ers2idl.ERSParameter par : mts_issue.params) {
			parameters.put(par.name, par.value);
		}

		if (mts_issue.cause.length > 0) {
			cause = mts2ersIssue(mts_issue.cause[0]);
		}

		try {
			// System.err.println("Creating issue " + mts_issue.ID +
			// " with context: " + rcontext.file_name()) ;
			issue = ers.Issue.createIssue(rcontext, mts_issue.ID, mts_issue.message, cause, mts2ErsSeverity(mts_issue.sev),
					(long)(mts_issue.time/1000), qualifiers, parameters); // mts is in microsecs, but jers time in millis
			// System.err.println("Issue created: ") ;
			// ers.IssuePrinter.println(System.out, issue, issue.severity(), 0)
			// ;
		} catch (final ers.BadIssue ex) {
			System.err.println("Failed to recreate issue of class " + mts_issue.ID + ": " + ex.getMessage());
			issue = new ers.Issue(ex);
		}

		return issue;
	}
	

	/**
	 * Maps er2idl.Severity generated from er2idl.idl to ers.Severity enum
	 * 
	 * @param mts_sev
	 * @return ers.Severity.level
	 */
	
	private static ers.Severity.level mts2ErsSeverity(final ers2idl.Severity mts_sev) {
		switch (mts_sev.value()) {
		case ers2idl.Severity._debug:
			System.err.println("It is not expected that DEBUG ERS Issues will be transfered via MTS." +
					"DEBUG rank is not passed. Thus, retuning rank 0.");
			return ers.Severity.level.Debug;
		case ers2idl.Severity._information:
			return ers.Severity.level.Information;
		case ers2idl.Severity._warning:
			return ers.Severity.level.Warning;
		case ers2idl.Severity._error:
			return ers.Severity.level.Error;
		case ers2idl.Severity._fatal:
			return ers.Severity.level.Fatal;
		case ers2idl.Severity._log:
			return ers.Severity.level.Log;
		default:
			System.err.println("Unexpected integral ers.Severity received from MTS srteam: " + mts_sev.value()
					+ " falling back to ers.Log");
			return ers.Severity.level.Log;
		}
	}
 	
}
