/*! \file jsrc/test/TestReporter.java
 * Working example of a command-line Java MTS ERS receiver. Used in a Java test target.
 *
 * \author Andrei Kazarov
 * \date 2012
 */

 package test;

import ers.Logger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Level;
import org.apache.log4j.xml.DOMConfigurator;

// a class without parameters, just to have a dedicated message ID == "TestIssue"
// for a message with parameters, see jsrc/test/MyTestIssue.java 
// (each public Issue class must be in a separate java source file)
class TestIssue extends ers.Issue {
	public TestIssue(String message) {
		super(message);
	}

	public TestIssue(String message, ers.Issue cause) {
		super(message, cause);
	}
}

public class TestReporter {

	static void my_test_function() {
		ers.Issue issue_f = new ers.Issue("test program");
//		ers.Logger.debug(1, new TestIssue("TestIssue3 test issue"));
//		ers.Logger.log("TestIssue3 test message");

//		String param1 = "serious shit happened";
//		int param2 = 19;
//		ers.Logger.error(new MyTestIssue(param1, param2, new TestIssue("A funny cause")));
//		log4jLogger.error(new MyTestIssue(param1, param2, new TestIssue("A funny cause")));
//		ers.Logger.error(new MyTestIssue("another great shit on another port", 23));
		ers.Logger.error( issue_f );
		log4jLogger.error( issue_f );
		
//		ers.Logger.error( new Exception("123"));
	}
	
	static ers.Issue retIssue() {
		return new ers.Issue("This issue is created and returned by function retIssue()");
	}
	
	static void reportIssueFromOtherFunction() {
		ers.Logger.error(retIssue());
		log4jLogger.error(retIssue());
	}
	
	static void myExceptionFunction() throws Exception {
		throw new Exception(" This is an exception msg from a function!");
	}
	static void callingExceptionFunction() throws Exception {
		myExceptionFunction();
	}

	private static org.apache.log4j.Logger log4jLogger = LogManager.getLogger("TestReporterLogger");

	public static void main(String[] args) throws Exception {

		DOMConfigurator.configure(args[0] + "/jsrc/test/log4j_xmlConfig.xml");
		log4jLogger.setLevel(Level.ALL);

		ers.Issue issue = new ers.Issue("test program");

		ers.Issue issue2 = new TestIssue("TestIssue test program");

		ers.Issue issue_verbose_1 = new ers.Issue("more verbose message, you will see it");
		ers.Issue issue_verbose_3 = new ers.Issue("very verbose message, you wont see it!");
		ers.Issue error = new ers.Issue("an error occured");
		ers.Issue cause2 = new ers.Issue("more underlying reason");
		ers.Issue cause = new ers.Issue("an underlying reason", cause2);

		TestIssue message = new TestIssue("a specific TestIssue message with cause, qualifier and parameters", cause);
		TestIssue message2 = new TestIssue("a specific TestIssue message without a cause");

		MyTestIssue message3 = new MyTestIssue("Test Issue with 2 parameters and a cause", 124, message2) ;

		MyTestIssueString message4 = new MyTestIssueString() ;

		message.addQualifier("TEST_QUAL");
		message.setValue("TestValueString", "Test");
		message.setValue("TestValueInt", 31425);

		System.out.println("TDAQ_ERS_FATAL: " + java.lang.System.getenv("TDAQ_ERS_FATAL"));
		System.out.println("TDAQ_ERS_ERROR: " + java.lang.System.getenv("TDAQ_ERS_ERROR"));
		System.out.println("TDAQ_ERS_WARNING: " + java.lang.System.getenv("TDAQ_ERS_WARNING"));
		System.out.println("TDAQ_ERS_DEBUG: " + java.lang.System.getenv("TDAQ_ERS_DEBUG"));
		System.out.println("TDAQ_ERS_INFO: " + java.lang.System.getenv("TDAQ_ERS_INFO"));
		System.out.println("TDAQ_ERS_LOG: " + java.lang.System.getenv("TDAQ_ERS_LOG"));


	//	log4jLogger.error("This is an error message reported via log4j (MTS testing)");
		

		ers.Issue is = new ers.Issue("this is a testing issue");
				
		for ( int i = 0 ; i < 30; i++ )
		    { 	Thread.sleep(100) ;
		    	ers.Logger.error(is) ;
		    } ;
		
//		ers.Logger.error(issue);
//		log4jLogger.error(issue);
		
//		ers.Logger.error(issue2);
//		log4jLogger.error(issue2);
//		
//		ers.Logger.error(cause);
//		log4jLogger.error(cause);
//		
		ers.Logger.error(message3);
		ers.Logger.error(message4);
//		log4jLogger.error(message2);
		ers.Logger.error(message);
		log4jLogger.error(message);
//		
		 my_test_function();
//		
		reportIssueFromOtherFunction();
//		

		try {
			double e = 1 / 0;
		} catch (final RuntimeException ex) {
			ers.Logger.error(ex);
			log4jLogger.error(ex);
		}


//		
//		try {
//			//myExceptionFunction();
//			callingExceptionFunction();
//		} catch (Exception ex) {
//			ers.Logger.error(ex);
//			log4jLogger.error(ex);
//		}


//		log4jLogger.fatal("log4j fatal!");
//		ers.Logger.fatal(issue);


		//Carefull with the debug level.
		// 1. ipc may report something with debug.
		// 2. if sender reports its own debugs, while processing a user's debug message,
		//   then there will probably be infinite recursion  
//		log4jLogger.info("log4j info");
//		log4jLogger.debug("log4j debug");
//		log4jLogger.trace("log4j trace");
		
		
//		ers.Logger.debug(0, issue);
//		ers.Logger.debug(0, issue2);
//		ers.Logger.debug(0, "In-place issue");
//		ers.Logger.log("In-place Log message");
		
//		ers.Logger.debug(0, " my debug!! 0 ");
		
//		ers.Logger.debug(1, " my debug!! 1 ");
//		ers.Logger.debug(2, "2 debug message!!");
//		ers.Logger.debug(3, "3 debug message!!!");
//		ers.Logger.debug(2, issue_verbose_1);
//		ers.Logger.debug(3, issue_verbose_3);
//		ers.Logger.debug(4, "4 Very very verbose message");
//		System.out.println("Severity of issue sent to WARNING is " + cause2.severity());
//		ers.Logger.warning(cause2);
//		ers.Logger.info(message);
//		ers.Logger.info("Information message not important");
//		ers.Logger.log("Log message, not to be archived, just for stdout");

	}
}
