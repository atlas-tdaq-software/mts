package mts;

import java.util.Map;

import ers.AbstractOutputStream ;
import ers.Severity ;
import ers.Issue ;
import ers.ErsStreamName ;

/**
 * 
 * @author cbiau akazarov lpapaevg
 * 
 */

@ErsStreamName(name="mts")
public class MTSOutStream extends ers.AbstractOutputStream {

	private Sender m_sender;

	private void init(final String partition) {
		m_sender = new Sender(partition) ;
		}
	
	public MTSOutStream() throws Exception {
		super() ;
		
		if ( System.getenv("TDAQ_PARTITION") == null )
			{ throw new Exception("Enviromental variable TDAQ_PARTITION is not set."); }

		init(System.getenv("TDAQ_PARTITION")) ;
	}
	
	public MTSOutStream(final String partition) {
		super() ;
		init (partition) ;
	}

	
	/**
	 * Sends an Exception into the stream, may be Issue or base java Exception
	 * In latter case, make an Issue out of it and report that issue
	 * @param issue Exception
	 * @param severity Severity of the issue (derived from the stream it was sent to)
	 */
	public void write(final Exception ex, final ers.Severity severity) {
		
		ers.Issue ers_issue = null;
		
		//  check if it is an Issue or a simple java exception
		if( ex instanceof ers.Issue ) {
			// An ers.Issue (or subtype etc) was reported via ers.Logger
			//   or something reported via log4j, since JERSAppender always creates
			//   and sends an ers.Issue to the ers.Logger
			ers_issue = ((ers.Issue)ex);
		} else {
			// The exception is not ers.Issue (or subtype etc) and comes directly from ers.Logger
			// this builds recursively a chain of Issues from chain of Throwables (with causes), see ers.Issue(final Throwable exception)
			ers_issue = new ers.Issue(ex) ;
		}	

		ers_issue.setSeverity(severity); // FIXME does not propagate to chained issues, which have default DEBUG severity!
		
		m_sender.report_block(ers_issue); // no throw
		chained().write( ex, severity ); // next stream
	}
	
	public void configure(Map<String, String> config) {
	}

	public void cleanUp(){
	}
}
