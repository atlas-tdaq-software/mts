#include <thread>
#include <chrono>
#include <atomic>

#include <ers/ers.h>
#include <ers/RemoteContext.h>
#include <ers/AnyIssue.h>
#include <ers2idl/ers2idl.h>

#include "mts/Receiver.h"
#include "mts/Subscription.h"
#include "mts/WorkersManager.h"

// register mts::Receiver as ERS input stream
// for use from client code, see examples/mts_ers_receiver
ERS_REGISTER_INPUT_STREAM( daq::mts::ERSReceiver, "mts", format )

// declaration, see SubsValidator.cpp
namespace daq { namespace mts {
bool ext_assert_validity(const std::string& expr, std::string& ret_error) ; }}

using namespace daq::mts ;
using namespace std::chrono ;

ERSReceiver::ERSReceiver( const std::initializer_list<std::string>& args ) 
{
std::initializer_list<std::string>::iterator argsit = args.begin() ;
std::string partition, timeout_str ;
int timeout{1000} ; // 1000ms
if ( args.size() == 1 ) // one argument
	{
	m_subscription = *argsit ;
	}
else if ( args.size() == 2 )	{		// two arguments
	partition = *argsit++ ;
	m_subscription = *argsit ;
	}
else	{		// three arguments: timout TODO: exception if 3+ arguments?
	partition = *argsit++ ;
	m_subscription = *argsit++ ;
	timeout_str = *argsit ;
	m_sync_subscription = true ;
	try {
		timeout = std::stoi(timeout_str) ; // throw ...
	} catch ( const std::exception& ex ) {
		throw InvalidStreamConfiguration(ERS_HERE, "Wrong timeout value passed", ex) ;
	}
	WorkersManager wm(partition) ; // throw NoWorkers, BadIPCPartition
	m_n_workers = wm.get_all_workers().size() ;
ERS_DEBUG(0, "Will wait for confirmation from " << m_n_workers << " workers") ;
	}

assert_validity( m_subscription ) ; // throw ::mts::BadExpression

ERS_DEBUG(1, "Constructing MTS CORBAReceiver in partition " << partition) ;
m_receiver = new CORBAReceiver( partition, this ) ; // this is CORBA object, shall not throw!

// SubscriptionManager::subscribe
::mts::receiver_var rec = m_receiver->_this() ;
m_receiver->subscribe(IPCCore::objectToString(rec), m_subscription, m_sync_subscription) ; // publishes subscription in IS throw SubscriptionRegistrationFailure

if ( m_sync_subscription ) {
	std::unique_lock<std::mutex> lock(m_sync_mutex) ;
ERS_DEBUG(0, "Waiting for " << m_n_workers << " workers to subscribe") ;
	if ( m_condition.wait_for(lock, milliseconds(timeout)) == std::cv_status::timeout ) {
		throw SyncSubscriptionFailed(ERS_HERE) ;
	} ;
ERS_DEBUG(0, "All workers subscribed") ;
}

ERS_DEBUG(1, "Constructed ERSReceiver") ;
}

// WARNING: calling _destroy(true) waits for all the buffered callbacks to be processed, 
// which may take time
ERSReceiver::~ERSReceiver( ) 
{
ERS_DEBUG(1, "Unsubscribing MTS receiver") ;
m_receiver->remove_subscriptions() ;		// try to be graceful
ERS_DEBUG(1, "Destroying MTS (CORBA) Receiver") ;
m_receiver->_destroy(true) ; 				// wait for all active callbacks around, then destructor (empty) of Receiver called
ERS_DEBUG(2, "Corba objects destroyed") ;
}

CORBAReceiver::CORBAReceiver( const std::string & partition, ERSReceiver* parent) // throw SubscriptionRegistrationFailure
:SubscriptionManager(partition),
m_ers_receiver(parent),
m_message_counter(0)
{
ERS_DEBUG(1, "Constructed MTS CORBAReceiver") ;
}

CORBAReceiver::~CORBAReceiver() {
ERS_DEBUG(1, "Destroyed MTS CORBAReceiver") ;
}

// to be called from parent destructor before IPC _destroy
void CORBAReceiver::remove_subscriptions( )
{
	//SubscriptionManager::unsubscribe()
	unsubscribe_receiver(m_my_is_name) ;
}

// implementation of CORBA receiver::subscribed interface
// presently is needed in Java only
void CORBAReceiver::subscribed(const char* subscription) {
	if ( m_ers_receiver->m_sync_subscription ) {
ERS_DEBUG(0, "Got confirmation from worker for subscription " << subscription) ;
		if (  m_ers_receiver->m_subscription.compare(subscription) == 0 ) {
			std::unique_lock<std::mutex> lock(m_ers_receiver->m_sync_mutex) ;
			m_ers_receiver->m_n_workers-- ;
ERS_DEBUG(1, "Still waiting for " << m_ers_receiver->m_n_workers) ;
			if ( m_ers_receiver->m_n_workers == 0 ) { // all workers responded
				m_ers_receiver->m_condition.notify_all() ;
ERS_DEBUG(1, "All workers responded, subscriber unblocked") ;
			}
		}
	}
}

// implementation of CORBA receiver::notify interface
void CORBAReceiver::notify(const ::ers2idl::Issue& message) 
{
ERS_DEBUG(3, "Got MTS message " << message.ID) ;
m_message_counter++ ;
auto issue = daq::idl2ers_issue(message) ;

ERS_DEBUG(5, "Built ERS issue " << issue.get()->get_class_name()) ;

// calls ers InputStream receive function, which calls user's callback
// NB: may be "slow", and cause CORBA timeouts on workers
m_ers_receiver->notify( *issue ) ;
ERS_DEBUG(3, "User ERS receiver notified " << message.ID) ;
}

void
ERSReceiver::assert_validity(const std::string& expr) 	// throws ::mts::BadExpression if parsing fails
{
std::string reason ;
if ( !ext_assert_validity(expr, reason) )
	{
	throw InvalidExpression( ERS_HERE, reason.c_str() ) ;
	}
}
