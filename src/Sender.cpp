#include <assert.h>                                        // for assert
#include <chrono>                                   	   // for chrono
#include <stddef.h>                                        // for size_t
#include <atomic>                                          // for __atomic_base
#include <functional>                                      // for _Bind_help...
#include <memory>                                          // for shared_ptr
#include <string>                                          // for string

#include "omniORB4/CORBA.h"                                // for SystemExce...
#include "omniORB4/templatedecls.h"                        // for _CORBA_Obj...
#include "ers/Issue.h"                                     // for Issue (ptr...
#include "ers/ers.h"                                       // for ERS_DEBUG
#include "ers2idl/ers2idl.h"
#include "ers2idl/ers2idl.hh"                              // for Issue
#include "mts/ThreadPoolNQ.h"                              // for ThreadPoolNQ
#include "mts/WorkersManager.h"                            // for WorkersMan...
#include "mts/defs.h"                                      // for Message
#include "mts/exceptions.h"                                // for NoWorkers
#include "mts/mts.hh"                                      // for _objref_wo...

#include "mts/Sender.h"

/* CORBA Timeouts in omniORB
namespace omniORB {
void setClientCallTimeout(CORBA::ULong millisecs);
void setClientCallTimeout(CORBA::Object_ptr obj, CORBA::ULong millisecs);
void setClientThreadCallTimeout(CORBA::ULong millisecs);
void setClientConnectTimeout(CORBA::ULong millisecs);
}; */

// oneCallPerConnection = 0 
/* To tell the ORB that it may multiplex calls on a single connection, set the
oneCallPerConnection parameter to zero. If the oneCallPerConnection
parameter is set to the default value of one, and there are more concurrent calls than specified by
maxGIOPConnectionPerServer, calls block waiting for connections to become free.
*/

/*
catch (CORBA::TRANSIENT& ex) {
if (ex.minor() == omni::TRANSIENT_ConnectFailed) {
// retry with a different object reference...
}
else {
// print an error message...
}
*/

using namespace daq::mts ;
using namespace std::chrono ;

Sender::Sender( const std::string & p, size_t nthreads) 
:WorkersManager(p), m_messages_queued(0), m_messages_sent(0)
{
ERS_DEBUG(1, "Initializing MTS sender"); 

if ( nthreads )
	{ m_thrpool = std::make_unique<ThreadPoolNQ>(nthreads, *this) ; }

ERS_DEBUG(2, "Initialized MTS Sender") ;

// possibly: get list of available MTS workers here and ever
// or: the list of workers is updated each time get_worker() is called
// it is assumed that IPC caching works
}

Sender::~Sender()
{
ERS_DEBUG(1, "Destroying Sender") ;
} 

// build chained ers2idl::Issue and place it in a queue which is managed by a threadpool
void Sender::report_nonblock(const ers::Issue & issue) 
{
ERS_DEBUG(1, "Sending issue (async) " << issue.get_class_name()) ;
Message* message = new ::ers2idl::Issue() ; // allocate root message
daq::ers2idl_issue(*message, issue) ; // build chained

// enqueue shared pointer to just created structure
// TODO: handle local buffer overallocation? use bounded queue
ERS_DEBUG(3, "Scheduling async message sending") ;
m_messages_queue.push(std::shared_ptr< Message >(message)) ;
m_messages_queued++ ;
assert (m_thrpool != nullptr) ;
// tbb::atomic_fence() ;	 // guarantee that push is done BEFORE try_pop from another thread ?
// ERS_DEBUG(4, "Awaking one thread") ;
m_thrpool->awake_one() ; // awake one worker thread
}

// build chained ers2idl::Issue and call remote method directly
void Sender::report_block(const ers::Issue & issue) 
{
ERS_DEBUG(1, "Sending issue (sync) " << issue.get_class_name()) ;
Message* message = new ::ers2idl::Issue() ; // allocate root message
daq::ers2idl_issue(*message, issue) ; // build chained

report_remote(std::shared_ptr< Message >(message)) ;
}

// task scheduler for Sender
// just peak up next message from the queue and return executable Task wich will do real sending job 
Task
Sender::get_next_task() // throw(NoTasks) 
{
MessageSP message ;

ERS_DEBUG(4, "Trying to get a message from the queue") ;

if ( !m_messages_queue.try_pop(message) ) 
		{
ERS_DEBUG(4, "Nothing to be sent") ;
		throw NoTasks() ;
		}

return std::bind(&Sender::report_remote, this, message) ;
}

// executed in a worker thread, waits a message to appear in queue and send it to a Worker using CORBA
// must not throw anything
// handle all corba, timeouts and IPC exceptions here
void Sender::report_remote(MessageSP message) noexcept 
{
int noloop = 0 ;
while ( true ) // try until we find a working node or no working nodes...
	{
	try 	{
ERS_DEBUG(5, "Getting a worker to send message");
#ifndef ERS_NO_DEBUG
		system_clock::time_point zero = system_clock::now() ;
#endif
		::mts::worker_var wrk = get_worker() ; 
#ifndef ERS_NO_DEBUG
		system_clock::time_point start = system_clock::now() ;
#endif
ERS_DEBUG(4, "Getting worker took "  << duration_cast<microseconds>(start-zero).count() << " microseconds");
ERS_DEBUG(3, "Reporting message to remote worker");
		wrk->report(*message) ; // real CORBA call 
		// end = system_clock::now() ;
		m_messages_sent++ ;
ERS_DEBUG(3, "Message reported OK: "  << duration_cast<microseconds>(system_clock::now()-start).count() << " microseconds");
ERS_DEBUG(4, "Messages sent: " << m_messages_sent) ;
ERS_DEBUG(4, "Messages queued: " << m_messages_queued) ;
ERS_DEBUG(4, "Messages in the delivery queue: " << m_messages_queue.unsafe_size() ) ;
		break ;
		}
	catch ( NoWorkers& ex )
		{
		// TODO: report to std::error ?
		ERS_DEBUG(1, "Message not reported: No good workers available: " << ex);
		return ;
		}
	catch	( CORBA::SystemException& ex ) // TODO: timeout vs transient ?
		{
		ERS_DEBUG(1, "Can not send message on a worker: " << ex << std::endl << ex.minor() << std::endl << "Will re-try on another") ;
		if ( ++noloop > 2 )
			{
			ERS_DEBUG(0, "Failed " << noloop << " times, no success! Giving up seding message. There are some severe issues with MTS infrastructure.") ;
			return ;
			}
		// update_workers() here ? or it may be a transient problem?
		try { update_workers(false) ; }
		catch ( NoWorkers& ex )
			{
			ERS_DEBUG(0, "Message not reported: No good workers available: " << ex);
			return ;
			}
		continue ;
		}
	catch	( ... ) // can we ever be here?
		{
		ERS_LOG("Unexpected exception, can not send message, will re-try few times more... ") ;
		if ( ++noloop > 2 ) { return ; } ;
		continue ;
		}
	}
}
