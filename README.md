# MTS package

## Introduction
MTS: Message Transfer System, a CORBA-based transport layer for sending and receiving (following subscribe/notify model) ERS messages in TDAQ system.
Client libraries available in C++ and Java. Normally clients do not use MTS API directly, instead they configure MTS as ERS (atlas-tdaq-software/ers>) plugin, by defining ERS stream `mts` e.g. `TDAQ_ERS_ERROR='lstderr,mts'`

## Subscription syntax
Supported subsciption syntax (EBNF notation), based on available ERS fields:

    key = 'app' | 'msg' | 'qual'
    sev = 'sev'
    par = '(param|par)(+[a-zA-Z_-])'
    context_item = 'app' | 'host' | 'user' | 'package' | 'file' | 'function' | 'line'
    context = 'context(context_item)'
    sevlev = 'fatal' | 'error' | 'warning' | 'info' | 'information'
    token_wildcard = +[a-zA-Z0-9_.:-]-'*'
    token = +[a-zA-Z0-9_.:-]
    item = (key ('=' | '!=') token_wildcard) | (sev ('=' | '!=') sevlev) | ((par | context) ('=' | '!=') token) 
    factor = item | ( expression ) | ('not' factor)
    expression = '*' | factor *(('and' expression) | ('or' expression))

For complete source code with subscription syntax, look at [here](mts/SubscriptionParser.h#L243)

### Subscription example

    (app=HLT* and (not (param(algorithm)=muons and context(file)=test.cpp)) or sev=fatal or msg=rc::*)
    
### Libraries
Package provides C++ and Java libraries, which are however not intended for a public use: mts is used as a ERS plugin.  

### Utilities
To subscribe and receive messages in terminal
```
> mts_ers_receiver -p $TDAQ_PARTITION -s "sev=ERROR AND context(host)=pc-tdq-tpu-55020.cern.ch" -v
```
You can also run a GUI version which is equivalent to IGUI ERS panel: `start_ers_monitor`

For sending test ERS messages (in addition to reporting from your code using ERS API), one can use Java utility like
```
> TDAQ_PARTITION=mypartition TDAQ_APPLICATION_NAME=myapp java -classpath ${TDAQ_INST_PATH}/share/lib/erssender.jar:${TDAQ_INST_PATH}/share/lib/ers.jar utils.ERSSender ERROR test-message "Hello world"
```

Package also includes utility `mts2splunk_receiver` which is used at P1 to subscribe to all ERS messages and dump them into a file, which is monitored by SPLUNK ERS application. Normally this application should be added to setup segment for all partitions.

### Bug reporting, feature requests
Followed up in [JIRA](https://its.cern.ch/jira/projects/ATDAQCCMRS)