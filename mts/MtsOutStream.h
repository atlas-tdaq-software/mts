/*! \file MrsOutStream.h This ers stream sends issues into a MTS system.
* ERS issues are converted to MTS IDL structures by MTS library.
* 
* \author Serguei Kolos, Andrei Kazarov
* \version 1.0
* \brief ERS stream implementation based on MTS
* \copyright  2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
* \date       March 2013
*/ 
      
#ifndef MTS_OUT_STREAM_H
#define MTS_OUT_STREAM_H

#include <ers/OutputStream.h>
#include <ipc/partition.h>
#include <mts/Sender.h>

namespace daq::mts
{

/*! This class is ERS stream implementation based on MTS.
    It includes multi-threaded implementation of MTS Sender, to be used by normal applications.
    \ingroup public
    \see ers::OutputStream
    */
class MtsOutStream : public ers::OutputStream
{
private:
    Sender		m_sender ;		/** MRS sender associated with stream */
        
public: 
    MtsOutStream( const std::string & partition ) ;
    
    void write( const ers::Issue & issue ) ; 
};

    /*! This class is ERS stream implementation based on MTS.
    It includes no-threaded implementation of MTS Sender, to be used by HLT forked applications.
    \ingroup public
    \see ers::OutputStream
    */
class MtsOutStreamST : public ers::OutputStream
{
private:
    Sender		m_sender ;		/** MRS sender associated with stream */
        
public: 
    MtsOutStreamST( const std::string & partition ) ;
    
    void write( const ers::Issue & issue ) ; 
};
} // daq::mts

#endif




