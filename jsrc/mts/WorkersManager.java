package mts;

class WorkersManager {

	/**
	 * User should take care that partition is valid (no exceptions from IPC)
	 * @param partition
	 */
	WorkersManager( final ipc.Partition partition )
	{
		m_partition = partition ;
		m_workers = null ;
	}

	/**
	 * Get all registered MTS workers
	 * @return iterator over all MTS workers, or NoWorkers exception
	 * @exception NoWorkers
	 */
	ipc.ObjectEnumeration<mts.worker> getAllWorkers() throws NoWorkers 
	{
		updateWorkers() ;		
		return m_workers ; // may be empty!
	}
	
	/**
	 * Refresh list of registered workers from IPC
	 * @exception NoWorkers
	 */
	private void updateWorkers() throws NoWorkers
	{
		try 	{	
			m_workers = new ipc.ObjectEnumeration<mts.worker>(m_partition, mts.worker.class, true) ;
			}
		catch ( final ipc.InvalidPartitionException ex )
			{
			throw new NoWorkers(m_partition.getName(), ex) ;
			}

		if ( m_workers.size() == 0 )
			{ throw new NoWorkers(m_partition.getName()) ; }	
	}
	
	/**
	 * Get a random worker from the list of available workers
	 * @return MTS worker as ipc.Object
	 * @exception NoWorkers
	 */
	synchronized ipc.Element<mts.worker> getWorker() throws NoWorkers
	{
		if ( m_workers == null || m_workers.isEmpty() ) // first call
		    {
		    updateWorkers() ;
		    }

		// TODO store locally ref to one random worker, refresh workers periodically (to get on board freshly restarted workers)
		while ( true ) {
		if ( m_workers.hasMoreElements() )
		    {
		    try {
			final ipc.Element<mts.worker> aworker = m_workers.nextElement() ;
			
			// TODO: _non_existent makes real remote call on the worker. It is not needed, since on a bad worker, 
			// user code will get the exception anyway, and for good workers it requires additional call for each reported message.
			// Let instead user's code handle the CORBA/RuntimeException exceptions by calling 
			if ( aworker.reference._non_existent() )
			    {
			    System.err.println("Found bad worker, will update... ") ;
			    updateWorkers() ; // throws NoWorkers
			    continue ;
			    }
			return aworker ;
			}
		    catch ( final java.util.NoSuchElementException ex )
			{    
			System.err.println("Not expected here: " + ex) ;
			break ;
			}
		    catch ( final RuntimeException ex )
			{    
			System.err.println("Bad worker detected: " + ex) ;
			updateWorkers() ;
			continue ;
			}		    
		    }
		else
		    {
		    m_workers.reset() ;
		    }
		}

		throw new NoWorkers(m_partition.getName()) ;
	}
	
	private final ipc.Partition m_partition ;
	private ipc.ObjectEnumeration<mts.worker> m_workers ;
}
