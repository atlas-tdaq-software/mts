/**
 * \file SubsValidator.cpp
 * Contains code used to validate a MTS subscription expression against the syntax defined in SubscriptionParser.h
 * To be used from Receiver.cpp and java MTSInStream via JNI.
 *
 *
 */

#include <mts/SubscriptionParser.h>

// generated from MTSInStream.java
#include <mts_MTSInStream.h>

namespace daq { namespace mts {
bool ext_assert_validity(const std::string& expr, std::string& ret_error) ; }}

JNIEXPORT jboolean JNICALL Java_mts_MTSInStream_assertValidity
  (JNIEnv * env, jobject java_this, jstring expr, jstring result)
{
	std::string expression (env->GetStringUTFChars(expr, NULL)) ;
	std::string res ;
	if ( daq::mts::ext_assert_validity(expression, res) == false )
		{
		// TODO fill in result and pass it back to Java
		return false ;
		}
return true ;
}

namespace daq {
namespace mts {

/**
 * To be called from Java native method and from Receiver::assert_validity
 *
 * @param expr subscription expression to be validated
 * @param ret_error external string to be filled with exact error in case validation fails
 * @return boolean result: true if expression passed validation, false otherwise
 */
bool ext_assert_validity(const std::string& expr, std::string& ret_error)
{
	std::string::const_iterator it = expr.begin(), end = expr.end() ;
	bool match ;
	MessageFilter filter ;
	SubscriptionParser<std::string::const_iterator> r(&filter) ;

	ShortMessage msg{"DEBUG", "mts::Test", "TestApp", {"TEST-QUAL"}} ;
	filter.set_message(&msg) ;

	phrase_parse(it, end, r, ascii::space, match) ;

	if ( it != expr.end() )
		{
		ret_error = "Failed to parse subscription expression: " + std::string(it, end) ;
		ret_error.append(". Example of valid expression: \n(app=HLT* and (not (param(algorithm)=muons and context(file)=test.cpp))) or sev=fatal or msg=rc::*") ;
		return false ;
		}

	return true ;
}

}}
