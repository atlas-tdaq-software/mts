/*
 * IPCPartitionManager.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: andrei
 */

#include <cstdlib>                    // for getenv

#include <string>
#include "mts/IPCPartitionManager.h"

using namespace daq::mts ;

IPCPartitionManager::IPCPartitionManager(const std::string& part)
:m_ipc_partition( part.empty() ? get_partition_name() : part )
{
}

IPCPartitionManager::IPCPartitionManager()
:m_ipc_partition(get_partition_name())
{
}

IPCPartitionManager::~IPCPartitionManager()
{
}

const char*
IPCPartitionManager::get_partition_name()
{
return std::getenv(part_env) ? std::getenv(part_env) : "" ;
}
