/**
 * Package MTS (java part) implements the Input and Output streams for the 
 * (<a href="http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/ERS/">Error Reporting System</a>)
 * using CORBA as transport layer for passing messages in distributed TDAQ system.<br>

 * <p>
 * No user-level API is exposed by MTS, since only ERS API is to be used for both sending and receiving messages. See {@link ers} package for description of the ERS framework implementation in Java.
 * <p>
 * Main classes provided by the package: {@link mts.MTSInStream}, {@link mts.MTSOutStream} are registered in ERS framework under 'mts' name, 
 * such that they are available for end users at sender and receiver sides using ERS API.
 * <p>
 * See mts/test/TestReceiver.java and mts/test/TestReporter.java for examples of using ERS with 'mts' streams for sending and receiving messages (ERS Issues).
 * <p>
 * Selection criteria syntax allows (see {@link ers.StreamManager#addReceiver}) to subscribe to certain messages specifying selection expressions using ERS Issue attributes:<br>
 * severity (sev), applicationID (app), messageID (msg), qualifiers (qual), grouping of expressions and arbitrary logical combination (and, or, not) of them.<br>
 * The syntax is defined using <a href="http://www.cs.cmu.edu/~pattis/misc/ebnf.pdf">EBNF notation</a>:
 * <p>
 * <code>
 * key = 'app' | 'msg' | 'qual' <br>
 * sev = 'sev' <br>
 * par = '(param|par)(+[a-zA-Z_-])' <br>
 * context_item = 'app' | 'host' | 'user' | 'package' | 'file' | 'function' | 'line' <br>
 * context = 'context(context_item)' <br>
 * sevlev = 'fatal' | 'error' | 'warning' | 'info' | 'information' <br>
 * token_wildcard = +[a-zA-Z0-9_.:-]-'*' <br>
 * token = +[a-zA-Z0-9_.:-] <br>
 * item = (key ('=' | '!=') token_wildcard) | (sev ('=' | '!=') sevlev) | ((par | context) ('=' | '!=') token)  <br>
 * factor = item | ( expression ) | ('not' factor) <br>
 * expression = '*' | factor *(('and' expression) | ('or' expression)) <br>
 * </code>
 * <br>
 * Keys and logical operators are case-insensitive. 
 * String comparison operations (line 3 above matching key with token) supports simple pattern matching.
 * <br>
 * Examples of selection criteria:<br>
 * <code>(sev=ERROR or sev=FATAL)</code><br>
 * <code>(msg=RCD* and app=Tile* and not qual=debug)</code>
 * 
 * @see ers.StreamManager#add_receiver
 * 
 * @see ers.IssueReceiver
 * 
 * @see ers.Logger
 * 
 * @see ers.Issue
 */
package mts ;
