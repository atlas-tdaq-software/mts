#include <functional>

#include <ers/ers.h>

#include "mts/ThreadPool.h"

using namespace daq::mts ;

void WorkerThread::operator()()
{
    std::function<void()> atask;
    while(true)
    {
        {   // acquire lock
            std::unique_lock<std::mutex> lock(pool.queue_mutex);

ERS_DEBUG(1, "Thread pool task size: " << pool.tasks_queue.size()) ;

            // look for a work item
            while(!pool.stop && pool.tasks_queue.empty())
            { // if there are none wait for notification
                pool.condition.wait(lock);
            }
 
            if(pool.stop) // exit if the pool is stopped
                return;
 
            // get the task from the queue
            atask = pool.tasks_queue.front();
            pool.tasks_queue.pop_front();
 
        }   // release lock
 
        // execute the task
        atask();
    }
}

// the constructor just launches some amount of workers
ThreadPool::ThreadPool(size_t nthreads, bool finish_flag)
    :   stop(false), finish(finish_flag)
{
std::cerr <<"Contructing ThreadPool" << std::endl ;
for(size_t i = 0; i < nthreads; ++i)
	{ worker_threads.push_back(std::thread(WorkerThread(*this))) ; }
}
 
// the destructor joins all threads
ThreadPool::~ThreadPool()
{
std::cerr <<"Destroying ThreadPool" << std::endl ;

if ( finish )
	{
std::cerr <<"Waiting for task queue to be empty" << std::endl ;
	while ( get_tasks_count() !=0 ) { std::this_thread::yield(); } ;
	}

{ std::unique_lock<std::mutex> lock(queue_mutex);
  if ( tasks_queue.size() )
	{
	std::cerr <<"There are still " << tasks_queue.size() << " tasks scheduled, removing them all..." << std::endl ;
	tasks_queue.clear() ;
	}
}

	// stop all threads
	stop = true;
	condition.notify_all();
 
	// join them
	for(size_t i = 0; i < worker_threads.size() ; ++i)
		worker_threads[i].join();

std::cerr <<"ThreadPool destroyed, there shall not be any more tasks around!" << std::endl ;
}
    
size_t ThreadPool::get_tasks_count() 
{
std::unique_lock<std::mutex> lock(queue_mutex) ;
return tasks_queue.size() ;
}
