/*! \file SubscriptionParser.h
 *
 *  A boost::spirit based implementation of parser of a subscription criteria syntax.
 *
 *  \author A. Kazarov Copyright 2012 CERN. All rights reserved.
 *  \date   Nov. 2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 * Selection criteria syntax allows to subscribe to certain messages specifying selection expressions using following ERS Issue attributes:
 * severity (sev), applicationID (app), messageID (msg), qualifiers (qual), parameters (par) and context (context),
 * grouping of expressions wiith parentheses and arbitrary logical combination (and, or, not) of them.
 * The syntax is defined using <a href="http://www.cs.cmu.edu/~pattis/misc/ebnf.pdf">EBNF notation</a>:
 *
\code
    key = 'app' | 'msg' | 'qual'
    sev = 'sev'
    par = '(param|par)(+[a-zA-Z_-])'
    context_item = 'app' | 'host' | 'user' | 'package' | 'file' | 'function' | 'line'
    context = 'context(context_item)'
    sevlev = 'fatal' | 'error' | 'warning' | 'info' | 'information'
    token_wildcard = -'*'+[a-zA-Z_.:-]-'*'
    token = +[a-zA-Z_.:-]
    quoted_string = '"'.+'"' | "'".+"'"
    item = (key ('=' | '!=') token_wildcard) | (sev ('=' | '!=') sevlev) | (par ('=' | '!=') token) | (context ('=' | '!=') quoted_string)
    factor = item | ( expression ) | ('not' factor)
    expression = '*' | factor *(('and' expression) | ('or' expression))
\endcode

 * Simple wildcard string comparision supported for app, msg and qual tokens (like app=HLT*). File names are short (no full path).
 * Example:
 * \verbatim(app=HLT* and (not (param(algorithm)=muons and context(file)=test.cpp)) or sev=fatal or msg=rc::*)\endverbatim
 */

#ifndef MTS_SUBSCRIPTION_PARSER_H
#define MTS_SUBSCRIPTION_PARSER_H

#include <string>
#include <map>
#include <vector>

#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix/bind.hpp>
#include <boost/phoenix/operator.hpp>

// needed only for ERS_DEBUG, comment out for production
// #include <ers/ers.h>

namespace qi = boost::spirit::qi ;
namespace ascii = boost::spirit::ascii ;

namespace daq {
namespace mts {

// class containing only values relevant for comparison, extracted from a full Message - for performance reasons
struct ShortMessage
{
	const std::string sev ;
	const std::string msgid ;
	const std::string appid ;
	const std::vector<std::string> quals ;
	const std::unordered_map<std::string, std::string> params ; 
	const std::unordered_map<std::string, std::string> context ;
} ;

/**
 * \class MessageFilter
 * 
 * This class is used to construct SubscriptionParser only once and then pass it a pointer to ERS/MTS messages for filtering (via set_message(const ShortMessage* message))
 * It contains boolean functions used by the Parser to accept or reject a particular message. These functions are used to build the Parser gramma (see SubscriptionParser below).
*/
class MessageFilter: boost::noncopyable
{
	const ShortMessage m_dummy_message ;
	const ShortMessage* m_message ;

public:
	MessageFilter(): m_dummy_message{ "DEBUG", "mts::Test", "TestApp", {"TEST_QUAL"}}, m_message(&m_dummy_message) { }

	void set_message(const ShortMessage* message) { m_message = message ; }

	bool severity_match(const std::string& sev) const
	{
//ERS_DEBUG(3, "Matching message severity " << m_message->sev << " to " << sev) ;
		return m_message->sev == sev ;
	}

	bool msgid_match(const std::string& msgid) const
	{
//ERS_DEBUG(3, "Matching msgid to " << msgid) ;
		return compare_strings(m_message->msgid, msgid) ;
	}

	bool appid_match(const std::string& appid) const
	{
//ERS_DEBUG(3, "Matching appid to " << appid) ;
		return compare_strings(m_message->appid, appid) ;
	}

	bool severity_not_match(const std::string& sev) const
	{
//ERS_DEBUG(3, "Matching severity not to " << sev) ;
		return !severity_match(sev) ;
	}

	bool msgid_not_match(const std::string& msgid) const
	{
//ERS_DEBUG(3, "Matching msgid not to " << msgid) ;
		return !msgid_match(msgid) ;
	}

	bool appid_not_match(const std::string& appid) const
	{
//ERS_DEBUG(3, "Matching appid not to " << appid) ;
		return !appid_match(appid) ;
	}

	bool quals_match(const std::string& qual) const
	{
//ERS_DEBUG(3, "Matching qual to " << qual) ;
		for ( auto mqual: m_message->quals )
			{
			if ( qual == mqual )
				{
				return true ;
				}
			}
		return false;
	}

	bool quals_not_match(const std::string& qual) const
	{
//ERS_DEBUG(3, "Matching qual not to " << qual) ;
		for ( auto mqual: m_message->quals )
			{
			if ( qual == mqual )
				{
				return false ;
				}
			}
		return true ;
	}

	bool params_match(const std::string& key, const std::string& value) const
	{
//ERS_DEBUG(3, "Matching param " << key << " to " << value) ;
		const auto found = m_message->params.find(key) ;
		if ( found == m_message->params.end() )
			{
			return false ;
			}
		if ( found->second == value )
			{
			return true ;
			}
		return false;
	}

	bool params_not_match(const std::string& key, const std::string& value) const
	{
//ERS_DEBUG(3, "Matching param " << key << " not to " << value) ;
		const auto found = m_message->params.find(key) ;
		if ( found == m_message->params.end() )
			{
			return true ;
			}
		if ( found->second == value )
			{
			return false ;
			}
		return true;
	}

	bool context_match(const std::string& key, const std::string& value) const
	{
// ERS_DEBUG(0, "Matching context " << key << " to " << value) ;
		const auto found = m_message->context.find(key) ;
		if ( found == m_message->context.end() )
			{
			return false ;
			}
		if ( found->second == value )
			{
			return true ;
			}
		return false;
	}

	bool context_not_match(const std::string& key, const std::string& value) const
	{
// ERS_DEBUG(0, "Matching context " << key << " not to " << value) ;
		const auto found = m_message->context.find(key) ;
		if ( found == m_message->context.end() )
			{
			return true ;
			}
		if ( found->second == value )
			{
			return false ;
			}
		return true;
	}

	bool file_match(const std::string& value) const
	{
		const auto found = m_message->context.find("file") ;
		if ( found == m_message->context.end() )
			{
			return false ; // shall not happen
			}
		std::string shortfilename = found->second.substr(found->second.find_last_of('/')+1) ;
		return value == shortfilename ;
	}

	bool file_not_match(const std::string& value) const
	{
		const auto found = m_message->context.find("file") ;
		if ( found == m_message->context.end() )
			{
			return false ; // shall not happen
			}
		std::string shortfilename = found->second.substr(found->second.find_last_of('/')+1) ;
		return value != shortfilename ;
	}

	bool function_match(const std::string& value) const
	{
		const auto found = m_message->context.find("function") ;
		if ( found == m_message->context.end() ) {
			return false ; // shall not happen
		}
		return value == found->second ;
	}

	bool function_not_match(const std::string& value) const
	{
		const auto found = m_message->context.find("function") ;
		if ( found == m_message->context.end() ) {
			return false ; // shall not happen
		}
		return value != found->second ;
	}


// service function used to compare string using simple pattern-matching
// i.e. sample may be of form some* or *some
bool	compare_strings(const std::string& token, const std::string& sample) const
{
	if ( sample.size() > token.size() + 1 ) { return false ; } ;
	if ( sample.front() == '*' ) {
		return !token.compare(token.length() - sample.size() + 1, sample.size()-1, sample, 1, sample.size()-1) ;
	}
	if ( sample.back() == '*' ) {
		return !token.compare(0, sample.size()-1, sample, 0, sample.size()-1) ;
	}
	
	return !token.compare(sample) ;
} 
} ; 

template <typename Iterator>
struct SubscriptionParser : qi::grammar<Iterator, bool(), ascii::space_type>
{
    SubscriptionParser (MessageFilter* match):
	SubscriptionParser::base_type(expr)

		{
		using qi::_val;
		using qi::_1;
		using qi::_2;
		using qi::_3;
		using qi::lexeme, qi::char_ ;
		using boost::spirit::ascii::string;

		// no spaces
		id = lexeme[ +qi::char_("a-zA-Z0-9_:.\\-") ] ;

		// just to allow user to write either sev=error or sev='error' or param(code)='124' etc.
 
		value %= '\'' >> id >> '\''  | id ;
		value_wld_end %= '\'' >> id >> -char_("*") >> '\'' | id >> -char_("*") ;
		value_wld_start %= '\'' -char_("*") >> id >> '\'' | -char_("*") >> id ;
 		value_wld %= value_wld_end | value_wld_start ;

 		quoted_string %= lexeme['"' >> +(char_ - '"') >> '"'] | lexeme['\'' >> +(char_ - '\'') >> '\''];

		NOT	= qi::no_case["not"] ;
		AND = qi::no_case["and"] ;
		OR	= qi::no_case["or"] ;

		sev = qi::no_case["sev"] ;
		msg = qi::no_case["msg"] ;
		app = qi::no_case["app"] ;
		qual = qi::no_case["qual"] ;
   		param = qi::no_case["param"] | qi::no_case["par"] ;

		context = qi::no_case["context"] ;
		context_item =  qi::no_case[ascii::string("app")] |
		qi::no_case[ascii::string("user")] |
		qi::no_case[ascii::string("host")] |
		qi::no_case[ascii::string("package")] | 
		qi::no_case[ascii::string("line")] ;

		context_function = qi::no_case[ascii::string("function")] ;

		context_file =  qi::no_case[ascii::string("file")] ;

		context_eq =  ( context >> '(' >> context_item >> ')'  >> "="   >> value ) [ _val = boost::phoenix::bind(&MessageFilter::context_match, boost::ref(*match), _2, _3) ] ;
		context_neq = ( context >> '(' >> context_item >> ')'  >> "!="  >> value ) [ _val = boost::phoenix::bind(&MessageFilter::context_not_match, boost::ref(*match), _2, _3) ] ;
		context_file_eq =  ( context >> '(' >> "file" >> ')'  >> "="   >> (quoted_string | value) ) [ _val = boost::phoenix::bind(&MessageFilter::file_match, boost::ref(*match), _2) ] ;
		context_file_neq = ( context >> '(' >> "file" >> ')'  >> "!="  >> (quoted_string | value) ) [ _val = boost::phoenix::bind(&MessageFilter::file_not_match, boost::ref(*match), _2) ] ;

		context_func_eq =  ( context >> '(' >> "function" >> ')'  >> "="   >> (quoted_string | value) ) [ _val = boost::phoenix::bind(&MessageFilter::context_match, boost::ref(*match), "function", _2) ] ;
		context_func_neq = ( context >> '(' >> "function" >> ')'  >> "!="  >> (quoted_string | value) ) [ _val = boost::phoenix::bind(&MessageFilter::context_not_match, boost::ref(*match), "function", _2) ] ;

		par_name %= qi::lexeme [ +qi::char_("a-zA-Z0-9_-") ] ; // ERS issue parameter (more correctly: as C++ variable, it should start with a character

		sev_level %=	qi::no_case[ascii::string("error")] |
						qi::no_case[ascii::string("fatal")] |
						qi::no_case[ascii::string("warning")] |
						qi::no_case[ascii::string("information")] |  // neither LOG nor DEBUG are supposed to be passed vi MTS
						qi::no_case[ascii::string("info")] ;

   		bool_item =
	 		 ( app 	>> "=" 	>> value_wld ) 		[ _val = boost::phoenix::bind(&MessageFilter::appid_match, boost::ref(*match), _2) ]
			|( app 	>> "!=" >> value_wld ) 		[ _val = boost::phoenix::bind(&MessageFilter::appid_not_match, boost::ref(*match), _2) ]
			|( msg 	>> "="  >> value_wld ) 		[ _val = boost::phoenix::bind(&MessageFilter::msgid_match, boost::ref(*match), _2) ]
			|( msg 	>> "!=" >> value_wld ) 		[ _val = boost::phoenix::bind(&MessageFilter::msgid_not_match, boost::ref(*match), _2) ]
			|( sev 	>> "=" 	>> sev_level ) 		[ _val = boost::phoenix::bind(&MessageFilter::severity_match, boost::ref(*match), _2) ]
			|( sev	>> "!=" >> sev_level )		[ _val = boost::phoenix::bind(&MessageFilter::severity_not_match, boost::ref(*match), _2) ]
			|( param >> '(' >> par_name >> ')'  >> "="  >> value ) [ _val = boost::phoenix::bind(&MessageFilter::params_match, boost::ref(*match), _2, _3) ]
			|( param >> '('	>> par_name >> ')'  >> "!=" >> value ) [ _val = boost::phoenix::bind(&MessageFilter::params_not_match, boost::ref(*match), _2, _3) ]	
			|( context_eq | context_neq | context_file_eq | context_file_neq | context_func_neq | context_func_eq) [_val=_1]
			|( qual >> "!=" >> value_wld ) 		[ _val = boost::phoenix::bind(&MessageFilter::quals_not_match, boost::ref(*match), _2) ] 
			|( qual >> "=" 	>> value_wld ) 		[ _val = boost::phoenix::bind(&MessageFilter::quals_match, boost::ref(*match), _2) ] ;

    	factor =
			bool_item[_val=_1]
			| '(' >> expr[_val=_1] >> ')'
			| (NOT >> factor)[_val=!_1]
			;

    	expr =	qi::lit('*')[_val=true] |
			 factor[_val=_1] >>  *( (AND >> expr)[_val = _val && _1] | (OR >> expr)[_val = _val || _1] )
			;
		}

    // here we declare our atoms, composing the syntax. second template parameter is the type of synthesised attribute _val
	qi::rule<Iterator, bool(), ascii::space_type> 	expr, bool_item, factor, context_eq, context_neq, context_file_eq, context_file_neq, context_func_eq, context_func_neq ;
	qi::rule<Iterator> 				NOT, AND, OR ;
	qi::rule<Iterator, std::string()> 		msg, app, sev, sev_level, qual, value, value_wld, value_wld_end, value_wld_start, id, param, par_name, context,
							context_item, context_file, context_function, quoted_string ;
}; // gramma structure

} }

#endif
