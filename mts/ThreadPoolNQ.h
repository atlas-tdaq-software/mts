/*!
 *  \file ThreadPoolNQ.h
 *  \brief NQ = No Queue. Implementation of a thread pool model without internal task queue.
 *
 *  ThreadPoolNQ relies on TaskProvider abstract class, also defined in this file. The tasks for execution are not stored in the pool queue
 *  (like in TBB implementation), instead they are queried from the providers by the threads ready to execute.
 * 
 *  \author A. Kazarov
 *  \date Nov 2012
 *
 *  \copyright 2012 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

#ifndef MTS_TPOOL_H
#define MTS_TPOOL_H

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <vector>

#include "mts/defs.h"

namespace daq {
namespace mts {


/*! \brief Abstract class for getting the jobs ready for execution in a thread pool. Implemented by real providers of the tasks.
 *
 *  \ingroup private
*/
class TaskProvider	
{
public:
class NoTasks {} ; // just an exception

/*! \brief called by ThreadPoolNQ when a thread from the pool is ready to execute a Task
 *
 * \ingroup private
 * \return Task a Task for execution or NoTasks exception
 * \throw NoTasks is case no task is ready to be executed
 */
virtual Task get_next_task() = 0 ;

virtual ~TaskProvider() {} ;
} ;

class ThreadPoolNQ ; // forward declaration

/*! 	\brief Class for instantiation of std::threads
		\ingroup private
*/
class WorkerThread {
public:
    WorkerThread(ThreadPoolNQ &s) : pool(s) { } /// constructor

    void operator()() ;  /// needed to construct std::thread, this is where all real code is executed
    ThreadPoolNQ &pool ; /// ref to the holder pool, needed to access data shared by all threads
} ;

       /*! \brief Thread pool implementaion, used by MTS.
        * \ingroup public
	 	* This thread pool does not contain internal queue of all tasks picked up by the worker threads.
		* Instead, it relies on implementation of TaskProvider::get_next_task() method, returning the Task
		* ready for execution or NoTasks exception, in latter case the thread sleeps on a semaphore and eventually
		* waken up by the client with awake_one() call.
		* In this way, the client code is responsible for effective queuing and task scheduling.
		* All threads are created at construction time.
		* Implementers: mts::Worker
       */
class ThreadPoolNQ {
friend class WorkerThread ;

public:
	/**
	 * Constructor of a ThreadPool
	 *
	 * Creates and launches all pool threads and waits for them to wait on condition,
	 * getting ready for accepting the new tasks.
	 * @param pool_size number of threads in the pool
	 * @param tp reference to TaskProvider
	 * @param expandable set the pool to automatically grow as more threads get "busy"
	 */
    ThreadPoolNQ(size_t pool_size, TaskProvider& tp, bool expandable = false) ;

    /**
     * Destructor sets stop flag, notifies and joins all threads
     */
    ~ThreadPoolNQ() ;

	/**
	 * Called by user, notifies the pool that there is a new task ready, awakes one thread.
	 */
    void awake_one() { std::unique_lock<std::mutex> lock(condition_mutex) ; ready = true ; condition.notify_one(); }

    size_t	get_busy_threads() 	{ return busy_counter ; }    
    size_t	get_total_threads() 	{ return total_counter ; }    

private:

    size_t	increase_pool() ;    

    // synchronization
    std::mutex 					condition_mutex ;
    std::condition_variable 	condition ;

    // container to keep track of threads so we can join them
    std::vector<std::thread> 		worker_threads ;
  
    bool					expandable ;
    TaskProvider& 			task_provider ; // provider of jobs

    std::atomic<size_t> 	ready_counter ; // counter of all ready threads, used in constructor
    std::atomic<size_t> 	total_counter ; // number of threads started in the pool
    std::atomic<size_t> 	busy_counter ; 	// number of threads currently busy i.e. executing a job
    std::atomic<bool> 		stop ;     	// stop flag
    bool 		            ready ;     	// ready flag for condition
    size_t					initial_size ;	// initial pool size, used to increase pool size
};

}} // daq::mts

#endif
