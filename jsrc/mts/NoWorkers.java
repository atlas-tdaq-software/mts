package mts;

import ers.Issue;

public class NoWorkers extends Issue {

	public String partition ;
	
	public NoWorkers(final String partition)
	{
	super("No MTS workers found registered in partition %s", partition) ;
	}

	public NoWorkers(final String partition, final Exception cause)
	{
	super("No MTS workers found registered in partition %s", partition, cause) ;
	}
}
