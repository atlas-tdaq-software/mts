package mts;

import ers.Issue;

public final class PublicationFailure extends Issue
{
public String partition ;

public PublicationFailure(final String partition)
{
super("Failed to publish MTS subscription info in partition %s", partition) ;
}

public PublicationFailure(final String partition, final Exception cause)
{
super("Failed to publish MTS subscription info in partition %s", partition, cause) ;
}
}

