/*!
 *  \file mts/Receiver.h
 *  \brief Defines user API for receiving ERS messages via MTS transport layer
 *  
 *  Used by MtsInStream implementation of ERS output stream, \see MtsInStream.cpp/h files
 *  Defines 2 related classes: CORBAReceiver and ERSReceiver
 *
 *  \author Andrei Kazarov
 *  \date Nov 2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef MTS_RECEIVER_H
#define MTS_RECEIVER_H

#include <string>
#include <list>
#include <map>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include <ers/Issue.h>
#include <ers/InputStream.h>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ers2idl/ers2idl.hh>

#include "mts/mts.hh"
#include "mts/exceptions.h"
#include "mts/SubscriptionManager.h"

namespace daq {
namespace mts {

class CORBAReceiver ;

/*! \brief Implements the ERS receiver functionality.
 *
       Inherits from ers::InputStream

	   Real CORBA Receiver is included as a member CORBAReceiver* m_receiver, since we have to destroy CORBA objects with _destroy()
	   and not via destructor

	   This class is registered in ERS under name 'mts'
	   ERS_REGISTER_INPUT_STREAM( daq::mts::ERSReceiver, "mts", format )
	   and can be used as in examples/mts_ers_receiver.cpp

	   It contains the "real" transport layer receiver as

	\sa mts_ers_receiver.cpp
    \ingroup private
 */
class ERSReceiver: public ers::InputStream
{
// friend ::ers::InputStream* InputStreamRegistrator::create(const std::string&) ;
friend class CORBAReceiver ;
public:
        /*! Constructor for a specific partition.
            \param args Stream constructor arguments, e.g. partition name and subscription expression.
         */
	ERSReceiver( const std::initializer_list<std::string>& args ) ;

	~ERSReceiver() ;

	/*!
	 * Link to ERS layer, calls protected function from the base class ers::InputStream
	 * \param an_issue Issue to report to the subscribed client
	 * \see ers::InputStream#receive
	 */
	void notify(const ers::Issue& an_issue) { return receive(an_issue) ; } ;

private:

	/**
	 * \brief Examines the validity of subscription given by user by creating a parser for a test Issue.
	 * NB: also used from Java via JNI
	 * \param expr subscription expression
	 * \throw ::mts::BadExpression if parsing of the expressoin fails
	 */
	void assert_validity(const std::string& expr) ;	// throws ::mts::BadExpression if parsing fails
       
	CORBAReceiver* m_receiver ; /*!< real CORBA transport layer receiver */

	bool 					m_sync_subscription = false ; 	/*!< is the synchronous subscription is reqreuied */
	std::string				m_subscription ;
	size_t					m_n_workers = 0 ; 			/*!< counter of responded workers */
    std::mutex 				m_sync_mutex ;			/*!< mutex for condition */
    std::condition_variable m_condition ;			/*!< wait/notify */
	size_t					m_subs_timeout = 100 ; 	/*!< synchronous subscription timeout, ms */
	} ;


/*! \brief Implements the IDL interface of the MTS receiver, \see idl/mts.idl. Used from ERSReceiver only.
 *
 *  Inherits from SubscriptionManager, since it needs to publish the subscription to MTS IS server
 *  Private class, used only from ERSReceiver
    \ingroup private
    \see ERSReceiver
 */
class CORBAReceiver: public IPCObject<POA_mts::receiver, ::ipc::multi_thread>, public SubscriptionManager
	{
friend class ERSReceiver ;

	/** Constructor for a specific partition.
		@param      parent    	Holder
		@param      filter    	Subscription expression
		@throw SubscriptionRegistrationFailure
	 */
	CORBAReceiver( const std::string & partition, ERSReceiver* parent ) ;

	~CORBAReceiver() ;
	
	/*!
	 * Unsubscribe, to be called from parent destructor before calling IPC _destroy
	 */
	void remove_subscriptions( ) ;

	/*! implementation of CORBA receiver::notify IDL interface
		\param    an_issue MTS IDL issue received via CORBA from a worker
	 */
	void notify(const ::ers2idl::Issue& an_issue) ;

	/*! implementation of CORBA receiver::subscribed IDL interface
		\param    subscription subscription which is passed back from worker to subscriber to identify the subscription
	 */
	void subscribed(const char* subscription) ;

	/* helper function, conversion from MTS structure to ERS chained issue */
	// static ers::Issue * mts2ersIssue(const ::ers2idl::Issue& message) ;

	/* stat function */
	void count_rate() ;

	/* parent ERS receiver */
	ERSReceiver*				m_ers_receiver ;

	/* list of subscriptions on workers, used in destructor to unsubscribe */
	// std::list< std::pair< ::CORBA::Long, ::mts::worker_var> >	m_subscriptions ;

	// to be moved to a separate class
	std::atomic<size_t>			m_message_counter ;
	} ;

}} // daq::mts

#endif
