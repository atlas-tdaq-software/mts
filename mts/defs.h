/**	\file defs.h
 	  \brief      Common data structures for MTS package.
  	\author     A. Kazarov
    \copyright  2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
	  \date       March 2013
 */

/**
\mainpage (DAQ/HLTI Software Documentation: doxygen for release packages)
*/

#ifndef MTS_DEFS_H
#define MTS_DEFS_H

#include <chrono>
#include <functional>
#include <memory>

#include <tbb/concurrent_queue.h>

#include "ers2idl/ers2idl.hh"

#include "mts/mts.hh"

namespace daq {
namespace mts {

/**
  *  @defgroup mts Package MTS: Message Transfer System.
  *
  *  @brief This package provides API and utilities used by TDAQ applications
  *  to exchange messages using send/ubscribe/nofify pattern.
  *
  *  It is a re-implementation of the former MRS package. The key difference is that sending API is hidden from users by ERS layer.
  *  See also: https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/tdaq-common/production/html/main.html
  *
  *  Detailed design documentation can be found here:
  *  https://its.cern.ch/jira/secure/attachment/14921/HLD_draft_v0.4.pdf
  *
  *  Corresponding Jira project, for bug reporting etc:
  *  https://its.cern.ch/jira/browse/ATDAQCCMRS
  *
  */


/** \page MTS_package Message Transfer System (MTS) user documentation

   [Detailed design documentation (PDF)](https://its.cern.ch/jira/secure/attachment/14921/HLD_draft_v0.4.pdf)

   ### Introduction

   Functionality provided by MTS library: sending and subscribing/receiving messages in ERS system. Available in Java and C++.

   ### Subscribing to messages

   %Subscription is done using ERS API. However the syntax for selecting the messages is MTS-specific:

   %Subscription criteria syntax: see daq::mts::Subscription and also detsails on page 7 of https://its.cern.ch/jira/secure/attachment/14921/HLD_draft_v0.4.pdf

   \see ers::StreamManager::add_receiver 	
   \see daq::mts::Subscription

   %Examples:

   C++\n
   mts/examples/mts_ers_receiver.cpp

   Java\n
   mts/jsrc/test/TestReceiver.java

   ### Sending messages

   The sending part of MTS is completely hidden from the end users by the ERS layer (see reporing to ERS streams). ERS documentation:

   [C++](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/code/doxygen/tdaq-10-00-00/html/md__cvmfs_atlas_8cern_8ch_repo_sw_tdaq_tdaq-common_tdaq-common-10-00-00_installed_share_doc_ers_README.html)\n
   [Java](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/code/javadoc/tdaq-10-00-00/ers/package-summary.html)
  
  %Examples:

  C++\n
  mts/examples/ers_sender.cc

  Java\n
  mts/jsrc/test/TestReporter.java

  ### Configuration of ERS stream for MTS

The name of ERS stream implmented by MTS is 'mts'. It accepts partition name as a parameter (TDAQ_PARTITION environment can also be used for that), e.g.
\code
  >export TDAQ_ERS_ERROR='mts,lstderr'
  >export TDAQ_ERS_FATAL='mts(initial),lstderr'
\endcode

  ### Bugs and discussions:

   https://its.cern.ch/jira/browse/ATDAQCCMRS

*/

using Message = ::ers2idl::Issue ;	            /*!< IDL ers2idl::Issue, see ers2idl/mts.idl */
using MessageSP = std::shared_ptr< Message > ;  /*!< shared pointer to IDL Message */
using Receiver = ::mts::receiver_var ;          /*!< shared CORBA pointer to mts::receiver, see idl/mts.idl */
using ReceiverP = ::mts::receiver_ptr	;         /*!< raw CORBA pointer to mts::receiver */

// we use TBB implementation of non-blocking thread-safe double-end queue
// to decouple providers from receivers in multithreaded code
using MessagesQueue =	tbb::concurrent_queue< MessageSP >  ; /** Queue of pointers to Messages that are delivered to subscribers by workers in thread pool */

using Task = std::function<void()>  ; 	// just a helper class for thread pool implementation

/*!
 A helper template function to return a time required for execution of a method
 \param T template parameter, time precision, e.g. std::chrono::microseconds std::chrono::milliseconds
 \param exec an executable Task
 \return duration, time that took execution of exec
 */
template <typename T>
int timed_execute(Task exec)
{
auto start = std::chrono::high_resolution_clock::now() ;
exec() ;
auto end = std::chrono::high_resolution_clock::now() ;
return std::chrono::duration_cast<T>(end-start).count() ;
}

}} // daq::mts
#endif
