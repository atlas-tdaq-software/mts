/*!
 *  \file Sender.hxx
 *  
 *  Defines user API for sending out ERS messages via MRS transport layer
 *  Primarily used by MrsOutStream implementation of ERS output stream, see
 *  MtsOutStream.ccp/hpp files
 *
 *  \author Andrei Kazarov
 *  \date Nov 2012.
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef MTS_SENDER_H
#define MTS_SENDER_H

#include <map>
#include <memory>
#include <atomic>

#include <ers/Issue.h>

#include "mts/mts.hh"
#include "mts/defs.h"
#include "mts/ThreadPoolNQ.h"
#include "mts/WorkersManager.h"

namespace daq {
namespace mts {

/**
 * \brief Implements the MTS sender functionality.
 *
 *   Two versions are provided: mutithreaded non-blocking version and no-threaded version (n_threads = 0 in constructor).
 *   Multithreaded version uses local queue and thread pool to isolate user sending code from network delivery code. Sending code
 *   simply places message to the buffer and returns. The delivery job is done asynchronously by worker threads.
 *   It is to be used as 'mts' ESR stream for normal applications for message reporting.
 *   No-threaded version is to be used by HLT applications, which needs to shutdown all threads prior to forking.
 *   In this version the caller (user) thread is used to make a remote CORBA call to a mts worker for actual message delivery.
 *
 *  \warning reporting of message in no-threaded version may block for few seconds in case of a worker node is unavailable or busy.
 *  Also, no-threaded version has limited delivery performance of about ~6.5 kHz (since two-way CORBA call is used).
 */
class Sender: public WorkersManager, public TaskProvider
{
friend class MtsOutStream ;
friend class MtsOutStreamST ;

constexpr static size_t	n_threads = 4 ; /*!< # of threads in sending thread pool (for threaded version) */

private:
        
	/*! Constructor for a specific partition.
		\param      p    		Partition name
		\param      threads    	Number of worker threads, 0 for non-threaded implementation
		\see MtsOutStreamST, MtsOutStream - classes for implementing ERS out streams
		\throw daq::mts::NoWorkers, daq::ipc::InvalidPartition
	 */
	Sender( const std::string& p, size_t threads = n_threads ) ; // throw ( daq::mts::NoWorkers, daq::ipc::InvalidPartition ) 


	/*! Default constructor for $TDAQ_PARTITION partition and 4 worker threads.
		\see MtsOutStreamST, MtsOutStream - classes for implementing ERS out streams
		\throw daq::mts::NoWorkers, daq::ipc::InvalidPartition
	 */
	Sender(  ) ; // throw ( daq::mts::NoWorkers, daq::ipc::InvalidPartition ) 

    virtual ~Sender() ;

	/*!
	* Local method, do not block, creates IDL Issue and buffers it locally to for aync delivery by the threadpool
	* \param issue an ers::Issue to report
	*/
	void report_nonblock(const ers::Issue & issue) ;

 	/*! Local method, may block on network call, creates IDL Issue and calls report_remote which is CORBA remote call
	 * \param issue an ers::Issue to report
     */
	void report_block(const ers::Issue & issue) ;
	
	/*!
	 * Calls remote report method on a random worker. May block, called from independent thread.
	 * \param message smart pointer to a Message structure to be sent out
	 */
	void report_remote(MessageSP message) noexcept ;

	/*! Implementation of TaskProvider abstract class. Called from threads in a pool.
		\ingroup public
		\return next Task (runnable) to be executed by a thread pool
		\throws NoTasks if nothing to be done, put ThreadPool in sleep mode
	 */
	Task get_next_task() ; // throw(NoTasks) 

	std::unique_ptr<ThreadPoolNQ>	m_thrpool ;		/*! < pool of worker threads */
	MessagesQueue			m_messages_queue ; 	/*! queue of smart pointers to Messages to be sent out */

	std::atomic<size_t>	m_messages_queued ; // counter of queued messages
	std::atomic<size_t>	m_messages_sent ;   // counter of actually sent messages

	} ;

}} // daq::mts

#endif
