package test;


// a custom issue with parameters and formatted message
public class MyTestIssue extends ers.Issue {
	public String reason;
	public int port_number;

	public MyTestIssue(String reason, int port_number, ers.Issue cause) {
		super("My-Test-Issue happened because of %s on a port number %d", reason,
				port_number, cause);
	}

	public MyTestIssue(String reason, int port_number) {
		super("MyTestIssue happened because of %s on a port number %d", reason,
				port_number);
	}

	public MyTestIssue(String reason) {
		super("MyTestIssue happened because of %s on a port number XX", reason);
	}
};
