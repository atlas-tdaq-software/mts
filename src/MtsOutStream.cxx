/**
 *  \file MtsOutStream.cxx
 *  \package MTS
 *
 *  Created by Serguei Kolos on 18.11.05 (MRS package).
 *  Adopted by Andrei Kazarov on Nov 2012.
 *  Copyright 2005-2012 CERN. All rights reserved.
 *
 */


#include <iostream>            // for operator<<, endl, basic_ostream, cerr
#include <string>              // for allocator, string

#include "ers/OutputStream.h"  // for ERS_REGISTER_OUTPUT_STREAM, OutputStream
#include "ipc/core.h"          // for IPCCore

#include "mts/Sender.h"        // for Sender
#include "mts/defs.h"          // for mts
#include "mts/MtsOutStream.h"

using namespace daq::mts ;

// multi-threaded implementation: use where no-blocking send is required
ERS_REGISTER_OUTPUT_STREAM( MtsOutStream,   "mts-mt", 	partition )

// default implementation is single-threaded: use elsewhere
ERS_REGISTER_OUTPUT_STREAM( MtsOutStreamST, "mts", 	partition )

MtsOutStream::MtsOutStream( const std::string & partition )
  : m_sender( partition )
{
	if ( !IPCCore::isInitialised() )
		{
		std::cerr << " ERROR [MtsOutStream] IPC Core not initialized, messages will not be sent until IPCCore::init(argc,argv) called" << std::endl ;
		}		
}

MtsOutStreamST::MtsOutStreamST( const std::string & partition )
  : m_sender( partition, 0)
{
	if ( !IPCCore::isInitialised() )
		{
		std::cerr << " ERROR [MtsOutStream] IPC Core not initialized, messages will not be sent until IPCCore::init(argc,argv) called" << std::endl ;
		}		
}

/** Sends the issue to the underlying MTS stream. Non-blocking send, uses queue and thread pool.
 * \param issue reference to the issue to send 
 */
void
MtsOutStream::write( const ers::Issue & issue )
{
	m_sender.report_nonblock(issue) ; // no throw
	chained().write(issue) ; // next steam in the chain
}

/** Sends the issue to the underlying MTS stream
    Blocking send, does not use thread pool (which is not created)
  * \param issue reference to the issue to send 
  */
void
MtsOutStreamST::write( const ers::Issue & issue )
{
	m_sender.report_block(issue) ; // no throw
	chained().write(issue) ; // next steam in the chain
}
