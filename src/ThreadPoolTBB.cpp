#include <functional>

#include <ers/ers.h>

#include "mts/ThreadPoolTBB.h"

using namespace daq::mts ;

void WorkerThreadTBB::operator()()
{
    std::function<void()> atask;
    while(true)
    {
ERS_DEBUG(1, "Thread pool task size: " << pool.tasks_queue.unsafe_size()) ; 

    if ( pool.tasks_queue.try_pop(atask) ) // if there is something in the queue
	{
	atask() ; 			// execute a task, continue
	}
    else // wait
	{
	std::unique_lock<std::mutex> lock(pool.queue_mutex) ;
	pool.condition.wait(lock [&]{ return pool.stop ; })
	if ( pool.stop ) { return ; } ;
	}
    }   
}

// the constructor just launches some amount of workers
ThreadPoolTBB::ThreadPoolTBB(size_t nthreads)
    :   stop(false)
{
std::cerr <<"Contructing ThreadPool" << std::endl ;
for(size_t i = 0; i < nthreads; ++i)
	{ worker_threads.push_back(std::thread(WorkerThreadTBB(*this))) ; }
}
 
// the destructor joins all threads
ThreadPoolTBB::~ThreadPoolTBB()
{
std::cerr <<"Destroying ThreadPool" << std::endl ;

	while (tasks_queue.try_pop()) ; // clear 

	// stop all threads
	{	std::unique_lock<std::mutex> lock(pool.queue_mutex) ;
		stop = true;
	}
	condition.notify_all();
	
		// join them
	for(size_t i = 0; i < worker_threads.size() ; ++i)
		worker_threads[i].join();

std::cerr <<"ThreadPool destroyed, there shall not be any more tasks around!" << std::endl ;
}
    
size_t ThreadPoolTBB::get_tasks_count() 
{
//std::unique_lock<std::mutex> lock(queue_mutex) ;
return tasks_queue.unsafe_size() ;
}
