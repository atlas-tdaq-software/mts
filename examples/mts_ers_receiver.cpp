/**
 * \file mts/examples/mts_ers_receiver.cpp
 * Working example of a command-line C++ ERS receiver using MTS transport.
 * \author Andrei Kazarov
 * \date 2012
 * source taken from ers/test/receiver.cpp + added IPCCore::init
 * 		in short:
 * 		struct MyAllIssueReceiver : public ers::IssueReceiver
 *	    ers::StreamManager::instance().add_receiver( "mts", std::string(subscription), new MyAllIssueReceiver(verb, sleep) );
 *
 */


#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <list>
#include <cstdlib>

#include <signal.h>

#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <ers/ers.h>
#include <ipc/core.h>
 
using std::chrono::system_clock;

namespace po = boost::program_options ;

std::mutex out_mutex ;
std::list<ers::IssueReceiver*> receivers ;
// bool stopthread = false ;

ERS_DECLARE_ISSUE(      mts,
                        WrongProgramOptions,
                        "Wrong program options passed: " << text,
                        ((std::string)text )
                )

/* just to exit properly by ^C or other signals */
void sig_func(int) {}

void myatexit()
{
std::cerr << "Exiting application: removing ERS receivers" << std::endl ;
for ( auto rec: receivers )  ers::StreamManager::instance().remove_receiver(rec) ;
std::cerr << "ERS receivers removed" << std::endl ;
}

struct MyReceiver : public ers::IssueReceiver
{
    int sleep_time = 0 ;
    bool verbose ;
    bool stat ;
	std::atomic<int> counter ;
	std::atomic<long> total_delay ;
	long max_delay = 0 ;
	static const size_t	stat_period ; 				/*!<  Interval for publishing statistics, secs */
	std::condition_variable 	m_thread_cond ;
	std::mutex 			m_thread_cond_mx ;
	std::thread	m_stat_thread ;

    MyReceiver( bool verb, bool i_stat, int stime ): sleep_time(stime), verbose(verb), stat(i_stat), counter(0), total_delay(0),
    		m_stat_thread(std::bind(&MyReceiver::count_rate, this)) { }


    ~MyReceiver()
	{
	// stopthread = true ;
	std::unique_lock<std::mutex> tlock(m_thread_cond_mx) ;
	m_thread_cond.notify_all() ;
	tlock.unlock() ;

	m_stat_thread.join() ;
	//	  if ( verbose ) 
	std::cout << "Exiting Receiver, total messages received: " << counter << std::endl ;
	}

    void receive( const ers::Issue & issue )
    {

	// const std::time_t now = std::time(NULL);
	std::time_t now = system_clock::to_time_t(std::chrono::system_clock::now());
 
	long lapse = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - issue.ptime()).count() ;
	total_delay += lapse ;
	if ( max_delay < lapse ) max_delay = lapse ;
	++counter ;

	if ( verbose )
		{
		std::lock_guard<std::mutex> lock(out_mutex) ; 
  		//std::cerr << "##########################################" << std::endl ;
		std::cerr << ctime(&now) << " got message " << counter << " time: "
		<< issue.time() << " with delay " << lapse << std::endl ;
   		std::cout << issue << std::endl ;
		}

	if ( sleep_time )
		{ std::this_thread::sleep_for(std::chrono::milliseconds(sleep_time)) ; }

    }

    // async stat thread
    void 		count_rate()
    {
    	while (true)
    	{
    	size_t r_prev = counter, td_prev = total_delay ;
   
	std::unique_lock<std::mutex> lock(m_thread_cond_mx) ;
	if ( m_thread_cond.wait_for(lock, std::chrono::seconds(stat_period )) == std::cv_status::no_timeout )
		{ 
		ERS_DEBUG(1, "Got signal, exiting stat thread") ;
		return ;
		}

    	size_t r_now = counter, td_now = total_delay ; ;

    	if (stat) {
		ERS_LOG("Receiving rates (msgps): " << (r_now - r_prev)/stat_period << ": max lapse: " <<  max_delay << ": av lapse: " << 
		(r_now - r_prev ? (td_now - td_prev)/(r_now - r_prev) : 0)) ;
		}
    	}
    }

};

const size_t MyReceiver::stat_period = 5 ;

struct MyFatalIssueReceiver : public ers::IssueReceiver
{
    void receive( const ers::Issue & issue )
    {
	std::lock_guard<std::mutex> lock(out_mutex) ;
    	std::cerr << issue << std::endl ;
    }
};

// just a test receiver which does nothing
struct MyNullIssueReceiver : public ers::IssueReceiver
{
    void receive( const ers::Issue & )
    {
    }
} ;

int main(int argc, char** argv)
{
    std::list< std::pair< std::string, std::string > > opt;
    try {
        opt = IPCCore::extractOptions( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::error( ex );
        return 2;
    }

//    opt.push_front( std::make_pair( std::string("maxListeningThreadsPerEndpoint"), std::string("2") ) );	// # of threads for connection polling
    opt.push_front( std::make_pair( std::string("threadPerConnectionPolicy"), std::string("0") ) );
    opt.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), std::string("2") ) );
//    opt.push_front( std::make_pair( std::string("threadPoolWatchConnection"), std::string("0") ) );
//    opt.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("5000") ) );
//    opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("30") ) );

    try {
	IPCCore::init(opt);    
	}
    catch( daq::ipc::Exception & ex ) {
        ers::error( ex );
        return 1;
    }

    // command line options and default values
    std::string subscription = "*" ;
    std::string partition ;
    unsigned int nrec = 1, sleep = 0 ;
    bool verb = false, i_stat = false ;

 	po::variables_map vm ;
    po::options_description desc("Message Transfer System ERS receiver application. Used to subscribe and to receive (and print out) messages from MTS ERS stream.");

     try {
    	 desc.add_options()
 		 ("subscription,s",	po::value<std::string>(&subscription), 			"subscription, e.g. * or 'sev=ERROR and not qual=Tile'")
 		 ("partition,p",	po::value<std::string>(&partition), 			"IPC partition")
 		 ("receivers,n",	po::value<unsigned int>(&nrec)->default_value(nrec), 	"number of subscriptions to made")
 		 ("sleep,t",		po::value<unsigned int>(&sleep)->default_value(sleep),	"burn (sleep) time for receiving a message, millisecs")
		 ("stat,i",		"publish interval statistics (to stdout)")
 		 ("verbosity,v",	"if specified, received Issues will be printed to stdout")
 	         ("help,h", 		"Print help message")
 		 ;

		po::store(po::parse_command_line(argc, argv, desc), vm) ;
		po::notify(vm) ;
		}
	catch( std::exception& ex )
		{
		ers::error( mts::WrongProgramOptions(ERS_HERE, argv[1], ex) );
		return 0 ;
		}

 	if ( vm.count("help") )
 		{
 		std::cout << desc << std::endl ;
 		return 0 ;
 		}

	if ( vm.count("stat") )
		{
		i_stat = true ;
		}

 	if ( vm.count("verbosity") )
 		{
 		verb = true ;
 		}

	if ( partition.empty() && !std::getenv("TDAQ_PARTITION") )
		{
		std::cerr << "partition name neither specified in program options nor defined in TDAQ_PARTITION environment." << std::endl ;
		return 1 ;
		}
    
    try {
	for ( size_t i = 0; i < nrec; i++ )
		{
		auto rec = new MyReceiver(verb, i_stat, sleep) ;
		auto rec0 = new MyNullIssueReceiver() ;
		receivers.push_back(rec) ;
		receivers.push_back(rec0) ;
		try {
	    		ers::StreamManager::instance().add_receiver( "mts", { partition, subscription, "100"} , rec );
	    		// ers::StreamManager::instance().add_receiver( "mts", { partition, subscription, "1"} , rec0 );
	    		ers::StreamManager::instance().add_receiver( "mts", { partition, subscription} , rec0 );
			}
		catch ( const ers::InvalidFormat& ex )
			{
    			std::cerr << "Failed to add ERS receiver for 'mts' stream:\n" << ex << std::endl ;
			return 1 ;
			}
		}
    	}
    catch ( ers::Issue& issue )
	{
    	std::cerr << "Failed to add ERS receiver for 'mts' stream:\n" << issue << std::endl ;
	return 1 ;
	}

    std::atexit(myatexit) ;

	sigset_t set ;
	sigemptyset(&set) ;
	sigaddset(&set, SIGINT) ;
	sigaddset(&set, SIGQUIT) ;
	sigaddset(&set, SIGTERM) ;
	signal(SIGINT, sig_func) ;
	signal(SIGQUIT, sig_func) ;
	signal(SIGTERM, sig_func) ;
	int sig ;
	sigwait(&set, &sig) ; // block and wait for a signal

    std::cerr << "Exiting application" << std::endl ;
    
    return 0; 
}
