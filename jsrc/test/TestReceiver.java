/** @file mts/jsrc/test/TestReceiver.java
 * Working example of a command-line Java MTS ERS receiver. Used in a Java test target.
 *
 * @author Andrei Kazarov
 * @date 2012
 */

package test;

import ers.StreamManager;

class TestReceiver {

	static class MyIssueReceiver implements ers.IssueReceiver {

		synchronized public void receive(ers.Issue issue) {
			// System.err.println("MyIssueReceiver: Issue received");
			try {
				ers.IssuePrinter.println(java.lang.System.out, issue,
						issue.severity(), ers.Logger.verbosityLevel());
			} catch (java.io.IOException ex) {
			}
		}
	}

	static class MyNullIssueReceiver implements ers.IssueReceiver {

	synchronized public void receive(ers.Issue issue) {
		}
	}

	public static void main(String argv[]) {
		try {
			ers.IssueReceiver receiver = new MyIssueReceiver();
			ers.IssueReceiver null_receiver = new MyNullIssueReceiver();
		
			Integer timeI = 100 ;
			Long timeL = 100L ;

			StreamManager.instance().add_receiver(receiver, "mts", "*", timeI) ; // variadic stream constructor
			StreamManager.instance().add_receiver(null_receiver, "mts", "*", timeL) ; // variadic stream constructor
			System.err.println("Receiver with 3 parameter added");

			if ( argv.length > 0 )
			    {
			    StreamManager.instance().add_receiver("mts", "*", null_receiver) ; // old API
			    System.err.println("Empty test receiver with 1 parameters added");
			    }
			
			// StreamManager.instance().add_receiver("mts", "*", receiver) ; //
			// to be used with new MTS subscription
			// StreamManager.instance().add_receiver("mts",
			// "sev=ERROR or qual=Test", receiver) ; // to be used with new MTS
			// subscription
			Thread.currentThread().sleep(Long.MAX_VALUE); // block program, wait

		} catch ( ers.BadStreamConfiguration ex ) {
			System.err.println("Failed to add receiver: " + ex.getCause() + ": " + ex.getCause().getCause()) ;

			try {
				throw ex.getCause() ; // underlying (mts) stream exception: mts.BadExpression,  mts.PublicationFailure
			}
			catch ( mts.BadExpression mex )
				{
				System.err.println("BadExpression Cause: " + mex.getCause()) ;
				}
			catch ( mts.PublicationFailure pex )
				{
				System.err.println("PublicationFailure Cause: " + pex.getCause()) ;
				}
			catch ( Throwable pex )
				{
				System.err.println("Unknown/unexpected Cause: " + pex + ": " + pex.getCause()) ;
				}
			
			ex.printStackTrace() ;
			System.exit(6);

		} catch (java.lang.InterruptedException ex) {
			System.err.println("Got " + ex.getMessage());
			System.exit(0);
		}
	}
}
