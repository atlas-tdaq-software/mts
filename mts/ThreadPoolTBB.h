/**
 *  \file ThreadPool.h
 * 
 *  Implementation of a simple thread pool class based on internal queue of tasks, hold in tbb::concurrent_queue.
 *  
 *  \author Kazarov A.
 *  \date 12.11.2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef MTS_TPOOL_H
#define MTS_TPOOL_H

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <vector>

#include "tbb/concurrent_queue.h"

namespace daq {
namespace mts {

class ThreadPoolTBB ;

class WorkerThreadTBB {
public:
    WorkerThreadTBB(ThreadPoolTBB &s) : pool(s) { }
    void operator()();
    ThreadPoolTBB &pool;
};


       /*! 	\brief Thread pool implementaion, used by MTS.
	 	\ingroup public
	 	\sa
       */
class ThreadPoolTBB {
friend class WorkerThreadTBB ;

public:
    ThreadPoolTBB(size_t pool_size) ;
    ~ThreadPoolTBB() ;

    size_t get_tasks_count() ;

    template<class F>    void schedule(F f) ;

private:
 
    // need to keep track of threads so we can join them
    std::vector< std::thread > worker_threads ;
 
    // the task queue
    tbb::concurrent_queue< std::function<void()> > tasks_queue ;
 
    // synchronization
    std::mutex queue_mutex;
    std::condition_variable condition;
    std::atomic<bool> stop;
};

// add new work item to the pool
template<class F>
void ThreadPoolTBB::schedule(F f)
{
    tasks_queue.push(std::function<void()>(f)) ;
 
    // wake up one thread
    condition.notify_one();
}

}} // daq::mts

#endif
