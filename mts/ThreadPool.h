/**
 *  \file ThreadPool.h
 *  \brief Implementation of a simple thread pool class with internal queue based on std::deque.
 *  \author Kazarov A.
 *  \date 12.11.2012.
 *  \copyright 2012 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 */

#ifndef MTS_TPOOL_H
#define MTS_TPOOL_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <deque>

namespace daq {
namespace mts {

class ThreadPool ;

class WorkerThread {
public:
    WorkerThread(ThreadPool &s) : pool(s) { }
    void operator()();
    ThreadPool &pool;
};


    /*! \brief Thread pool implementaion, used by MTS. Implementation with internal queue based on std::deque.
	*   \ingroup public
	*   \sa ThreadPoolNQ
    */
class ThreadPool {
friend class WorkerThread ;

public:
    ThreadPool(size_t pool_size, bool finish = false) ;
    ~ThreadPool() ;

    size_t get_tasks_count() ;

    template<class F>    void schedule(F f) ;

private:
 
    // need to keep track of threads so we can join them
    std::vector< std::thread > worker_threads ;
 
    // the task queue
    std::deque< std::function<void()> > tasks_queue ;
 
    // synchronization
    std::mutex queue_mutex;
    std::condition_variable condition;
    bool stop;
    bool finish ;
};

// add new work item to the pool
template<class F>
void ThreadPool::schedule(F f)
{
    { // acquire lock
        std::unique_lock<std::mutex> lock(queue_mutex);
 
        // add the task
        tasks_queue.push_back(std::function<void()>(f));
    } // release lock
 
    // wake up one thread
    condition.notify_one();
}

}} // daq::mts

#endif
