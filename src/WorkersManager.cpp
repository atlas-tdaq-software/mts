#include <chrono>              		  // for operator-, operator<=>, system_...
#include <compare>                    // for operator>, strong_ordering
#include <ctime>                      // for time
#include <exception>                  // for exception
#include <iostream>                   // for operator<<, endl, basic_ostream
#include <iterator>                   // for advance
#include <map>                        // for map, _Rb_tree_iterator, operator==
#include <random>                     // for mt19937, uniform_int_distribution
#include <string>                     // for string, char_traits, operator<<
#include <utility>                    // for pair

#include <omniORB4/CORBA.h>           // for is_nil
#include "ers/Issue.h"                // for operator<<
#include "ers/IssueFactory.h"         // for system_clock
#include "ers/LocalContext.h"         // for ERS_HERE
#include "ers/ers.h"                  // for ERS_DEBUG
#include "ipc/exceptions.h"           // for InvalidPartition, Exception
#include "ipc/partition.h"            // for IPCPartition
#include "ipc/policy.h"               // for narrow, no_cache, non_existent
#include "ipc/core.h"                 // for IPCore
#include "mts/IPCPartitionManager.h"
#include "mts/exceptions.h"           // for NoWorkers, mts
#include "mts/mts.hh"                 // for worker, worker_var
#include "mts/WorkersManager.h"

using namespace daq::mts ;

WorkersManager::WorkersManager( const std::string& part )
:IPCPartitionManager(part), m_rndgen(m_rnddev())
{
std::srand(std::time(0)) ;
std::srand(std::time(0)) ;
m_last_update_time = std::chrono::system_clock::now() ;
}


/*WorkersManager::WorkersManager( )
:m_rndgen(m_rnddev())
{
ERS_DEBUG(3, "Constructing WorkersManager") ;
std::srand(std::time(0)) ;
m_last_update_time = std::chrono::system_clock::now() ;
} */

WorkersManager::~WorkersManager() 
{
ERS_DEBUG(3, "Destructing WorkersManager") ;
RWLock lock(m_workers_mutex, true) ;
m_workers.clear() ;
}

void WorkersManager::update_workers(bool use_cache) // throw ( NoWorkers )
{
ERS_DEBUG(2, "Updating MTS workers in partition " << m_ipc_partition.name()) ;
m_last_update_time = std::chrono::system_clock::now() ;
std::map< std::string, ::mts::worker_var >  	workers ;

try	{
	if ( !IPCCore::isInitialised() )
		{
		std::cerr << "ERROR [MtsOutStream] IPC Core not initialized, messages will not be sent until IPCCore::init(argc,argv) called" << std::endl ;
		}
	if ( use_cache )
		{
		ERS_DEBUG(2, "Updating workers from cache");
		m_ipc_partition.getObjects< ::mts::worker >( workers ) ;
		}
	else
		{
		ERS_DEBUG(2, "Updating workers, NO cache");		
		m_ipc_partition.getObjects< ::mts::worker, ::ipc::no_cache, ::ipc::non_existent >( workers ) ;
		}
	}
catch 	( daq::ipc::InvalidPartition& ex )
	{
	std::cerr << "Invalid partition " << m_ipc_partition.name() << ": " << ex << std::endl ;
	throw NoWorkers(ERS_HERE, m_ipc_partition.name(), ex) ;
	}
catch 	( std::exception& ex )
	{
	std::cerr << "Got unexpected std::exception from getObjects " << ex.what() << std::endl ;
	throw NoWorkers(ERS_HERE, m_ipc_partition.name(), ex) ;
	}
catch 	( ... )
	{
	std::cerr << "Got unexpected exception from getObjects " << std::endl ;
	throw NoWorkers(ERS_HERE, m_ipc_partition.name()) ;
	}

if ( workers.empty() ) throw NoWorkers(ERS_HERE, m_ipc_partition.name() ) ;

// update m_workers with fresh value, protected section
// RWLock lock (m_workers_mutex, true) ;
m_workers = workers ; 
}

// returns a good random mts::worker or an exception
::mts::worker_var
WorkersManager::get_worker() // throw ( NoWorkers )
{
ERS_DEBUG(5, "Getting a random MTS worker") ;

RWLock lock(m_workers_mutex, true) ; // initialize or refresh workers from single thread only

if ( m_workers.size() == 0 )
	{
	ERS_DEBUG(1, "No workers yet registered, refresh ...") ;
	//lock.release() ;
	update_workers() ; // throw NoWorkers, may block....
	}
else	
	{
	//lock.release() ;
	if ( std::chrono::system_clock::now() - m_last_update_time > std::chrono::seconds(30) )
		{
ERS_DEBUG(2, "Updating list of workers in IPC, in case of (re)start") ;
		update_workers() ; 
		} // TODO: DONE: call update_workers() every 2 minutes
	}

lock.release() ;

assert ( m_workers.size() != 0 ) ;

::mts::worker_var aworker ;
std::string aworker_id ;

// TODO: reshuffle workers not at any call, but every ~2 minutes, to avoid 
while ( true ) 
	{
	RWLock lock(m_workers_mutex) ; // read-protect m_workers
	auto it = m_workers.begin() ;
	std::uniform_int_distribution<int> dist(0, m_workers.size() - 1) ;

	size_t rnd = dist(m_rndgen) ;
ERS_DEBUG(4, "Random: " << rnd << " Size: " << m_workers.size()) ;
	std::advance(it, rnd) ;  // choose random element

	assert ( it != m_workers.end() ) ;
	aworker = (*it).second ;
	aworker_id = (*it).first ;

	lock.release() ;

	// trying to check is the worker is good, calling IPC 'ping' on it
	// if not, call update_workers
	try	{
ERS_DEBUG(5, "Trying worker " << aworker) ;		
		if ( true == m_ipc_partition.isObjectValid< ::mts::worker, ::ipc::use_cache, ::ipc::narrow >(aworker_id) ) 
			{ 
			ERS_DEBUG(5, "Found good worker: " << aworker) ;
			return aworker ;
			}
		else	// worker is down or restarted 
			{ 
			ERS_DEBUG(0, "Worker " << aworker << " is not valid, will update workers from IPC and re-try") ;
			RWLock lock(m_workers_mutex, true) ;
			update_workers() ; // throw NoWorkers
			continue ; // try again
			} 
		}
	catch ( daq::ipc::Exception& ex ) // m_partition is not accessible
		{
		ERS_DEBUG(0, "No good workers found") ;
		throw NoWorkers(ERS_HERE, m_ipc_partition.name(), ex) ;		
		} 
	}
}
	
std::map< std::string, ::mts::worker_var >
WorkersManager::get_all_workers() 
{
RWLock lock(m_workers_mutex, true) ;
update_workers() ;
return m_workers ;
}
