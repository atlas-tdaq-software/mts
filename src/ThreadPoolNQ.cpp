#include <stddef.h>                                        // for size_t
#include <functional>
#include <atomic>                                          // for atomic
#include <condition_variable>                              // for condition_...
#include <exception>                                       // for exception
#include <memory>                                          // for allocator_...
#include <mutex>                                           // for mutex, uni...
#include <ostream>                                         // for operator<<
#include <thread>                                          // for thread, yield
#include <vector>                                          // for vector

#include "ers/Issue.h"                                     // for ERS_IS_EMP...
#include "ers/ers.h"                                     // for ERS_IS_EMP...
#include "mts/defs.h"                                      // for Task, mts
#include "mts/ThreadPoolNQ.h"

using namespace daq::mts ;

// main working cycle of a thread
void WorkerThread::operator()()
{
ERS_DEBUG(1, "Thread " << std::this_thread::get_id() << " starting") ;  

    while(true)
    {
    if ( pool.stop )
		{
ERS_DEBUG(1, "Stopping thread " << std::this_thread::get_id()) ; 
		return ;
		}

	if ( pool.total_counter - pool.busy_counter < pool.total_counter * 0.3 && pool.expandable )
		{
		// size_t new_size = pool.increase_pool() ;
		// ERS_LOG("MTS thread pool is quite busy, total threads now: " << pool.total_counter) ;
		}

    try {
ERS_DEBUG(4, "Thread " << std::this_thread::get_id() << " seeking for a job") ;  
	Task t = pool.task_provider.get_next_task() ; 	// get next task or TaskProvider::NoTasks
	pool.busy_counter++ ;
	t() ;		// execute the task
	pool.busy_counter-- ;
ERS_DEBUG(4, "Thread " << std::this_thread::get_id() << " executed a job") ;
		}
    catch ( const TaskProvider::NoTasks& )
		{
ERS_DEBUG(3, "Thread " << std::this_thread::get_id() << " idle waiting for a job") ;
	std::unique_lock<std::mutex> lock(pool.condition_mutex) ;
    	pool.ready_counter++ ;
		pool.ready = false ;
    	pool.condition.wait(lock, [&](){ return pool.ready || pool.stop; }) ;
		}
    catch ( std::exception& ex )
		{
		ERS_LOG("Unexpected exception from task: " << ex.what() ) ;  
		pool.busy_counter-- ;
		}
    catch ( ... )
		{
		ERS_LOG("Unexpected exception from task! Consider a BUG " ) ;  
		pool.busy_counter-- ;
		}
    }   
}

// the constructor just launches some amount of workers and waits for them to start and reach ready point
ThreadPoolNQ::ThreadPoolNQ(size_t nthreads, TaskProvider& tp, bool exp)
    :   expandable(exp), task_provider(tp), ready_counter(0), total_counter(0), busy_counter(0), stop(false), ready(false)
{
ERS_DEBUG(1, "Constructing ThreadPool") ;
initial_size = nthreads ;
for(size_t i = 0; i < nthreads; ++i)
	{ worker_threads.push_back(std::thread(WorkerThread(*this))) ; total_counter++ ; }

ERS_DEBUG(2, "Waiting for all threads to be ready to process tasks") ;
while( true ) { if ( ready_counter == nthreads ) { break ; } else { std::this_thread::yield() ; } ; } ;

ERS_DEBUG(1, "ThreadPoolNQ constructed and ready") ;
}
 
// the destructor joins all threads
ThreadPoolNQ::~ThreadPoolNQ()
{
ERS_DEBUG(1, "Destroying ThreadPool") ;

	// stop all threads
  stop = true;
  condition.notify_all();
 
	// join them when they finish
  for (size_t i = 0; i < worker_threads.size() ; ++i)
	  { worker_threads[i].join() ; }

ERS_DEBUG(1, "ThreadPool destroyed, there shall not be any more tasks around!") ;
}

size_t
ThreadPoolNQ::increase_pool()
{
for(size_t i = 0; i < initial_size; ++i)
	{ worker_threads.push_back(std::thread(WorkerThread(*this))) ; total_counter++ ; }
ERS_DEBUG(0, "ThreadPool size increased, now " << total_counter << " threads running") ;
return total_counter ;
}
