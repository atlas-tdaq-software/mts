
/*! \file examples/ers_sender.cpp
 * Working example of a command-line C++ ERS sender. To use with MTS, define TDAQ_ERS_ERROR='mts' and TDAQ_PARTITION environment.
 *
 * \author Andrei Kazarov
 * \date 2012
 */

#include <semaphore.h>
#include <ctype.h>
#include <stdlib.h>

#include <chrono>
#include <thread>

#include <boost/program_options.hpp>

#include <ipc/core.h>

// just test issues to be reported
 ERS_DECLARE_ISSUE(    lowlevellib,
			  NoConf,
			  "Low-level-lib Failure reason is: " << reason,
			  ((const char *)reason )
			  )
 ERS_DECLARE_ISSUE(    highlevellib,
			  BadConf,
			  "High-level-lib Failure reason is: " << reason,
			  ((const char *)reason )
			  )
 ERS_DECLARE_ISSUE(    mtssender,
			  ProcDied,
			  "Detected problem: " << reason <<
			  "PID: " << pid,
			  ((const char *)reason )
			  ((int)pid )
			  )

namespace po = boost::program_options ;

int main(int argc, char ** argv)
{
		 // Initialise IPC
		 try {
			IPCCore::init(argc,argv);
		 } catch (daq::ipc::Exception & ex) {
			exit(1) ;
		 }

	    // command line options and default values
	    size_t n_messages = 1 ;
		int sleep = 0, issue_type = 0 ;

	 	po::variables_map vm ;
	    po::options_description desc("ERS sender application. Used to send a number of test Issues at desired rates. "
	    		"Issues are sent to ers::error stream. To use it with MTS, define TDAQ_ERS_ERROR='mts', TDAQ_PARTITION, and possibly TDAQ_APPLICATION_NAME");

	     try {
	    	 desc.add_options()
	 		("messages,n",		po::value<size_t>(&n_messages)->default_value(n_messages),	"number of messages to send.")
	 		("sleep,t",		po::value<int>(&sleep)->default_value(sleep),		"time to sleep b/w sending messages (microseconds).")
	 		("issue,i",		po::value<int>(&issue_type)->default_value(issue_type),	"type of the Issue to sent. " 
													"0 = lowlevellib::NoConf with qualifier MTS-TEST, 1 = highlevellib::BadConf.")
	 	     	("help,h", 		"Print help message") ;

			po::store(po::parse_command_line(argc, argv, desc), vm) ;
			po::notify(vm) ;
			}
		catch( std::exception& ex )
			{
			std::cerr << ex.what() << std::endl ;
			return 1 ;
			}

	 	if ( vm.count("help") )
	 		{
	 		std::cout << desc << std::endl ;
	 		return 0 ;
	 		}


	//try simple error issue
	mtssender::ProcDied issue1( ERS_HERE, "can not acquire needed resources", 123) ;
	issue1.add_qualifier("MTS-TEST") ;
//	std::cerr << issue1 ;
	ers::error( issue1 ) ;

	//two issues chained	
        highlevellib::BadConf issue2(ERS_HERE, "No Configuration DB has been loaded") ;
        mtssender::ProcDied issue3( ERS_HERE, "Chained issue: failed to read configuration", 22, issue2 ) ;
//	std::cerr << issue3 ;
//	ers::error( issue3 );
	
	//three issues chained
	lowlevellib::NoConf nested_issue(ERS_HERE, "Single issue: missing configuration");
	highlevellib::BadConf issue4(ERS_HERE, "No Configuration DB has been loaded", nested_issue) ;
	mtssender::ProcDied issue5( ERS_HERE, "mts sender very well known problem", 11) ;
	nested_issue.add_qualifier("MTS-TEST") ;
//	std::cerr << issue ;
//	ers::error( issue );
 

for (size_t i = 0 ; i < n_messages ; i++)
	{
	ers::Issue* issue2sent ;
	if ( issue_type == 0 )
		{
		issue2sent = new lowlevellib::NoConf(ERS_HERE, "Single test issue: missing configuration") ;
		issue2sent->add_qualifier("MTS-TEST") ;
		}
	else	
		issue2sent = new highlevellib::BadConf(ERS_HERE, "No Configuration DB has been loaded", nested_issue) ;

	ers::error( *issue2sent ) ;

	if ( sleep ) { std::this_thread::sleep_for(std::chrono::microseconds(sleep)) ; }
  	}

// std::cerr << "Done!" << std::endl ;

	sem_t semaphore;
    	sem_init( &semaphore, 0, 0 );
    	sem_wait( &semaphore );

	return 0;
}

