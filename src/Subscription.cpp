#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/exceptions.h>
#include <mts/defs.h>
#include <mts/exceptions.h>
#include <mts/Subscription.h>
#include <mts/Worker.h>

using namespace daq::mts ;

Subscription::Subscription (const std::string& expr, const std::string& is_name, ReceiverP rec, Worker* wrk)
:m_priority(Normal), m_worker(wrk), m_expression(expr), m_is_object(is_name), m_is_active(true), m_is_slow(false),
m_queue_counter(0), m_thread_counter(0), m_messages_sent(0), m_messages_enqueued(0), m_average_delay(0),
m_total_delay(0), m_filter(), m_parser(&m_filter)
{
ERS_DEBUG(1, "Created subscription " << expr) ;
m_receiver = ::mts::receiver::_duplicate(rec) ;
} 

const char* sev2string(::ers2idl::Severity erssev)
{
switch ( erssev )
	{
	case ::ers2idl::Severity::debug: 		return "DEBUG"; break;
	case ::ers2idl::Severity::log: 			return "LOG"; break;
	case ::ers2idl::Severity::information: 	return "INFORMATION"; break;
	case ::ers2idl::Severity::warning: 		return "WARNING"; break;
	case ::ers2idl::Severity::error: 		return "ERROR"; break;
	default: 								return "FATAL";
	}
}
Subscription::~Subscription()
{ 
	decltype(m_messages_queue)::value_type v ;
	while ( m_messages_queue.try_pop(v) ) ;
} 

bool	Subscription::is_matched(const Message& msg)
{
ERS_DEBUG(1, "Parsing subscription " << m_expression << " for message " << msg.ID << ":" << sev2string(msg.sev)) ;
std::string::const_iterator it = m_expression.begin() ;
std::string::const_iterator end = m_expression.end() ;

// build a ShortMessage from 'full' Message, adding maps for fast search.
bool match, res ;
std::vector<std::string> quals ;
std::unordered_map<std::string, std::string> params, context ;

for ( size_t i = 0; i < msg.quals.length(); i++ )
	quals.emplace_back(std::string(msg.quals[i])) ;

for ( size_t i = 0; i < msg.params.length(); i++ )
	params.emplace(msg.params[i].name, msg.params[i].value) ;

context.emplace("app", msg.cont.application_name) ;
context.emplace("user", msg.cont.user_name) ;
context.emplace("host", msg.cont.host_name) ;
context.emplace("package", msg.cont.package_name) ;
context.emplace("function", msg.cont.function_name) ;
context.emplace("file", msg.cont.file_name) ;
context.emplace("line", std::to_string(msg.cont.line_number)) ; // TODO: be more effective, no strings (linenumber context is used very rarely anyway)

ShortMessage amsg { sev2string(msg.sev), std::string(msg.ID), std::string(msg.cont.application_name), std::move(quals), std::move(params), std::move(context) } ;

{ // protected section, since we have a single shared parser used for many messages (sent to the same subscriber) in parallel
std::lock_guard<std::mutex> lock(m_spirit_mutex) ;
m_filter.set_message(&amsg) ;
res = phrase_parse(it, end, m_parser, ascii::space, match) ;
}

if ( it != m_expression.end() )
	{ // TODO: exception
	ERS_LOG("Bad subscription (UNEXPECTED!): failed to parse, stopped at: " << std::string(it, end)) ;
	return false ;
	}

if ( res )
	{
	ERS_DEBUG(1, "Parsing result: " << match) ;
	return match ;
	}
else
	{ // TODO: exception
	ERS_LOG("Parsing failed!") ;
	}

return false ;
}

// just adds a message to the delivery queue, it will be picked up by TaskProvider scheduler at a good moment
void
Subscription::enqueue_message ( MessageSP message )
{
//std::lock_guard<std::mutex> lock(m_queue_mutex) ;
m_messages_queue.push(message) ;
m_queue_counter++ ;     // current
m_messages_enqueued++ ; // total
ERS_DEBUG(4, "Have " << m_messages_enqueued << " total messages queued for " << m_receiver) ;
}

// real CORBA call to a receiver to send a single message
// may block, or be slow
// this is a "quantum job" for a thread pool task to be assigned per Subscriber per message
void
Subscription::notify_receiver ()
{
if ( !m_is_active ) { return ; } // subscription was removed, lazy shared pointer destructor will be called

MessageSP message ;

size_t half_of_queued_messages = m_messages_queue.unsafe_size() >> 1 ;
m_thread_counter++ ; 	// mark that one more thread is busy with this subscriber
ERS_DEBUG(4, "Have " << m_thread_counter << " threads serving receiver " << m_receiver) ;

size_t local_counter = 0 ;

while (true)
try 	{
	if ( !m_messages_queue.try_pop(message) ) // return if nothing to send (e.g. queue was emptied by another thread)
		{
ERS_DEBUG(4, "Nothing to be sent to " << m_receiver << " exiting job, sent " << local_counter) ;
		m_thread_counter-- ; // returning from the worker thread () job
		return ;
		}

	m_queue_counter-- ;	// one message removed from the queue
ERS_DEBUG(5, "Subscriber " << m_receiver << " is served by " << m_thread_counter << " threads"  ) ;
	int time = timed_execute<std::chrono::milliseconds>([this,message]{ m_receiver->notify(*message) ; }) ;
ERS_DEBUG(4, "Subscriber " << m_receiver << " notified: " << time << " ms"  ) ;
	evaluate_priority(time) ;
	++m_messages_sent ; local_counter++ ;
	m_worker->message_sent() ; // for worker statistics
ERS_DEBUG(4, "Total messages sent to this subscriber: " << m_messages_sent ) ;
ERS_DEBUG(4, "Messages left in the queue: " << m_messages_queue.unsafe_size() << ":" << m_queue_counter) ;

	// in case we are served just by one thread, send 1/2 of all messages which were in the queue when we started this job
	// still do not give more then 1 thread to Slow subscriber m_thread_counter > 1
	if ( m_thread_counter > 1 || m_messages_queue.unsafe_size() < half_of_queued_messages || m_priority == Slow )
		{
ERS_DEBUG(4, "Finishing the task, however still " << m_queue_counter << " messages to sent: awaking another thread") ;
		if ( m_queue_counter )
			{
ERS_DEBUG(1, "This subscriber get its quantum of thread pool, finishing task, still " << m_queue_counter << " messages to sent") ;
			m_worker->awake_thread() ; // needed in order not to get stuck without active threads
			}
		break ;
		}
	else 
		{ 
 //		if ( m_messages_queue.unsafe_size() )
		ERS_DEBUG(5, "Continue serving the same receiver having still " << m_messages_queue.unsafe_size() << " messages queued" ) ;
		continue ; // try_pop next message from the queue of this Subscriber
		}

	} // while(true) loop
catch( const CORBA::TIMEOUT & ex )
	{
ERS_DEBUG(1, "TIMEOUT: Receiver "  << IPCCore::objectToString(m_receiver, IPCCore::Corbaloc) << " is slow, message not delivered and will be retried. Threads allocated: " << m_thread_counter ) ;
	evaluate_priority(Worker::call_timeout) ;
	m_messages_queue.push(message) ; // we've just removed the message fromt the front, need to try again
	m_queue_counter++ ;
	}
catch ( CORBA::SystemException& ex ) // minor codes: TRANSIENT_ConnectFailed, COMM_FAILURE_WaitingForReply
	{
	// TRANSIENT_CallTimedout needs to be handled in the same way as CORBA::TIMEOUT: receiver is alive, but slow
	if ( ex.minor() == omni::TRANSIENT_CallTimedout )
		{
ERS_DEBUG(1, "TIMEOUT: Receiver "  << IPCCore::objectToString(m_receiver, IPCCore::Corbaloc) << " is slow, message not delivered and will be retried. Threads allocated: " << m_thread_counter ) ;
		evaluate_priority(Worker::call_timeout) ;
		m_messages_queue.push(message) ; // we've just removed the message fromt the front, need to try again
		m_queue_counter++ ;
 		}
	else
	if ( m_is_active ) 
		{
ERS_DEBUG(0, "Failed to notify subscriber " << IPCCore::objectToString(m_receiver, IPCCore::Corbaloc) << ": " << ex ) ;
		ers::info( SubscriberRemoved(ERS_HERE, IPCCore::objectToString(m_receiver, IPCCore::Corbaloc), daq::ipc::CorbaSystemException( ERS_HERE, &ex )) ) ;
		m_is_active = false ; 				// to help other scheduled tasks to exit early
		m_worker->remove_receiver(m_receiver) ; 	// remove all subscriptions for this receiver
		}
	}
catch ( ... )
	{
ERS_DEBUG(0, "Unpredictable exception in Subscription::notify_receiver") ;
	}

m_thread_counter-- ; // task completed, decrease counter
ERS_DEBUG(4, local_counter << " messages sent, exiting thread task, still " << m_thread_counter << " threads managing this subscriber") ;
}

void Subscription::evaluate_priority(int delay)
{
	std::lock_guard<std::mutex> lock(m_eval_mutex) ;

	if ( delay == Worker::call_timeout ) // timeout
		{
		if ( m_priority != Slow )
			{ 
			ERS_LOG("Subscriber " << m_receiver<< " detected as Slow") ;
			}
		m_priority = Slow ;
		m_last_timeout = std::chrono::system_clock::now() ;
		}

	size_t last_msgs_count = m_messages_sent ;
	m_total_delay += delay ;

	// every N (=32) messages recalculate average delay
	if ( last_msgs_count >> 5 << 5 == last_msgs_count ) // do it every 32 messages
		{
		m_total_delay = m_total_delay * 0.5 ;
		m_average_delay = m_total_delay / 16 ;
		ERS_DEBUG(4, "Subs " << m_receiver << " MS: "<< m_messages_sent << " TD: " << m_total_delay << " AD: " << m_average_delay << " ms") ;
		}
	// m_average_delay = ( m_average_delay * m_messages_sent + delay ) / (m_messages_sent + 1);

	// if last Timeout was detected some time ago, we may re-evaluate this receiver as Normal
	if ( m_priority == Slow && (std::chrono::system_clock::now() - m_last_timeout) > std::chrono::seconds(s_timeout_reset) )
		{
		ERS_LOG("Subscriber " << m_receiver<< " is back to Normal priority") ;
		m_priority = Normal ;
		}
}

