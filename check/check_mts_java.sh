#!/bin/bash

partition=mtscheckjava

tmpdir=`mktemp -d`
trap "rm -rf ${tmpdir}" EXIT KILL TERM

export TDAQ_IPC_INIT_REF="file:/${tmpdir}/ipc.ref"

ipc_server &
pidipcg=$!

count=0;
while test $count -lt 5
do
        sleep 2
        test_ipc_server > /dev/null 2>&1 && { echo " OK!" ; break ; } 
        count=`expr $count + 1`
done

ipc_server -p $partition &
pidipcp=$!
sleep 1

is_server -p $partition -n MTS &
pidis=$!
sleep 1

#export TDAQ_ERS_STREAM_LIBS="mtsStreams"

TDAQ_ERS_DEBUG_LEVEL=2 TDAQ_APPLICATION_NAME=worker1 mts_worker -i -p $partition >& mts_worker1.out &
pidw1=$!

TDAQ_ERS_DEBUG_LEVEL=2 mts_worker -i -p $partition -n worker2 >& mts_worker2.out &
pidw2=$!

mts_worker -i -p initial -n worker_i >& mts_worker2_i.out &
pidw3=$!


sleep 1
echo -e "Workers' pids: $pidw1 $pidw2 $pidw3"

export TDAQ_ERS_VERBOSITY_LEVEL="3"
export TDAQ_ERS_DEBUG_LEVEL="0"

TDAQ_PARTITION=$partition TDAQ_ERS_DEBUG='null' TDAQ_ERS_LOG='null' \
mts_ers_receiver -t 0 -n 1 -s "*" -v   2> mts_ers_receiver_cpp.err > mts_ers_receiver_cpp.out &
pidrcpp=$!
echo `pwd`
TDAQ_PARTITION=$partition TDAQ_ERS_DEBUG='null' \
$TDAQ_JAVA_HOME/bin/java -cp test.jar:$TDAQ_CLASSPATH:$CLASSPATH test/TestReceiver > mts_ers_receiver.out 2> mts_ers_receiver.err &
pidr=$!

sleep 1


TDAQ_PARTITION=$partition TDAQ_ERS_ERROR='mts,lstdout,file(mts_ers_sender.out)' \
$TDAQ_JAVA_HOME/bin/java -cp test.jar:$TDAQ_CLASSPATH:$CLASSPATH test/TestReporter $1 >& mts_ers_sender-local.out &
#mts_ers_sender -n 1 >& mts_ers_sender-local.out &
pids=$!

sleep 3

#echo "killing one worker"
#kill $pidw2 

#sleep 3
#echo "restarting worker"
#TDAQ_ERS_DEBUG_LEVEL=2 mts_worker -i -p $partition -n worker2 >& mts_worker2.out &
#pidw2=$!

# wait for sender to finish
sleep 5

echo -e "\nComparing output..."
echo "---- receiver cpp ----"
cat mts_ers_receiver_cpp.out
echo "---- receiver java ----"
cat mts_ers_receiver.out
echo "---- sender ----"
cat mts_ers_sender.out
echo "---- sender-local ----"
cat mts_ers_sender-local.out
echo -e "------------"

result=0 ;
diff mts_ers_sender.out mts_ers_receiver.out && { echo -e "\n!Identical!\n"; result=0 ;}

received=`grep TestReporter.java: mts_ers_receiver.out | wc -l`
echo -n "$received messages received: "

[ $received = 45 ] && { echo "OK" ; }
[ $received != 45 ] && { echo "Failed!"; result=1; }

echo "Exiting check"

kill $pidr
kill $pidrcpp
kill $pids
ipc_rm -p $partition -f -n .\* -i .\*
sleep 1
kill $pidw2 $pidw1 $pidw3 &> /dev/null
sleep 1
kill $pidis
sleep 1
kill $pidipcp &> /dev/null
sleep 1
kill $pidipcg &> /dev/null
sleep 1

# sometimes it hangs at shutdown, and ctest waits forever...
kill -9 $pidipcp $pidipcg $pidis $pidw2 $pidw1 $pidw3 &> /dev/null

exit $result

