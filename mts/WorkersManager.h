/*! \file mts/WorkersManager.h
 *  
 *  Defines WorkersManager class, managing a list of mts:workers published in IPC partition
 *
 *  \author Andrei Kazarov
 *  \date 12.11.2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef MTS_WORKERSMANAGER_H
#define MTS_WORKERSMANAGER_H

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <chrono>
#include <random>

#include <tbb/queuing_rw_mutex.h>

#include <ers/Issue.h>

#include "mts/mts.hh"
#include "mts/exceptions.h"
#include "mts/ThreadPool.h"
#include <mts/IPCPartitionManager.h>

namespace daq {
namespace mts {

using RWLock = tbb::queuing_rw_mutex::scoped_lock ;

/*! \brief Internal service class, manages list of mts:workers published in IPC partition.
	   Used from Sender and Receiver classes which needs access the list of available workers in a partition
    \ingroup private
 */
class WorkersManager: public IPCPartitionManager
{
friend class ERSReceiver ;
protected:
        
	/*! Constructor for a specific partition.
		\param      p    IPC partition name.
		\throw 		daq::mts::NoWorkers, daq::ipc::InvalidPartition
		*/
	WorkersManager( const std::string& p ) ;

	~WorkersManager() ;

	/*! Get a good random worker, checked by isObjectValid from a cache. Used in Sender.
		\return Worker reference
		\throw NoWorkers if no Workers registered
		\warning uses IPC calls, may block or timeout
	 */
	::mts::worker_var get_worker()  ; // gets a random worker

	/*!
	 * Update list of workers published in IPC partition
	 * \param use_cache flag to use or not IPC cache when browsing all workers, default is true
	 * \throw NoWorkers if no Workers registered
	 * \warning uses IPC calls, may block or timeout
	 */
	void update_workers(bool use_cache = true) ;

	/*!
	 * Get a copy of map to all workers. Used in Receiver.
	 * @return a map of <name, worker_var> of all registered workers
	 * \warning uses IPC calls, may block or timeout
	 */
	std::map< std::string, ::mts::worker_var > get_all_workers() ;

private:

	tbb::queuing_rw_mutex							m_workers_mutex ; 	// control access to m_workers
	std::map< std::string, ::mts::worker_var >  	m_workers ; 		// map of active workers
	std::chrono::system_clock::time_point			m_last_update_time ;	// last update of workers in IPC

	std::random_device 				m_rnddev ;
	std::mt19937 					m_rndgen ;

	} ;

}} // daq::mts

#endif
