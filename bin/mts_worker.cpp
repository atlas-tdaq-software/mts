/*!
 *  \file mts_worker.cpp
 *  Defines main function for implementation of the MTS worker binary.
 *
 *
 *  \author A. Kazarov
 *  \date Nov. 2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#include <signal.h>
#include <string>

#include <boost/program_options.hpp>

#include <ers/ers.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>

#include "mts/Worker.h"

namespace po = boost::program_options ;

void sig_func(int) {} ; // not safe to report enything


ERS_DECLARE_ISSUE(      mts,
                        MissingParameter,
                        "Missing application parameter: " << text,
                        ((std::string)text )
                )

ERS_DECLARE_ISSUE(      mts,
                        WrongProgramOptions,
                        "Wrong program options passed: " << text,
                        ((std::string)text )
                )


int main(int argc, char** argv)
{
    std::list< std::pair< std::string, std::string > > opt;
    try {
        opt = IPCCore::extractOptions( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::error( ex );
        return 2;
    }

// maxServerThreadPerConnection - how many threads handle the same client connection (in case a client makes many frequent calls)

//    opt.push_front( std::make_pair( std::string("maxListeningThreadsPerEndpoint"), std::string("2") ) );	// # of threads for connection polling

    opt.push_front( std::make_pair( std::string("threadPerConnectionPolicy"), std::string("0") ) );		// thread pool mode
    opt.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), std::string("16") ) );		// number of input CORBA threads managing MTS senders
    opt.push_front( std::make_pair( std::string("threadPoolWatchConnection"), std::string("0") ) );		// return to the pool instead of waiting some more time on the same connection
    opt.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("5000") ) );
    opt.push_front( std::make_pair( std::string("maxGIOPConnectionPerServer"), std::string("16") )) ;   	// number of outgoing connections per client (MTS receiver) process
    // opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("30") ) );

    try {
	    IPCCore::init(opt);    
	}
    catch( daq::ipc::Exception & ex ) {
        ers::error( ex );
        return 1;
    }

    // command line options and default values
    std::string partition_name, worker_name ;
    size_t threads = 24 ;
    bool i_stat = false ;

	po::variables_map vm ;
   	po::options_description desc("Message Transfer System Worker application. To be started before other applications in partition infrastructure.");

    try {
		desc.add_options()
		  ("partition,p",	po::value<std::string>(&partition_name), 				"name of the partition (TDAQ_PARTITION)")
		  ("name,n",		po::value<std::string>(&worker_name), 					"unique name of this worker instance (TDAQ_APPLICATION_NAME if started within a partition)")
		  ("threads,t",		po::value<size_t>(&threads)->default_value(threads),		"size of dispatching thread pool, default 24")
		  ("sync,s",		"if specified, creates PMG sync file upon readiness")
		  ("stat,i",		"publish interval statistics (to stdout)")
	      ("help,h", 		"Print help message") ;

		po::store(po::parse_command_line(argc, argv, desc), vm) ;
		po::notify(vm) ;
		}
    catch(std::exception& ex) {
       ers::error(mts::WrongProgramOptions(ERS_HERE, argv[1], ex));
       return 0 ;
    }

	if ( vm.count("help") )
		{
		std::cout << desc << std::endl ;
		return 0 ;
		}

	if ( vm.count("stat") )
		i_stat = true ;

	if ( !vm.count("partition") )
		{
		const char *p = std::getenv("TDAQ_PARTITION") ;
		if ( !p )
			{
			ers::error( mts::MissingParameter(ERS_HERE, "partition name is not given not it is defined in TDAQ_PARTITION environment.") ) ;
			return 1 ;
			}
		partition_name = p ;
		}

	if ( !vm.count("name") )
		{
		const char *p = std::getenv("TDAQ_APPLICATION_NAME") ;
		if ( !p )
			{
			ers::error( mts::MissingParameter(ERS_HERE, "worker name is not given not it is defined in TDAQ_APPLICATION_NAME environment.") ) ;
			return 1 ;
			}
		worker_name = p ;
		}
        
    try {
        ERS_DEBUG(1, "constructing MTS worker") ;
    	daq::mts::Worker* worker = new daq::mts::Worker( partition_name, worker_name, threads, i_stat ) ;
        ERS_DEBUG(1, "publishing worker in IPC partition");
    	worker->publish() ; // throw ...

        if ( vm.count("sync") ) { pmg_initSync(); }
        ERS_DEBUG(1, "worker is ready, waiting for signal to exit");

        sigset_t set ;
        sigemptyset(&set) ;
        sigaddset(&set, SIGINT) ;
        sigaddset(&set, SIGQUIT) ;
        sigaddset(&set, SIGTERM) ;
        signal(SIGINT, sig_func) ;
        signal(SIGQUIT, sig_func) ;
        signal(SIGTERM, sig_func) ;
        int sig ;
        sigwait(&set, &sig) ; // block and wait for a signal

        ERS_LOG("interrupted by signal " << sig << ", stopping worker");
        worker->_destroy(true) ;
	}
    catch( daq::is::Exception & ex ) {
        ers::fatal( ex );
        std::cerr << "Failed to construct Worker, exiting..." << std::endl ;
        return 2 ;
    }
    catch( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
        std::cerr << "Failed to construct Worker, exiting..." << std::endl ;
        return 2 ;
    }
    catch( std::exception & ex ) {
        std::cerr << ex.what() << std::endl ;
        std::cerr << "Failed to construct Worker, exiting..." << std::endl ;
        return 3;
    }
    catch( ... ) {
        std::cerr << "unexpected exception thrown" << std::endl ;
        return 1;
    }
	
    ERS_LOG("exiting worker");

    return 0; 
}
