#!/bin/bash
#
# Script for running mts2splunk_receiver and redirecting it's std output to a host:port where splunk is listening
# Usage:
# ers2splunk.sh <partition> <subscription> <host> <port>
# 
#
[ $# -ne 4 ] && { echo "Wrong parameters. Usage: ers2splunk.sh <partition> <subscription> <host> <port"; exit 1 ;}
[ -f /usr/bin/nc ] || { echo "nc utility not found, exiting... "; exit 1 ;}

# prevent internal messagages to go to splunk
export TDAQ_ERS_LOG="null"
export TDAQ_ERS_DEBUG="null"
export TDAQ_ERS_ERROR="null"

# just for a record...
#exec 4>&1
#exitstatus=`{ { command1; printf $? 1>&3; } | command2 1>&4; } 3>&1`

mts2splunk_receiver -d -s "$2" -p "$1" -i "stdout" | /usr/bin/nc "$3" "$4" &

# this is pid of nc, mts2splunk_receiver is getting SIGTERM when it's parent exits (-d option)
pid="$!"

trap "kill $pid;" INT QUIT TERM

wait $pid